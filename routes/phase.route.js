'use strict';
const Phase = require('../models/phase.model');
const School = require('../models/school.model');

module.exports = router=>{
  router.post('/api/phase', (req, res, next)=>{
    let new_phase = new Phase(req.body);
    new_phase.save(err=>{
      res.json({r: "done"});
    });
  });

  router.get('/api/phase', (req, res, next)=>{
    Phase
    .find({})
    .exec((err, phases)=>{
      if(err){return next(err)}
      res.json(phases);
    });
  });

  // router.get('/api/phase/:id', (req, res, next)=>{
  //   Phase.findById(req.params.id)
  //   .limit(100)
  //   .populate('schools')
  //   .exec((err, phase)=>{
  //     if(err){return next(err)}
  //       res.json(phase);
  //     });
      
  // });

}
 