'use strict';
const School = require('../models/school.model');
const http = require('http');
//const sch = require('../schools');
const async = require('async');

module.exports = (router)=>{
    // router.get('/school/load-data', (req, res, next)=>{
    //     sch.forEach(item=>{
    //         let new_sch = new School(item);
    //         new_sch.Location = [parseFloat(item.GIS_Latitude), parseFloat(item.GIS_Longitude)];
    //         new_sch.RegistrationDate = new Date(item.RegistrationDate);
    //         new_sch.save();
    //     })
    //     res.json({response: "Data Loaded"});
    // });
    router.get('/api/school', (req, res, next)=>{
        School.find()
        .limit(3000)
        .sort('Institution_Name')
        .exec((err, schools)=>{
            if(err){return next(err)}
            res.json(schools);
        });
    }, (err, req, res, next)=>{console.log(err.message)});

    router.get('/api/school/ward/:id/:phase', (req, res, next)=>{
        School.find()
        .and([{Ward_ID: req.params.id}, {Phase: req.params.phase}])
        .sort('point')
        .sort('Institution_Name')
        .exec((err, schools)=>{
            if(err){return next(err)}
            res.json(schools);
        });
    }, (err, req, res, next)=>{console.log(err.message)});

    router.get('/api/school/district/:id/:phase', (req, res, next)=>{
        School.find()
        .and([{EIDistrict: req.params.id}, {Phase: req.params.phase}])
        .sort('point')
        .sort('Institution_Name')
        .exec((err, schools)=>{
            if(err){return next(err)}
            res.json(schools);
        });
    }, (err, req, res, next)=>{console.log(err.message)});

    router.get('/api/school/suburb/:id/:phase', (req, res, next)=>{
        School.find()
        .and([{Suburb: req.params.id}, {Phase: req.params.phase}])
        .sort('point')
        .sort('Institution_Name')
        .exec((err, schools)=>{
            if(err){return next(err)}
            res.json(schools);
        });
    }, (err, req, res, next)=>{console.log(err.message)});

    router.get('/api/school/:province/:phase', (req, res, next)=>{
        School.find()
        .and([{Province: req.params.province}, {Phase: req.params.phase}])
        .limit(100)
        .sort('-point')
        .sort('Institution_Name')
        .exec((err, schools)=>{
            if(err){return next(err);}
            res.json(schools);
        });
    });

    router.get('/api/school/district/profile/:province/:district', (req, res, next)=>{
        School.find()
        .and([{Province: req.params.province}, {EIDistrict: req.params.district}])
        .exec((err, schools)=>{
            if(err){return next(err);}
            res.json(schools);
        });
    });

    // router.get('/api/school/province', (req, res, next)=>{
    //    let distinctProvince = [];
    //     School
    //     .find()
    //     .exec((err, schools)=>{
    //         if(err){return next(err);}

    //         let province = schools.map((item)=>{
    //             return item.Province
    //         });

    //         province.forEach(item=>{
    //             if(distinctProvince.indexOf(item) < 0){
    //                 distinctProvince.push(item);
    //             }
    //         });
    //         res.json({province: distinctProvince});
    //     });
    // }, (err, req, res, next)=>{console.log(err.message)});

    router.get('/api/school/:id', (req, res, next)=>{
        School.findById(req.params.id)
        .exec((err, school)=>{
            if(err){return next(err)}
            res.json(school);
        })
    }, (err, req, res, next)=>{console.log(err.message)});

    router.post('/api/school', (req, res, next)=>{
        let new_sch = new School(req.body);

        new_sch.save(err=>{
            if(err){return next(err)}
            res.json({response: "School added"});
        });
    }, (err, req, res, next)=>{console.log(err.message)});

    router.get('/api/school/fixture', (req, res, next)=>{

    })

}
