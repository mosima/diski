var Fixture = require('../models/fixture.model');
var School = require('../models/school.model');
var Tournament = require('../models/tournament.model');
var _ = require('lodash');
var async = require('async');
var dateUtil = require('../utility/dateUtil');

module.exports = function (router, async) {

    router.get('/api/fixture', function (req, res, next) {
        Fixture.find()
            .populate('home_team')
            .populate('away_team')
            .limit(20)
            .exec(function (err, fixtures) {
                if (err) { return next(err); }
                res.json(fixtures);
            });
    });

    // router.get('/api/fixture', function (req, res, next) {
    //     Fixture.find()
    //         .and([{played: false}, {}])
    //         .populate('home_team')
    //         .populate('away_team')
    //         .limit(20)
    //         .exec(function (err, fixtures) {
    //             if (err) { return next(err); }
    //             res.json(fixtures);
    //         });
    // });

    router.get('/api/fixture/province/:province', function (req, res, next) {
        Fixture.find({ province: req.params.province })
            .populate('home_team')
            .populate('away_team')
            .exec(function (err, fixtures) {
                if (err) { return next(err); }
                res.json(fixtures);
            });
    });

    router.get('/api/fixture/generate-fixture/:province/:district/:phase/:term', (req, res, next) => {

        async.waterfall([
            (cb) => {
                School
                    .find()
                    .and([{ Province: req.params.province }, { EIDistrict: req.params.district }, { Phase: req.params.phase }])
                    .exec((err, schools) => {
                        if (err) { return cb(err) }
                        var wards = schools.map(item => { return item.Ward_ID });   
                        cb(null, wards, schools);
                    });
            },
            (wards, schools, cb) => {
                var distinctWard = [];
                wards.forEach(item => {
                    if (distinctWard.indexOf(item) < 0) {
                        distinctWard.push(item);
                    }
                });
                var distinctWard_sch = distinctWard.map(item => { return { ward: item, schools: [] } });
                cb(null, distinctWard_sch, schools);
            },
            (wards, schools, cb) => {
                schools.forEach(sch => {
                    wards.forEach(w => {
                        if (sch.Ward_ID == w.ward) {
                            w.schools.push(sch);
                        }
                    });
                });
                cb(null, wards);
            },
            (wards, cb) => {
                dateUtil.generateMatchDates(req.params.term, wards, cb);
            },
            (wards, dates, cb) => {

                wards.forEach(item => {
                    generateFixture(item.schools, dates);
                });
                cb(null, "Done");
            },
            (data, cb) => {
                Fixture.find().where('updatedAt').gte(new Date()).exec((err, f) => { console.log(f.length) });
                res.json({ response: "Done" });
            }

        ]);

    });

    router.get('/api/fixture/:id', function (req, res, next) {
        Fixture.findById({ _id: req.params.id })
            .populate('home_team')
            .populate('away_team')
            .populate('tournament')
            .exec(function (err, fixture) {
                if (err) { return next(err); }
                res.json(fixture);
            });
    });

    router.get('/api/fixture/school/:id', function (req, res, next) {
        Fixture.find()
            .or([{ home_team: req.params.id }, { away_team: req.params.id }])
            .populate('home_team')
            .populate('away_team')
            .sort('match_date')
            .exec(function (err, fixture) {
                if (err) { return next(err); }
                res.json(fixture);
            });
    });

    router.get('/api/fixture/school/three/:id', function (req, res, next) {
        Fixture.find()
            .or([{ home_team: req.params.id }, { away_team: req.params.id }])
            .populate('home_team')
            .populate('away_team')
            .limit(3)
            .exec(function (err, fixture) {
                if (err) { return next(err); }
                res.json(fixture);
            });
    });

    router.get('/api/fixture/tournament/:id', function (req, res, next) {
        Fixture.find()
            .and([{ tournament: req.params.id }, { played: false }])
            .select(['cup_team_home', 'cup_team_away', 'home_team', 'away_team', 'match_date', 'group'])
            .populate('home_team')
            .populate('away_team')
            .populate('group', 'name')
            .populate('cup_team_home', 'team')
            .populate('cup_team_away', 'team')
            .populate({
                path: 'cup_team_home',
                populate: {path: 'team'}
            })
            .populate({
                path: 'cup_team_away',
                populate: {path: 'team', select: 'name'}
            })
            .exec(function (err, fixture) {
                if (err) { return next(err); }
                res.json(fixture);
            });
    });

    router.post('/api/fixture', function (req, res, next) {
        async.waterfall([
            function (callback) {
                var new_fixture = new Fixture(req.body);
                new_fixture.createdBy = req.user._id;
                new_fixture.modifiedBy = req.user._id;
                new_fixture.save(function (err) {
                    if (err) { return next(err); }
                    callback(null, new_fixture);
                });
            },
            function (fixture, callback) {
                Tournament.findById({ _id: fixture.tournament }, function (err, tournament) {
                    if (err) { return next(err); }
                    tournament.teams = tournament.teams.concat([fixture._id]);

                    tournament.save(function (err) {
                        if (err) { return next(err); }
                        res.json({ response: "success" });
                    });
                });
            }
        ]);
    });
}

function generateFixture(arr, rd) {

    arr.forEach((s) => {
        var count = 0;
        arr.forEach((t, i) => {

            if ((s._id !== t._id)) {
                Fixture
                    .findOne({ match_date: new Date(rd[count]), home_team: t._id })
                    .exec((err, fx) => {
                        if (!fx) {
                            let f = new Fixture({
                                home_team: s._id,
                                away_team: t._id,
                                location: s.Location,
                                phase: s.Phase,
                                province: s.Province,
                                match_date: new Date(rd[i])
                            });
                            f.save(err => {
                                if (err) { return next(err); }

                            });
                        }
                    });

                count++;
            }
        });
    });
}