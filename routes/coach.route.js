var Coach = require('../models/coach.model');
const Multer = require('multer');
const _ = require('lodash');
const multer = Multer({
    storage: Multer.memoryStorage()
});

module.exports = function (router, async) {
    router.get('/api/coach', function (req, res, next) {
        Coach.find(function (err, coaches) {
            if (err) { return next(err) }
            res.json(coaches);
        });
    });

    router.get('/api/coach/:id', function (req, res, next) {
        Coach.findById({ _id: req.params.id }, function (err, coach) {
            if (err) { return next(err); }
            res.json(coach);
        });
    });

    router.post('/api/coach', multer.single('picture'), function (req, res, body) {
        var new_coach = new Coach({
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            middlename: req.body.middlename,
            date_of_birth: req.body.dob,
            profile: req.body.bio,
            team: req.body.team,
            created_by: req.user._id
        });
        new_coach.save(function (err) {
            if (err) { return next(err); }
            res.json({ info: "success" });
        });
    });

    router.put('/api/coach/:id', (req, res, next) => {
        Coach.findById(req.params)
            .exec((err, coach) => {
                if (err) { return next(err); }
                _.merge(coach, req.body);
                coach.save(err => {
                    if (err) { return next(err); }
                    res.json({ response: "coach updated" });
                });
            });
    });

    router.delete('/api/coach/:id', (req, res, next) => {
        Coach.findByIdAndRemove(req.params.id, (err) => {
            if (err) { return next(err); }
            res.json({ response: "coach removed" });
        });
    });
}