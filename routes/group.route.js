const Group = require('../models/group.model');

module.exports = router => {
    router.post('/api/group', (req, res, next) => {
        let gp = new Group(req.body);
        gp.save(err => {
            if (err) { return next(err); }
            res.json({ response: "group created" })
        })
    })
    router.get('/api/group/:tourId', (req, res, next) => {
        Group.find({tournament: req.params.tourId})
        .populate('teams')
        .populate({
            path: 'teams',
            populate: {path: 'team'}
        })
        .exec((err, gps)=>{
            if(err){return next(err);}
            res.json(gps);
        });
     });
    router.put('/api/group/add-team/:groupId', (req, res, next) => {
        Group.findById(req.params.groupId)
        .exec((err, gp)=>{
            if(err){return next(err);}
            req.body.selectedOption.forEach(element => {
                if (gp.teams.indexOf(element.value) < 0) {
                    gp.teams = gp.teams.concat([element.value]);
                }
            });
            gp.save(err=>{
                if(err){return next(err);}
                res.json({response: "success"});
            })
        })
     })
    router.put('/api/group/:id', (req, res, next) => { })
    router.delete('/api/group/:id', (req, res, next) => { })
}