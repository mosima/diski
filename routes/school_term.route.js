'use strict';
const Term = require('../models/school_term.model');

module.exports = (router)=>{
    router.get('/api/term', (req, res, next)=>{
        Term.find()
        .sort('-createdAt')
        .exec((err, terms)=>{
            if(err){return next(err);}
            res.json(terms);
        });
    });

    router.get('/api/term/:id', (req, res, next)=>{
        Term.findById(req.params.id).exec((err, term)=>{
            if(err){return next(err);}
            res.json(term);
        });
    });

    router.post('/api/term', (req, res, next)=>{
        let new_term = new Term({
            number: parseInt(req.body.number),
            start_date: new Date(req.body.start_date),
            end_date: new Date(req.body.end_date),
            year: new Date(req.body.year).getFullYear() || new Date().getFullYear()
        });

        new_term.save(err=>{
            if(err){return next(err);}
            res.status(201).json({response: "Added"});
        })
    });
    
}