'use strict';
const District = require('../models/district.model');
const Phase = require('../models/phase.model');
module.exports = router=>{
  // router.get('/api/district/:province/:phase', (req, res, next)=>{
  //   District.find({province: req.params.province})
  //   .populate('schools')
  //   .exec((err, districts)=>{
  //     if(err){return next(err)}
  //     res.json(districts);
  //   });
  // });

  router.get('/api/district/:province', (req, res, next)=>{
    District.find({province: req.params.province})
    .exec((err, dis)=>{
      if(err){return next(err);}
      res.json(dis);
    });
  });
}
