var TT = require('../models/tournamentType.model');

module.exports = function (router, async) {

    router.get('/tournamentType', function (req, res, next) {
        TT.find(function (err, uts) {
            if (err) { return next(err); }
            res.json(uts);
        });
    });

    router.get('/tournamentType/:id', function (req, res, next) {
        TT.findById({ _id: req.params.id }, function (err, ut) {
            if (err) { return next(err); }

            res.json(ut);
        });
    });

    router.get('/tournamentType/delete/:id', function (req, res, next) {
        TT.findByIdAndRemove({ _id: req.params.id }, function (err) {
            if (err) { return next(err); }
           res.json({info:"success"});
        });
    });

    router.post('/tournamentType', function (req, res, next) {
        var newTt = new TT({
            type: req.body.type
        });
        newTt.save(function (err) {
            if (err) { return next(err); }
            res.json({info:"success"});
        });
    });
}
