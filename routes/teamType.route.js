var TT = require('../models/teamType.model');

module.exports = function (router, async) {
    router.get('/teamType', function (req, res, next) {
        TT.find(function (err, uts) {
            if (err) { return next(err); }
            res.json(uts);
        });
    });

    router.get('/teamType/:id', function (req, res, next) {
        TT.findById({ _id: req.params.id }, function (err, ut) {
            if (err) { return next(err); }

            res.json(ut);
        });
    });

    router.post('/teamType', function (req, res, next) {
        var newTt = new TT({
            type: req.body.name
        });
        newTt.save(function (err) {
            if (err) { return next(err); }
            res.redirect('/admin/configuration');
        });
    });

    router.get('/teamType/delete/:id', function (req, res, next) {
        TT.findByIdAndRemove({ _id: req.params.id }, function (err) {
            if (err) { next(err); }
            res.redirect('/admin/configuration');
        });
    });
};