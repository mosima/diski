'use strict';

const Holiday = require('../models/publicholiday.model');

module.exports = (router)=>{
    router.get('/api/holiday', (req, res, next)=>{
        Holiday.find()
        .exec((err, holidays)=>{
            if(err){return next(err);}
            res.json(holidays);
        });
    });

    router.get('/api/holiday/:id', (req, res, next)=>{
        Holiday.findById(req.params.id)
        .exec((err, holiday)=>{
            if(err){return next(err);}
            res.json(holiday);
        });
    });

    router.post('/api/holiday', (req, res, next)=>{
        let new_holiday = new Holiday({
            name: req.body.name,
            date: new Date(req.body.date),
            year: new Date(req.body.date).getFullYear() || new Date().getFullYear()
        });
        new_holiday.save(err=>{
            if(err){return next(err)}
            res.json({response: "Holiday Added"});
        });
    });

    router.delete('/api/holiday/:id', (req, res, next)=>{
        Holiday.findByIdAndRemove(req.params.id, (err)=>{
            if(err){return next(err)}
            res.json({response: "Holiday removed"});
        });
    });
}
