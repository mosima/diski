var Team = require('../models/team.model');
var TeamType = require('../models/teamType.model');
var Player = require('../models/player.model');
var _ = require('lodash');
const Multer = require('multer');
const multer = Multer({
    storage: Multer.memoryStorage()
});

module.exports = function (router, async) {
    router.get('/api/team', function (req, res, next) {
        Team.find().populate('type', 'type').populate('owner', 'displayName').exec(function (err, teams) {
            if (err) { return next(err); }
            res.json(teams);
        });
    });

    router.get('/api/team/my', (req, res, next) => {
        if (req.user) {
            Team
                .find({ owner: req.user._id })
                .populate('coach')
                .exec((err, teams) => {
                    if (err) { return next(err); }
                    res.json({ teams: teams });
                })
        } else {
            res.json({ teams: false });
        }
    });

    router.get('/api/team/:id', function (req, res, next) {
        Team.findById({ _id: req.params.id }).populate('type').populate('owner').exec(function (err, team) {
            if (err) { return next(err); }

            res.json(team);
        });
    });

    router.post('/api/team', multer.single('logo'), (req, res, next) => {
        var newTeam = new Team({
            name: req.body.name,
            owner: req.user._id,
            type: req.body.teamType,
            year: req.body.year,
            venue: req.body.venue,
            coach: req.body.coach
        });

        newTeam.save((err) => {
            if (err) { return next(err); }
            res.json({ response: "success" });

        });
    });

    router.put('/api/team/:teamId', function (req, res, next) {
        Team.findById(req.params.teamId, function (err, team) {
            if (err) { return next(err); }

            _.merge(team, req.body);
            team.save(function (err) {
                if (err) { return next(err); }
                res.json({ response: "team edited successfully" });
            });
        });
    });

    router.delete('/api/team/:id', (req, res, next) => {
        Player.find({team: req.params.id}, (err, players)=>{
            if(err){return next(err);}
            players.forEach(p => {
                Player.findByIdAndRemove(p._id);
            }); 
        });
        Team.findByIdAndRemove(req.params.id, (err)=>{
            if(err){return next(err);}
            res.json({response: "success"});
        });
    });
}