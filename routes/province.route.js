'use strict';
const Province = require('../models/province.model');

module.exports = router=>{
  router.get('/api/province', (req, res, next)=>{
    Province
    .find()
    .exec((err, provinces)=>{
      if(err){return next(err)}
      res.json({province: provinces});
    });
  });

  // router.get('/api/province/:id', (req, res, next)=>{
  //   Province
  //   .findById(req.params.id)
  //   .exec((err, province)=>{
  //     if(err){return next(err)}
  //     res.json(province);
  //   });
  // });
}
