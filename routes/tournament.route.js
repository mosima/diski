var Tournament = require('../models/tournament.model');
var TournamentType = require('../models/tournamentType.model');
const async = require('async');
const CupTeam = require('../models/cup_team.model');


module.exports = function (router, async) {
    router.get('/api/tournament', function (req, res, next) {
        Tournament.find()
            .exec(function (err, tournaments) {
                if (err) { return next(err); }
                res.json(tournaments);
            });
    });

    router.get('/api/tournament/:id', function (req, res, next) {
        Tournament.findById({ _id: req.params.id })
            // .populate('cup_team')
            // .populate({
            //     path: 'cup_team',
            //     populate: { path: 'team' }
            // })
            .exec(function (err, tournament) {
                if (err) { return next(err); }
                res.json(tournament);
            });
    });

    router.get('/api/tournament/cup-team/:tourId', (req, res, next)=>{
        CupTeam.find({tournament: req.params.tourId})
        .populate('team', 'name')
        .exec((err, cts)=>{
            if(err){return next(err);}
            res.json(cts);
        })
    })

    router.post('/api/tournament', function (req, res, next) {

        var new_tournament = new Tournament(req.body);
        new_tournament.save(function (err) {
            if (err) { return next(err); }
            res.json({ response: "Tournament created" })
        });

    });

    router.put('/api/tournament/register-team/:tourId', (req, res, next) => {
        async.waterfall([
            (cb) => {
                req.body.selectedOption.forEach(element => {
                    CupTeam.findOne({ team: element.value, tournament: req.params.tourId })
                        .exec((err, team) => {
                            if (err) { return next(err); }
                            if (!team) {
                                let cp = new CupTeam({
                                    team: element.value,
                                    tournament: req.params.tourId
                                });
                                cp.save(err => { return next(err) })
                            }
                        })

                });
                cb(null)
            },
            () => {
                Tournament.findById(req.params.tourId)
                .exec((err, tour) => {
                    if (err) { return next(err); }
                    req.body.selectedOption.forEach(element => {
                        if (tour.cup_team.indexOf(element.value) < 0) {
                            tour.cup_team = tour.cup_team.concat([element.value]);
                        }
                    });
                    tour.save(err => {
                        if (err) { return next(err); }

                    })
                });
                res.json({ response: "teams registered" });
            }
        ])
    })

    router.put('/api/tournament/:id', function (req, res, next) {
        Tournament.findById(req.params.id, function (err, tournament) {
            if (err) { return next(err); }
            if (tournament.teams.indexOf(req.body.team) >= 0) {
                res.json({ info: "could not add team to tournament" });
            } else {
                tournament.teams.push(req.body.team);
                tournament.save(function (err) {
                    if (err) { return next(err); }
                    res.json({ info: "success" });
                });
            }
        });
    });
}