const Coach = require('../models/coach.model');
const Player = require('../models/player.model');

module.exports = router =>{
    router.get('/api/coach/my', (req, res, next)=>{
         if(req.user){
             Coach.find({created_by: req.user._id})
             .exec((err, coaches)=>{
                 if(err){return next(err);}
                 res.json({coaches: coaches})
             })
         }else{
             res.json({coaches: false});
         }
     });

     router.get('/api/system/my', (req, res, next)=>{
         if(req.user){
             Player.find({registeredBy: req.user._id})
             .exec((err, players)=>{
                 if(err){return next(err);}
                 res.json({players: players});
             });
         }else{
             res.json({players: false});
         }
     })
}
