var News = require('../models/news.model');
var User = require('../models/user.model');
var Team = require('../models/team.model');
var comment  = require('../models/comment.model');
var moment = require('moment');
var imgUpload = require('../modules/imgUpload');
var Multer = require('multer');
var store = Multer.memoryStorage();
const multer = Multer({
  storage: store
});

module.exports = function (router, async) {
  router.post('/api/news/add-news', multer.single('media'), imgUpload.uploadToGcs,function(req,res,next){
    if(req.file && req.file.cloudStoragePublicUrl){
      var news = new News({
        title: req.body.title,
      body: req.body.body,
     media: req.file.cloudStoragePublicUrl
     });
      news.save(function(err)
      {
        if(err) return next(err);
        res.json({response:"success"});
      });
    }else{
      res.status(502).json({error: "file could not upload to Cloud Storage"});
    }

  });

router.get('/api/news', function (req, res, next) {
    News
    .find()
    .exec((err, news)=>{
      if(err){return next(err)}
      res.status(200).json(news);
    });
 });
 router.get('/api/news/delete/:id', function (req, res, next) {

      News.findByIdAndRemove({ _id: req.params.id }, function (err) {
        if (err) { return next(err); }
        res.json({ info: "success" });
      });
    })

    router.post('/api/news/update/:id', function (req, res, next) {
      News.findById({ _id: req.params.id }, function (err, news) {
        if (err) { return next(err); }
        _.merge(news, req.body);
        news.save(function (err) {
          if (err) { return next(err); }
         res.json({ news: "success" });
        });
      });
    });

    router.delete('/api/news/:id', (req, res, next)=>{
      News.findByIdAndRemove(req.params.id, (err)=>{
        if(err){return next(err)}
        res.json({response: "deleted"});
      });
    });
  }
