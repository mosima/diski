var Team = require('../models/team.model');
var Result = require('../models/result.model');
var Fixture = require('../models/fixture.model');
var Tournament = require('../models/tournament.model');
var CupTeam = require('../models/cup_team.model');

module.exports = function (router, async) {

    router.get('/api/result', function (req, res, next) {
        Result.find()
            .populate('home_team')
            .populate('away_team')
            .exec(function (err, results) {
                if (err) {
                    return next(err);
                }
                res.json({
                    data: results
                });
            });
    });

    router.get('/api/result/:id', function (req, res, next) {
        Result.findById({_id: req.params.id})
            // .populate('home_team')
            // .populate('away_team')
            // .populate('fixture')
            .exec(function (err, results) {
                if (err) {
                    return next(err);
                }
                res.json({data: results});
            });
    });

    router.post('/api/result/:id', function (req, res, next) {
        async.waterfall([
            (cb) => {
                Tournament.findById({
                    _id: req.params.id
                }, async (err, tour) => {
                    if (err) {
                        return next(err);
                    }
                    const homeTeam = await CupTeam.findById({
                        _id: req.body.home
                    });
                    const awayTeam = await CupTeam.findById({
                        _id: req.body.away
                    });

                    cb(null, homeTeam, awayTeam, tour);
                });
            },
            (homeTeam, awayTeam, tour, cb) => {

                if (tour.type == "cup") {
                    if (parseInt(req.body.home_score) > parseInt(req.body.away_score)) {
                        //updating home team
                        homeTeam.goal_scored = parseInt(req.body.home_score);
                        homeTeam.goal_conceded = parseInt(req.body.away_score);
                        homeTeam.match_won += 1;
                        homeTeam.point += 3;
                        homeTeam.match_played += 1;
                        homeTeam.goalDifference += parseInt(req.body.home_score);
                        //updating away team
                        awayTeam.goal_scored = parseInt(req.body.home_score);
                        awayTeam.goal_conceded = parseInt(req.body.away_score);
                        awayTeam.match_lost += 1;
                        awayTeam.match_played += 1;
                        awayTeam.goalDifference -= parseInt(req.body.home_score);

                        homeTeam.save((err) => {
                            if (err) {
                                return next(err)
                            }
                        });

                        awayTeam.save((err) => {
                            if (err) {
                                return next(err)
                            }
                        });
                        cb(null, tour);

                    } else if (parseInt(req.body.home_score) < parseInt(req.body.away_score)) {
                        //updating home team
                        homeTeam.goal_scored = parseInt(req.body.home_score);
                        homeTeam.goal_conceded = parseInt(req.body.away_score);
                        homeTeam.match_lost += 1;
                        homeTeam.match_played += 1;
                        homeTeam.goalDifference -= paserInt(req.body.away_score);
                        //updating away team
                        awayTeam.goal_scored = parseInt(req.body.home_score);
                        awayTeam.goal_conceded = parseInt(req.body.away_score);
                        awayTeam.match_won += 1;
                        awayTeam.point += 3;
                        awayTeam.match_played += 1;

                        awayTeam.goalDifference += parseInt(req.body.away_score);

                        homeTeam.save((err) => {
                            if (err) {
                                return next(err)
                            }
                        });
                        awayTeam.save((err) => {
                            if (err) {
                                return next(err)
                            }
                        });
                        cb(null, tour);

                    } else {


                        homeTeam.point += 1;
                        homeTeam.match_drawn += 1;
                        homeTeam.match_played += 1;

                        awayTeam.point += 1;
                        awayTeam.match_drawn += 1;
                        awayTeam.match_played += 1;
                        homeTeam.save((err) => {
                            if (err) {
                                return next(err)
                            }
                        });
                        awayTeam.save((err) => {
                            if (err) {
                                return next(err)
                            }
                        });
                        cb(null, tour);
                    }


                }
                if (type == "league") {
                    res.json(type);
                }
            }, (tour, cb) => {
                Fixture.findById({
                    _id: req.body.fixtureId
                }, function (err, fixture) {
                    if (err) {
                        return next(err);
                    }
                    fixture.played = true;

                    fixture.save(function (err) {
                        if (err) {
                            return next(err);
                        }
                        cb(null, fixture, tour);
                    });
                });
            },
            (fixture, tour, cb) => {
                var newResult = new Result({
                    fixture: fixture._id,
                    home_team: req.body.home,
                    home_team_score: req.body.home_score,
                    away_team: req.body.away,
                    away_team_score: req.body.away_score,
                    tournament: req.params.id
                });

                newResult.save(function (err) {
                    if (err) {
                        return next(err);
                    }
                    //res.json(newResult);
                    cb(null, fixture, tour, newResult);
                });
            },
            (fixture, tour, result, cb) => {

                tour.results = tour.results.concat([result._id]);
                tour.save(function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.json({
                        info: "tournament updated "
                    });
                });
            }
        ]);
    });
};
