var Player = require('../models/player.model');
var _ = require('lodash');
var Team = require('../models/team.model');
const Multer = require('multer');
const multer = Multer({
    storage: Multer.memoryStorage()
});

module.exports = function (router, async) {

    router.get('/api/player', function (req, res, next) {
        Player.find().populate('school').exec(function (err, players) {
            if (err) { return next(err); }
            res.json(players);
        });
    });

    router.get('/api/player/:id', function (req, res, next) {
        Player.findById({ _id: req.params.id }, function (err, player) {
            if (err) { return next(err); }
            res.json(player);
        });
    });

    router.get("/api/player/school/:id", (req, res, next) => {
        Player.find({ school: req.params.id })
            .exec((err, players) => {
                if (err) { return next(err); }
                res.json(players);
            });
    });

    router.post('/api/player/school', function (req, res, next) {

        var dPlayer = new Player({
            firstname: (req.body.firstname).toUpperCase(),
            middlename: (req.body.middlename).toUpperCase(),
            lastname: (req.body.lastname).toUpperCase(),
            dateOfBirth: new Date(req.body.dateOfBirth),
            school: req.body.school,
            registeredBy: req.body.teacher
        });
        dPlayer.save(function (err) {
            if (err) { return next(err); }
            res.json({ response: "player registered" });
        });
    });

    router.get('/api/player/team/:id', (req, res, next) => {
        Player
            .find({ team: req.params.id })
            .exec((err, players) => {
                if (err) { return next(err); }
                res.json(players);
            });
    });

    router.post('/api/player/team', multer.single('picture'), (req, res, next) => {

        var player = new Player({
            firstname: (req.body.firstname).toUpperCase(),
            middlename: req.body.middlename,
            lastname: (req.body.lastname).toUpperCase(),
            dateOfBirth: req.body.dateOfBirth,
            team: req.body.teamId,
            registeredBy: req.user._id
        });

        player.save(function (err) {
            if (err) { return next(err); }
            res.json({ response: "player registered" });
        });
    });

    router.put('/api/player/:id', function (req, res, next) {

        Player.findById(req.params.id, function (err, player) {
            if (err) { return next(err); }

            _.merge(player, req.body);

            player.save(function (err) {
                if (err) { return next(err); }
                res.json({response: "player updated"});
            });
        });
    });

    router.delete('/api/player/:id', (req, res, next)=>{
        Player.findByIdAndRemove(req.params.id, (err)=>{
            if(err){return next(err);}
            res.json({response: "player removed"})
        })
    })
}
