'use strict';
const Showcase = require('../models/showcase.model');
const Multer = require('multer');
var imgUpload = require('../modules/imgUpload');
const multer = Multer({
  storage: Multer.memoryStorage()
});

module.exports = router=>{
  router.post('/api/showcase', multer.single('media'), imgUpload.uploadToGcs, (req, res, next)=>{
    if(req.file.cloudStoragePublicUrl){
      let new_showcase = new Showcase({
        media: req.file.cloudStoragePublicUrl,
        title: req.body.title,
        description: req.body.description
      });

      new_showcase.save(err=>{
        if(err){return next(err)}
        res.json({response: "showcase added"});
      })
    }else{
      res.status(502).json({error: "Internal server error. try again"});
    }
  });

  router.get('/api/showcase', (req, res, next)=>{
    Showcase
    .find()
    .exec((err, shows)=>{
      if(err){return next(err)}
      res.json(shows);
    });
  });

  router.get('/api/showcase/:id', (req, res, next)=>{
    Showcase
    .findById(req.params.id)
    .exec((err, show)=>{
      if(err){return next(err)}
      res.json(show);
    });
  });
}
