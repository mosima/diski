'use strict';
const Spotlight = require('../models/spotlight.model');
const Multer = require('multer');
var imgUpload = require('../modules/imgUpload');
const multer = Multer({
  storage: Multer.memoryStorage()
});

module.exports = router=>{
  router.post('/api/spotlight', multer.single('media'), imgUpload.uploadToGcs, (req, res, next)=>{
    if(req.file.cloudStoragePublicUrl){
      let new_spotlight = new Spotlight({
        media: req.file.cloudStoragePublicUrl,
        title: req.body.title,
        description: req.body.description
      });

      new_spotlight.save(err=>{
        if(err){return next(err)}
        res.json({response: "spotlight added"});
      })
    }else{
      res.status(502).json({error: "Internal server error. try again"});
    }
  });

  router.get('/api/spotlight', (req, res, next)=>{
    Spotlight
    .find()
    .exec((err, spots)=>{
      if(err){return next(err)}
      
      res.json(spots);
    });
  });

  router.get('/api/spotlight/:id', (req, res, next)=>{
    Spotlight
    .findById(req.params.id)
    .exec((err, spot)=>{
      if(err){return next(err)}
      res.json(spot);
    });
  });
}
