import {
    FETCH_USER,
    FETCH_POST,
    FETCH_SCHOOL,
    FETCH_PHASE,
    FETCH_FIXTURE,
    FETCH_NEWS,
    FETCH_TOURNAMENT,
    FETCH_PROVINCE,
    FETCH_SHOWCASE,
    FETCH_SPOTLIGHT,
    MY_TEAM, 
    MY_COACH,
    MY_PLAYER,
    FETCH_ALLTEAM,
    ONE_TOUR,
    FETCH_USERS,
    FETCH_RESULT
} from './types';

export const fetchUser = () => async dispatch => {
    try {
        const res = await fetch('/auth/get-current-user', { credentials: "include" });
        const data = await res.json();


        dispatch({ type: FETCH_USER, payload: data.user });
    } catch (err) {
        console.error(err.message);
    }
};

export const fetchUsers = () => async dispatch => {
    try {
        const res = await fetch('/users', { credentials: "include" });
        const data = await res.json();

        console.log('users',data);
        dispatch({ type: FETCH_USERS, payload: data });
    } catch (err) {
        console.error(err.message);
    }
};

export const fetchAllTeam = () => async dispatch => {
    try {
        const res = await fetch('/api/team', { credentials: "include" });
        const data = await res.json();


        dispatch({ type: FETCH_ALLTEAM, payload: data });
    } catch (err) {
        console.error(err.message);
    }
};

export const fetchTour = (id) => async dispatch => {
    try{
        const res = await fetch('/api/tournament/' + id, {credentials: "include"});
        const  data = await res.json();

        dispatch({type: ONE_TOUR, payload: data});
    }catch(err){
        console.error(err.message);
    }
};

export const fetchPost = () => async dispatch => {
    try {
        const res = await fetch('/api/post', { credentials: "include" });
        const data = await res.json();

        dispatch({ type: FETCH_POST, payload: data.data });
    } catch (err) {
        console.error(err.message);
    }
}

export const fetchMyTeam = () => async dispatch => {
    try {
        const res = await fetch('/api/team/my', { credentials: "include" });
        const data = await res.json();
        dispatch({ type: MY_TEAM, payload: data.teams });
    } catch (err) {
        console.error(err.message);
    }
}

export const fetchShowcase = () => async dispatch => {
    try {
        const res = await fetch('/api/showcase');
        const data = await res.json();

        dispatch({ type: FETCH_SHOWCASE, payload: data });
    } catch (err) {
        console.error(err.message);
    }
}

export const fetchSpotlight = () => async dispatch => {
    try {
        const res = await fetch('/api/spotlight');
        const data = await res.json();
        dispatch({ type: FETCH_SPOTLIGHT, payload: data });
    } catch (err) {
        console.error(err.message);
    }
}

export const fetchSchool = () => async dispatch => {
    try {
        const res = await fetch('/api/school');
        const data = await res.json();
        dispatch({ type: FETCH_SCHOOL, payload: data });
    } catch (err) {
        console.error(err.message);
    }
}

export const fetchMyCoach = () => async dispatch => {
    try {
        const res = await fetch('/api/coach/my', { credentials: "include" });
        const data = await res.json();
        dispatch({ type: MY_COACH, payload: data.coaches });
    } catch (err) {
        console.error(err.message);
    }
}

export const fetchMyPlayer = () => async dispatch => {
    try {
        const res = await fetch('/api/system/my', { credentials: "include" });
        const data = await res.json();
        dispatch({ type: MY_PLAYER, payload: data.players });
    } catch (err) {
        console.error(err);
    }
}

export const fetchPhase = () => async dispatch => {
    try {
        const res = await fetch('/api/phase');
        const data = await res.json();

        dispatch({ type: FETCH_PHASE, payload: data });
    } catch (err) {
        console.error(err.message);
    }
}

export const fetchProvince = () => async dispatch => {
    try {
        const res = await fetch('/api/province');
        const data = await res.json();
        dispatch({ type: FETCH_PROVINCE, payload: data.province });
    } catch (err) {
        console.error(err.message);
    }
}

export const fetchNews = () => async dispatch => {
    try {
        const res = await fetch('/api/news');
        const data = await res.json();

        dispatch({ type: FETCH_NEWS, payload: data });
    } catch (err) {
        console.error(err.message);
    }
}
export const fetchFixture = () => async dispatch => {
    try {
        const res = await fetch('/api/fixture');
        const data = await res.json();

        dispatch({ type: FETCH_FIXTURE, payload: data });
    } catch (err) {
        console.error(err.message);
    }
}
export const fetchTournament = () => async dispatch => {
    try {
        const res = await fetch('/api/tournament');
        const data = await res.json();

        dispatch({ type: FETCH_TOURNAMENT, payload: data });
    } catch (err) {
        console.error(err.message);
    }
}
export const fetchResults = () => async dispatch => {
    try {
        const res = await fetch('/api/result', { credentials: "include" });
        const data = await res.json();

        console.log('result',data);
        dispatch({ type: FETCH_RESULT, payload: data });
    } catch (err) {
        console.error(err.message);
    }
};