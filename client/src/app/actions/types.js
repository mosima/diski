export const FETCH_USER = 'fetch_user';
export const FETCH_USERS = 'fetch_users';
export const FETCH_POST = 'fetch_post';
export const FETCH_NEWS = 'fetch_news';
export const FETCH_TOURNAMENT = 'fetch_tournament';
export const FETCH_FIXTURE = 'fetch_fixture';
export const FETCH_PROVINCE = 'fetch_province';
export const FETCH_PHASE = 'fetch_phase';
export const FETCH_DISTRICT = 'fetch_district';
export const FETCH_SCHOOL = 'fetch_school';
export const FETCH_SPOTLIGHT = 'fetch_spotlight';
export const FETCH_SHOWCASE = 'fetch_showcase';
export const MY_TEAM = "fetch_my_team";
export const MY_COACH = "fetch_my_coach";
export const MY_PLAYER = "fetch_my_player";
export const ONE_TOUR = "fetch_tour";
export const FETCH_ALLTEAM = "fetch_all_team";
export const FETCH_RESULT = "fecth_all_results"
