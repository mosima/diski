import React, { Component } from 'react';
import { Button } from 'reactstrap';

export default class CreateFooter extends Component {
    render() {
        return (
            <div>
                <Button outline color={this.props.firstColor} onClick={()=>{this.props.action()}}>{this.props.first}</Button>{' '}
                <Button outline color={this.props.secondColor} onClick={()=>{this.props.close()}}>{this.props.second}</Button>
            </div>
        )
    }
}