import React, { Component } from 'react';
import { FormGroup, FormText, Input, Label } from 'reactstrap';

export default class FormInput extends Component {
    render() {
        const {label, inputProps} = this.props
        return (
            <FormGroup>
                <Label>{label}</Label>
                <Input 
                    {...inputProps}
                />
            </FormGroup>
        )
    }
}