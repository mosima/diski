import {combineReducers} from 'redux';
import authReducer from './authReducer';
import postReducer from './postReducer';
import fixtureReducer from './fixtureReducer';
import newsReducer from './newsReducer';
import tournamentReducer from './tournamentReducer';
import provinceReducer from './provinceReducer';
import phaseReducer from './phaseReducer';
import schoolReducer from './schoolReducer';
import showcaseReducer from './showcaseReducer';
import spotlightReducer from './spotlightReducer';
import myTeamReducer from './myTeamReducer';
import myCoach from './myCoachReducer';
import myPlayerReducer from './myPlayerReducer';
import oneTourReducer from './oneTourReducer';
import allTeamReducer from './allTeamReducer';
import usersReducers from './usersReducers';
import resultReducer from './resultsReducer';

export default combineReducers({
    auth: authReducer,
    posts: postReducer,
    news: newsReducer,
    tour: oneTourReducer,
    province: provinceReducer,
    fixtures: fixtureReducer,
    tournaments: tournamentReducer,
    phases: phaseReducer,
    schools: schoolReducer,
    spotlights: spotlightReducer,
    showcases: showcaseReducer,
    teams: allTeamReducer,
    my_teams: myTeamReducer,
    my_coaches: myCoach,
    my_players: myPlayerReducer,
    users: usersReducers,
    results :resultReducer
});