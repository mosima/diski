import {FETCH_SPOTLIGHT} from '../actions/types';

export default function(state = [], action){
    switch(action.type){
        case FETCH_SPOTLIGHT:
        return action.payload || false;
         default:
        return state;
    }
}