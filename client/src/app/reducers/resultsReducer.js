import {FETCH_RESULT} from '../actions/types';

export default function(state = [], action){
    switch(action.type){
        case FETCH_RESULT:
        return action.payload || false;
         default:
        return state;
    }
}