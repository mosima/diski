import React, {Component} from 'react';
import { withStyles } from '@material-ui/core';
import {format} from 'date-fns';
import {Chip, List, ListItem, ListItemSecondaryAction, ListItemText, SvgIcon, Icon, FormControlLabel as Label} from '@material-ui/core';
const styles = theme => ({
    root: {
      width: '100%',
      maxWidth: 360,
      background: theme.palette.background.paper,
    },
  });

class FixtureListItem extends Component{
    render(){
        const { classes } = this.props;        
        return(
            <div>
                <Label as='a' icon>
                <Icon name="shield" />
                {(((this.props.fixture.home_team.Institution_Name).substring(0,3)).toUpperCase()).replace('?', 'E')}
                </Label>
                <Label>
                {format(this.props.fixture.match_date, 'Do MMM')}
                </Label>
                <Label as='a' icon>
                <Icon name="shield" />
                {(((this.props.fixture.away_team.Institution_Name).substring(0,3)).toUpperCase()).replace('?', 'E')}
                </Label>
            </div>
        )
    }
}

export default withStyles(styles)(FixtureListItem);