import React, { Component } from 'react';
import {
    Container, Row, Col,
    Button,

} from 'reactstrap';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import FixtureModal from './FixtureModal';

class Fixture extends Component {
    constructor(props) {
        super(props);

        this.state = {
            groups: [],
            cup_teams: [],
            group: null,
            modal: false
        }
        this.getCupTeams = this.getCupTeams.bind(this);
        this.getGroups = this.getGroups.bind(this);
        this.toggle = this.toggle.bind(this);
        this.setGroup = this.setGroup.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }
    componentDidMount() {
        this.props.fetchTour(this.props.match.params.tourId);
        this.getCupTeams();
        this.getGroups();
    }

    async getCupTeams() {
        try {
            let response = await fetch('/api/tournament/cup-team/' + this.props.match.params.tourId);
            let result = await response.json();
            this.setState({
                cup_teams: result
            });
        } catch (error) {
            console.error(error);
        }
    }
    async getGroups() {
        try {
            let response = await fetch('/api/group/' + this.props.match.params.tourId);
            let result = await response.json();
            console.log("group", result);
            this.setState({
                groups: result
            });
        } catch (error) {
            console.error(error);

        }
    }

    setGroup(id) {
        for (let i = 0; i < this.state.groups.length; i++) {
            if (this.state.groups[i]._id === id) {
                this.setState({
                    group: this.state.groups[i]
                });
                break;
            }
        }
    }
    render() {
        return (
            <div>
                <Container fluid>
                    <Row>
                        <Col>
                            <h3 className="lead">{this.props.tour.name}</h3>
                        </Col>
                    </Row>
                    <hr />
                    <FixtureModal 
                        toggle={this.toggle} 
                        open={this.state.modal} 
                        setGroup={this.setGroup}
                        group={this.state.group}
                        teams={this.state.group ? this.state.group.teams : null}
                        groups={this.state.groups}
                    />
                    <Row>
                        <Col>
                            <Button onClick={this.toggle} outline color="danger">Create New Fixture</Button>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        tour: state.tour
    }
}

export default connect(mapStateToProps, actions)(Fixture);