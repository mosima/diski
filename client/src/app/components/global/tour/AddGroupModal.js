import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Form } from 'reactstrap';
import { Create, FormInput } from '../../../util';
import {withRouter} from 'react-router-dom';

class AddGroup extends Component{
    constructor(props){
        super(props);
        this.createGroup = this.createGroup.bind(this);
    }
    render(){
        return(
            <Modal isOpen={this.props.open} toggle={this.props.toggle}>
                <ModalHeader toggle={this.props.toggle}>Add Group</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormInput
                            inputProps={{
                                onChange: (e)=>{this.setState({name: e.target.value})},
                                placeholder: 'e.g Group A'
                            }}
                        />
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Create
                        first="Submit"
                        second="Close"
                        firstColor="success"
                        secondColor="danger"
                        action={this.createGroup}
                        close={this.props.toggle}
                    />
                </ModalFooter>
            </Modal>
        )
    }
    async createGroup(){
        try {
            let response = await fetch('/api/group',{
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({name: this.state.name, tournament: this.props.match.params.tourId})
            });
            let result = await response.json();
            this.props.get();
            this.props.toggle();
        } catch (error) {
            console.error(error);
        }
    }
}

export default withRouter(AddGroup);