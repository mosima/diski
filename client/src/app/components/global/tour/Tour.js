import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input
} from "reactstrap";
import { Create, FormInput } from "../../../util";

import { connect } from "react-redux";
import * as action from "../../../actions";
import TourRow from "./TourRow";
import PerfectScrollbar from "react-perfect-scrollbar";

class Tour extends Component {
  constructor(props) {
    super(props);
    this.state = {
      create: false,
      tourType: ['Cup','League','Exhibition'],
      type: ''
    };
    this.toggleCreate = this.toggleCreate.bind(this);
    this.setInput = this.setInput.bind(this);
    this.createTournament = this.createTournament.bind(this);
  }
  toggleCreate() {
    this.setState({ create: !this.state.create });
  }
  setInput(obj) {
    this.setState(obj);
  }

  componentDidMount(){
  }

  render() {

    return (
      
      <div>
        <Container fluid>
          <Row>
            <Modal isOpen={this.state.create} toggle={this.toggleCreate}>
              <ModalHeader className="lead" toggle={this.toggleCreate}>
                Create Tournament
              </ModalHeader>
              <ModalBody>
                <Form>
                  <FormInput
                    label="Name of Tournament"
                    inputProps={{
                      require: true,
                      onChange: e => {
                        this.setState({ name: e.target.value }, () => {
                          console.log(this.state);
                        });
                      }
                    }}
                  />
                  <FormInput
                    label="Number of Teams"
                    inputProps={{
                      type: "number",
                      min: 2,
                      onChange: e => {
                        this.setState({ no_of_team: e.target.value }, () => {
                          console.log(this.state);
                        });
                      }
                    }}
                  />
                  <FormGroup>
                  <Label for="exampleSelect">Select Type</Label>
                  <Input type="select" defaultValue={"Select"}  id="exampleSelect" onChange={(e)=>{this.setState({type: e.target.value})}}>
                    {
                      this.state.tourType.map((type, index)=>{
                        return(
                          <option value={type} key={index}>{type}</option>
                        )
                      })
                    }
                  </Input>
                </FormGroup>
                </Form>
              </ModalBody>
              <ModalFooter>
                <Create
                  first="Create Tournament"
                  second="Close"
                  action={this.createTournament}
                  close={this.toggleCreate}
                  firstColor="success"
                  secondColor="danger"
                />
              </ModalFooter>
            </Modal>
            <Col>
              <Button
                onClick={this.toggleCreate}
                outline
                color="danger"
                style={{ border: "none" }}
              >
                <i className="fa fa-plus" />
              </Button>
            </Col>
          </Row>
          <Row>
            <PerfectScrollbar>
              <Table bordered>
                <thead>
                  <tr>
                    <th>Tournament</th>
                    <th>No. of Teams</th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {this.props.tournaments.map((t, i) => {
                    return <TourRow key={i} item={t} />;
                  })}
                </tbody>
              </Table>
            </PerfectScrollbar>
          </Row>
        </Container>
      </div>
    );
  }
  async createTournament() {
    let obj = {
      name: this.state.name,
      type: this.state.type.toLowerCase()
    };
    if (this.state.no_of_team) {
      obj.numberOfTeams = this.state.no_of_team;
    }
    try {
      let response = await fetch("/api/tournament", {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(obj)
      });
      let result = await response.json();
      this.props.fetchTournament();
      this.toggleCreate();
      console.log(result);
    } catch (error) {
      console.error(error);
    }
  }
}
function mapStateToProps(state) {
  return {
    tournaments: state.tournaments
  };
}

export default connect(
  mapStateToProps,
  action
)(Tour);
