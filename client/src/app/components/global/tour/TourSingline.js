import React, { Component } from 'react';
import {
    Container, Row, Col, Button,
    Card, CardHeader, CardTitle, CardText, CardBody, CardFooter,
    ListGroupItem
} from 'reactstrap';
import { connect } from 'react-redux';
import * as actions from "../../../actions";
import RegisterTeam from './AddTeamModal';
import CreateGroup from './AddGroupModal';
import AddGroupTeam from './AddGroupTeam';
import GroupRow from './group/GroupRow';

class TourSingle extends Component {
    constructor(props) {
        super(props);

        this.state = {
            addTeam: false,
            cup_teams: [],
            addGroup: false,
            groups: [],
            groupId: null,
            addTeamToGroup: false
        }
        this.addTeamModal = this.addTeamModal.bind(this);
        this.addGroupModal = this.addGroupModal.bind(this);
        this.getCupTeams = this.getCupTeams.bind(this);
        this.getGroups = this.getGroups.bind(this);
        this.joinTeam = this.joinTeam.bind(this);
    }
    componentDidMount() {
        this.props.fetchTour(this.props.match.params.tourId);
        this.getCupTeams();
        this.getGroups();
    }
    addGroupModal() {
        this.setState({ addGroup: !this.state.addGroup });
    }

    addTeamModal() {
        this.setState({ addTeam: !this.state.addTeam });
    }

    joinTeam() {
        this.setState({ addTeamToGroup: !this.state.addTeamToGroup });
    }
    async getCupTeams() {
        try {
            let response = await fetch('/api/tournament/cup-team/' + this.props.match.params.tourId);
            let result = await response.json();
            this.setState({
                cup_teams: result
            });
        } catch (error) {
            console.error(error);
        }
    }
    async getGroups() {
        try {
            let response = await fetch('/api/group/' + this.props.match.params.tourId);
            let result = await response.json();
            this.setState({
                groups: result
            });
        } catch (error) {
            console.error(error);

        }
    }

    render() {
        return (
            <div>
                <Container fluid>
                    <Row>
                        <Col>
                            <h3 className="lead">{this.props.tour.name}</h3>
                        </Col>
                    </Row>
                    <hr />
                    <Row>
                        <Col>
                            <Button onClick={()=>{this.props.history.push('/global-admin/fixture/' + this.props.match.params.tourId)}} outline color="danger">View Fixtures</Button>{'  '}
                            <Button outline color="danger">View Result</Button>
                        </Col>
                    </Row>
                    <hr />
                    <CreateGroup open={this.state.addGroup} toggle={this.addGroupModal} get={this.getGroups} />
                    <RegisterTeam getTeam={this.getCupTeams} open={this.state.addTeam} tour={this.props.match.params.tourId} toggle={this.addTeamModal} />
                    {this.state.cup_teams.length > 0 && <AddGroupTeam open={this.state.addTeamToGroup} toggle={this.joinTeam} group={this.state.groupId} teams={this.state.cup_teams} get={this.getGroups} />}
                    <Row>
                        <Col md={6}>
                            <Card>
                                <CardHeader>
                                    <CardTitle className="float-left lead">Registered Teams</CardTitle>
                                    <Button onClick={this.addTeamModal} style={{ border: 'none' }} outline color="danger" className="float-right"><i className="fa fa-plus"></i></Button>
                                </CardHeader>
                                <CardBody>
                                    <div style={{ height: 200, overflowY: 'scroll' }}>
                                        {
                                            this.state.cup_teams.map((c, i) => {
                                                return (
                                                    <ListGroupItem key={i}>{c.team.name}</ListGroupItem>
                                                )
                                            })
                                        }
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                        {(this.props.tour.type === "cup") && <Col md={6}>
                            <Card>
                                <CardHeader>
                                    <CardTitle className="float-left lead">Group</CardTitle>
                                    <Button onClick={this.addGroupModal} style={{ border: 'none' }} outline color="danger" className="float-right"><i className="fa fa-plus"></i></Button>
                                </CardHeader>
                                <CardBody>
                                    {
                                        this.state.groups.map((g, i) => {
                                            return (
                                                <Row key={i}>
                                                    <Col>
                                                        <CardText className="float-left">{g.name}</CardText>
                                                        <Button outline color="danger" size="sm" onClick={() => { this.setState({ groupId: g._id }, () => { this.joinTeam() }) }} className="float-right">ADD TEAM</Button>
                                                    </Col>
                                                </Row>
                                            )
                                        })
                                    }
                                </CardBody>
                            </Card>
                        </Col>}
                    </Row>
                    <hr />
                    {
                        this.state.groups && <div>
                            {
                                this.state.groups.map((g, i) => {
                                    return (
                                        <GroupRow group={g} key={i} />
                                    )
                                })
                            }
                        </div>
                    }
                </Container>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        tour: state.tour,
        teams: state.teams
    }
}

export default connect(mapStateToProps, actions)(TourSingle);
