import React, { Component } from 'react';
import { Button } from 'reactstrap';
import {withRouter} from 'react-router-dom';


class TourRow extends Component {
    render() {
        return (
            <tr>
                <td>{this.props.item.name}</td>
                <td>{this.props.item.numberOfTeams}</td>
                <td>
                    <Button outline color="danger">View</Button>{' '}
                    <Button outline color="danger">Edit</Button>{' '}
                    <Button outline onClick={()=>{this.props.history.push("/global-admin/tournament/" + this.props.item._id)}} color="danger">Manage</Button>{' '}
                    <Button outline color="danger">Delete</Button>
                </td>
            </tr>
        )
    }
}

export default withRouter(TourRow);