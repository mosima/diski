import React, {Component} from 'react';

class GroupTeamRow extends Component{
    render(){
        return(
            <tr>
                <td>{this.props.team.team.name}</td>
                <td>{this.props.team.match_played}</td>
                <td>{this.props.team.match_won}</td>
                <td>{this.props.team.match_drawn}</td>
                <td>{this.props.team.match_lost}</td>
                <td>{this.props.team.goal_scored}</td>
                <td>{this.props.team.goal_conceded}</td>
                <td>{this.props.team.goal_scored - this.props.team.goal_conceded}</td>
                <td>{this.props.team.point}</td>
            </tr>
        )
    }
}

export default GroupTeamRow;