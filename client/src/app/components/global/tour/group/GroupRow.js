import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody, CardFooter, Button, Table } from 'reactstrap';
import TeamRow from './Row';

class GroupRow extends Component {
    render() {
        return (
            <Row style={{marginBottom: 10}}>
                <Col>
                    <Card>
                        <CardHeader className="lead">{this.props.group.name}</CardHeader>
                        <CardBody>
                            <Table hover color="danger" bordered striped>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>P</th>
                                        <th>W</th>
                                        <th>D</th>
                                        <th>L</th>
                                        <th>GF</th>
                                        <th>GA</th>
                                        <th>GD</th>
                                        <th>PTS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.props.group.teams.map((t, i)=>{
                                            return <TeamRow team={t} key={i} />
                                        })
                                    }
                                </tbody>
                                
                            </Table>
                        </CardBody>
                        <CardFooter></CardFooter>
                    </Card>
                </Col>
            </Row>
        )
    }
}

export default GroupRow;