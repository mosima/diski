import React, { Component } from 'react';
import {
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form, FormGroup, Label, Input
} from 'reactstrap';
import { Create, FormInput } from '../../../util';
import {withRouter} from 'react-router-dom';

class FixtureModal extends Component {
    constructor(props){
        super(props);
        this.createFixture = this.createFixture.bind(this);
    }
    render() {
        return (
            <Modal isOpen={this.props.open} toggle={this.props.toggle}>
                <ModalHeader toggle={this.props.toggle} className="lead">Create New Fixture</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormInput
                            label="Match Date"
                            inputProps={{
                                type: "datetime-local",
                                onChange: (e) => { this.setState({ match_date: e.target.value }) }
                            }}
                        />
                        <FormGroup>
                            <Label>Group</Label>
                            <Input type="select"
                                onChange={(e) => {
                                    this.setState({
                                        id: e.target.value
                                    }, () => {
                                        this.props.setGroup(this.state.id)
                                    });
                                }}>
                                <option>Select Group</option>
                                {
                                    this.props.groups.map((g, i) => {
                                        return (
                                            <option value={g._id} key={i}>{g.name}</option>
                                        )
                                    })
                                }
                            </Input>
                        </FormGroup>
                        {this.props.group &&
                            <div>
                                <FormGroup>
                                    <Label>Home Team</Label>
                                    <Input type="select"
                                        onChange={(e) => {
                                            this.setState({
                                                home_team: e.target.value
                                            });
                                        }}>
                                        <option>Select Team</option>
                                        {
                                            this.props.teams.map((g, i) => {
                                                return (
                                                    <option value={g._id} key={i}>{g.team.name}</option>
                                                )
                                            })
                                        }
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Away Team</Label>
                                    <Input type="select"
                                        onChange={(e) => {
                                            this.setState({
                                                away_team: e.target.value
                                            });
                                        }}>
                                        <option>Select Team</option>
                                        {
                                            this.props.teams.map((g, i) => {
                                                return (
                                                    <option value={g._id} key={i}>{g.team.name}</option>
                                                )
                                            })
                                        }
                                    </Input>
                                </FormGroup>
                            </div>
                        }
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Create
                        first="Create Fixture"
                        second="Close"
                        firstColor="success"
                        secondColor="danger"
                        close={this.props.toggle}
                        action={this.createFixture}
                    />
                </ModalFooter>
            </Modal>
        )
    }
    async createFixture() {
        let obj = {
            cup_team_home: this.state.home_team,
            cup_team_away: this.state.away_team,
            tournament: this.props.match.params.tourId,
            group: this.props.group._id,
            match_date: this.state.match_date
        }

        try {
            let response = await fetch('/api/fixture', {
                method: "POST",
                credentials: "include",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(obj)
            });
            let result = await response.json();
            this.props.toggle();
        } catch (error) {
            console.error(error);
            
        }
    }
}

export default withRouter(FixtureModal);