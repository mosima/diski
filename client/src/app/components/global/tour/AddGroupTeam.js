import React, { Component } from 'react';
import {
    Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';
import { Create } from '../../../util';
import { connect } from 'react-redux';
import Select from 'react-select';
import { withRouter } from 'react-router-dom';

class AddGroupTeam extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedOption: '',
        }
        this.addTeam = this.addTeam.bind(this);

    }
    handleChange = (selectedOption) => {
        this.setState({ selectedOption });
    }
    render() {
        return (
            <Modal isOpen={this.props.open} toggle={this.props.toggle}>
                <ModalHeader className="lead">Register Team For Tournament</ModalHeader>
                <ModalBody>
                    <Select
                        name="form-field-name"
                        value={this.state.selectedOption}
                        onChange={this.handleChange}
                        options={this.props.teams.map(t => { return { value: t._id, label: t.team.name } })}
                        isMulti
                    />
                </ModalBody>
                <ModalFooter>
                    <Create
                        first="Register"
                        firstColor="success"
                        second="Close"
                        secondColor="danger"
                        close={this.props.toggle}
                        action={this.addTeam}
                    />
                </ModalFooter>
            </Modal>
        )
    }
    async addTeam() {
        try {
            let response = await fetch('/api/group/add-team/' + this.props.group, {
                method: "PUT",
                credentials: "include",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(this.state)
            });
            let result = await response.json();
            console.log(result);
            this.setState({
                selectedOption: ''
            })
            this.props.toggle();
            this.props.get();
        } catch (error) {
            console.error(error);
        }
    }
}


export default withRouter(AddGroupTeam);