import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Table,

} from "reactstrap";
//import { Create, FormInput } from "../../../util";

import { connect } from "react-redux";
import * as action from "../../../actions";
import UserRow from "./UserRow";
import PerfectScrollbar from "react-perfect-scrollbar";

class Tour extends Component {
  constructor(props) {
    super(props);


  }

  render() {
      console.log('user', this.props.tournaments);
    return (
      <div>
        <Container fluid>
          <Row>
           
            <Col>
            </Col>
          </Row>
          <Row>
            <PerfectScrollbar>
              <Table bordered>
                <thead>
                  <tr>
                    <th>Tournament</th>
                    <th>No. of Teams</th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {this.props.users.map((t, i) => {
                    return <UserRow key={i} item={t} />;
                  })}
                </tbody>
              </Table>
            </PerfectScrollbar>
          </Row>
        </Container>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    tournaments: state.tournament,
    users: state.users
  };
}

export default connect(
  mapStateToProps,
  action
)(Tour);
