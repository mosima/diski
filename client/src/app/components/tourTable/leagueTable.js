import React, {Component} from 'react'
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import { Divider, Tab } from 'semantic-ui-react';
import DistrictComponent from '../district/districtCard';

const colors = [
  'orangered', 'orange', 'yellow', 'olive', 'green', 'teal',
  'blue', 'violet', 'purple', 'pink', 'brown', 'grey',
];

const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    table: {
      minWidth: 700,
    },
  });

class LeagueTable extends Component{
    constructor(props){
      super(props);
      this.state = {
        color: colors[0]
      }
    }
    render(){
      const {color} = this.state;
            const phases = this.props.phases.map((item, i)=>{
              return(
                { menuItem: item.phase, render: () => <DistrictComponent fp={this.props.fp} teams={[]} phase={item.phase} province={this.props.province} key={i} /> }
              )
            });

          
        const { classes } = this.props;
        return (
            <section>
              <Divider hidden />

              <Tab
                menu={{ color, inverted: true, attached: true, tabular: false }}
                panes={phases}
              />
            
          </section>
        )
    }

    
    componentDidMount(){
    //  this.getDistrict();
    }
}

export default withStyles(styles)(LeagueTable);