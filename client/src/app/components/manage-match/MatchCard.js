import React, { Component } from 'react';
import { Row, Col, Button, ButtonGroup, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { format } from 'date-fns';

class FixtureCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render() {
        return (

            <div>


                <Row style={{ marginBottom: 10 }}>
                    <Col md={2} sm={12} xs={12} />
                    <Col sm={8}>
                        <ButtonGroup
                        >
                            <Button style={{
                                textAlign: "center",
                                width: 300,
                                borderBottomLeftRadius: 30
                            }} size="md" color="danger">
                                <span
                                    className="lead"

                                >
                                    {this.props.fixture.cup_team_home.team.name}
                                </span>
                            </Button>
                            <Button size="md" color="warning">
                                1-0
                        </Button>
                            <Button style={{
                                textAlign: "center",
                                width: 300,
                                borderBottomRightRadius: 30
                            }} size="md" color="danger">
                                <span
                                    className="lead"

                                >
                                    {this.props.fixture.cup_team_away.team.name}
                                </span>
                            </Button>
                        </ButtonGroup>

                    </Col>
                </Row>
                <div>
                    <Button color="danger" onClick={this.toggle}> Update goal{this.props.buttonLabel}</Button>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                        <ModalHeader toggle={this.toggle}>Add Goal</ModalHeader>
                        <ModalBody>
                            <FormGroup>
                                <Label for="exampleSelect">Team Name</Label>
                                <Input type="select" name="select" id="exampleSelect">
                                    <option>--Select--</option>
                                    <option>ITthynk FC</option>
                                    <option>Checha Consulting FC</option>
                                    <option>FITS FC</option>
                                    <option>Dynamics FC</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Player Name</Label>
                                <Input type="select" name="select" id="exampleSelect">
                                    <option>--Select--</option>
                                    <option>Messi</option>
                                    <option>Ronaldo</option>
                                    <option>Neymer jr.</option>
                                    <option>Tau</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Scored  minute</Label>
                                <Input type="number" name="select" id="exampleSelect" min="1" max="120">
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Goal Number</Label>
                                <Input type="number" name="select" id="exampleSelect" min="0" max="40">
                                </Input>
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="danger" onClick={this.toggle}>Update</Button>{' '}
                            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                        </ModalFooter>
                    </Modal>
                </div>
            </div>

        );
    }
}

export default FixtureCard;