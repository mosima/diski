import React, {Component} from 'react';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import { Tab } from 'semantic-ui-react';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';

const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    table: {
      minWidth: 700,
    },
  });

class DistrictComponent extends Component{
    constructor(props){
        super(props);

        this.state = {
            schools: []
        }
    }
    render(){
        const { classes } = this.props;
        return(
        <Tab.Pane attached={false}>
        <Paper className={classes.root}>
            <center>
                <h3 className="page-header">{(this.props.fp).toUpperCase()}</h3>
            </center>
            <Table className={classes.table}>
                <TableHead>
                <TableRow>
                    <TableCell>SCHOOL</TableCell>
                    <TableCell>P</TableCell>
                    <TableCell>W</TableCell>
                    <TableCell>L</TableCell>
                    <TableCell>D</TableCell>
                    <TableCell>GF</TableCell>
                    <TableCell>GA</TableCell>
                    <TableCell>+/-</TableCell>
                    <TableCell>Pts</TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {

                    this.state.schools.map(item=>{
                    return(
                        <TableRow key={item._id}>
                        <TableCell>{item.Institution_Name}</TableCell>
                        <TableCell>{item.season_games_played}</TableCell>
                        <TableCell>{item.games_won}</TableCell>
                        <TableCell>{item.games_lost}</TableCell>
                        <TableCell>{item.games_drawn}</TableCell>
                        <TableCell>{item.season_goal_for}</TableCell>
                        <TableCell>{item.season_goal_against}</TableCell>
                        <TableCell>{item.season_goal_for - item.season_goal_against}</TableCell>
                        <TableCell>{item.point}</TableCell>
                        </TableRow>
                    
                    )
                    })
                }
                </TableBody>
            </Table>
</Paper>
      </Tab.Pane>
      )
    }
    async getSchool(){
        let response = await fetch('/api/school/' + this.props.province + '/' + this.props.phase);
         let result = await response.json();
         this.setState({
           schools: result
         });
       }
    componentDidMount(){
        this.getSchool();
    }
}

export default withStyles()(DistrictComponent);