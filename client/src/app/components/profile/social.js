import React, {Component} from 'react';
import PostContainer from '../post/postContainer';
import {SvgIcon} from 'material-ui';
import {Form, Button, Icon, Segment, Image, Dimmer, Loader} from 'semantic-ui-react';
import {connect} from 'react-redux';
import * as actions from '../../actions';
const loading = require('../../assets/loading.gif');

class SocialHomeComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            open: false,
            posts: [],
            processing: false
        }
        this.createPost = this.createPost.bind(this);
    }
    async getPosts(){
        
        let res = await fetch('/api/post/user/' + this.props.user._id);
        let data = await res.json();
        this.setState({
            posts: data
        });
    }
    

    render(){
        return(
            <div>
                <Segment>
                    <Dimmer active={this.state.processing}>
                        <Loader>Processing</Loader>
                    </Dimmer>
                <Form onSubmit={this.createPost}>
                    <Form.TextArea
                    placeholder="Share your thoughts"
                    required
                    onChange={(e)=>{this.setState({postBody: e.target.value})}}
                    label="What's on your mind"
                    />
                    <Form.Input
                    type="file"
                    required
                    onChange={(e)=>{this.setState({file: e.target.files[0]})}}
                    />
                    <Button type="submit" basic icon><Icon name="send outline" /></Button>

                </Form>
                </Segment>
                {this.props.posts.length > 0
                ?
                <PostContainer posts={this.props.posts} />
                :
                <Image src={loading} />
                
                }
                
            </div>
        )
    }

    async createPost(){
        this.setState({
            processing: true
        });
        let obj = {
            postbody: this.state.postBody,
            owner: this.props.user._id,
            file: this.state.file
        }
        let formData = new FormData();
        formData.append("postBody", obj.postbody);
        formData.append("file", obj.file);
        formData.append("owner", obj.owner);

       let response = await fetch("/api/post/new", {
            method: "POST",
            credentials: "include",
            body: formData
        })
        let result = await response.json();
            if(result){
                this.setState({
                    processing: false
                });
                this.props.fetchPost();
            }
        
        
    }
}

function matchStateToProps(state){
    return {
        user: state.auth,
        posts: state.posts,
        news: state.news
    }
}

export default connect(matchStateToProps, actions)(SocialHomeComponent);