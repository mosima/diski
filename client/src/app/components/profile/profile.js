import React, {Component} from 'react';
import { withStyles } from 'material-ui/styles';
import {IconButton, Switch, TextField, MenuItem, Paper, Typography} from 'material-ui';
import SaveIcon from 'material-ui-icons/Save';
import BackIcon from 'material-ui-icons/ArrowBack';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import * as actions from '../../actions/index';

const styles = theme => ({
    button: {
      margin: theme.spacing.unit,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 400
      }
  });
class SocialProfileComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            phase: null,
            school: null,
            province: null,
            district: null,
            schoolArr: [],
            schoolActive: true,
            districtArr: [],
            districtActive: true
        }
        this.handleChange = this.handleChange.bind(this);
    }
    render(){
        const { classes } = this.props;
        return(
            <div>
                <IconButton onClick={()=>{this.props.history.push('/social/')}} aria-label="back" className={classes.button}>
                    <BackIcon />
                </IconButton>
                <IconButton onClick={this.updateProfile.bind(this)} aria-label="save" className={classes.button}>
                    <SaveIcon />
                </IconButton>
                <Paper className={classes.root} elevation={4} style={{padding: 10}}>
                 
                <Typography type="headline" component="h3">Edit Your Profile Information</Typography>
                Become A Teacher?
                <Switch
                    checked={this.props.user.isAdmin || false}
                    onChange={this.registerTeacher.bind(this)}
                    aria-label="Teacher"
                    /><br />
                    <TextField
                    label="DISPLAY NAME"
                    placeholder="Display Name"
                    value={this.props.user.displayName}
                    className={classes.textField}
                    margin="normal"
                    onChange={this.handleChange('displayName')}
                    />    
                <TextField
                    label="ADDRESS"
                    placeholder="Address"
                    className={classes.textField}
                    margin="normal"
                    
                    />
                    <TextField
                    id="select-province"
                    select
                    label="PROVINCE"
                    className={classes.textField}
                    value={this.state.province || null}
                    onChange={this.handleChange('province')}
                    SelectProps={{
                        MenuProps: {
                        className: classes.menu,
                        },
                    }}
                    helperText="Please select your province"
                    margin="normal"
                    >
                    {this.props.province.map(option => (
                        <MenuItem key={option._id} value={option.short_name}>
                        {(option.name).toUpperCase()}
                        </MenuItem>
                    ))}
                    </TextField>
                    <TextField
                    id="select-district"
                    select
                    label="DISTRICT"
                    className={classes.textField}
                    value={this.state.district}
                    onChange={this.handleChange('district')}
                    SelectProps={{
                        MenuProps: {
                        className: classes.menu,
                        },
                    }}
                    disabled={this.state.districtActive}
                    helperText="Please select your district"
                    margin="normal"
                    >
                    {this.state.districtArr.map(item => (
                        <MenuItem key={item._id} value={item.district}>{(item.district).toUpperCase()}</MenuItem>
                    ))}
                    </TextField>
                    <TextField
                    label="SUBURB"
                    placeholder="Suburb"
                    defaultValue={this.props.user.suburb}
                    className={classes.textField}
                    margin="normal"
                    onChange={this.handleChange('suburb')}
                    />
                    {
                        (()=>{
                            if(this.props.user.isAdmin){
                                return (
                                    <div>
                                        <TextField
                    id="select-phase"
                    select
                    label="PHASE"
                    className={classes.textField}
                    value={this.state.phase}
                    onChange={this.handleChange('phase')}
                    SelectProps={{
                        MenuProps: {
                        className: classes.menu,
                        },
                    }}
                    disabled={this.state.districtActive}
                    helperText="Please select your phase"
                    margin="normal"
                    >
                    {this.props.phases.map(item => (
                        <MenuItem key={item._id} value={item.phase}>{(item.phase).toUpperCase()}</MenuItem>
                    ))}
                    </TextField>
                    <TextField
                    id="select-school"
                    select
                    label="SCHOOL"
                    className={classes.textField}
                    value={this.state.school}
                    onChange={this.handleChange('school')}
                    SelectProps={{
                        MenuProps: {
                        className: classes.menu,
                        },
                    }}
                    disabled={this.state.schoolActive}
                    helperText="Please select your school"
                    margin="normal"
                    >
                    {this.state.schoolArr.map(item => (
                        <MenuItem key={item._id} value={item._id}>{(item.Institution_Name).toUpperCase()}</MenuItem>
                    ))}
                    </TextField>
                                    </div>
                                )
                            }
                        })()
                    }
                        
                </Paper>
                
            </div>
        )
    }
    async updateProfile(){
        let obj = {
            school: this.state.school,
            province: this.state.province,
            suburb: this.state.suburb,
            district: this.state.district,
            displayName: this.state.displayName,
            address: this.state.address
        }
        let response = await fetch('/user/edit-profile', {
            credentials: "include",
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(obj)
        });
        let result = await response.json();
        this.props.history.push('/social/');
    }

    async registerTeacher(){
        let res = await fetch('/user/register-teacher', {credentials: "include"});
        let ris = await res.json();
        this.props.fetchUser();
    }

    async getSchool(){
        let response = await fetch('/api/school/district/' + this.state.district + '/' + this.state.phase);
        let result = await response.json();
        this.setState({
            schoolArr: result
        });
    }

    handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        }, ()=>{
            if(name == 'province'){
                
                this.setState({
                    districtActive: false
                })
            this.getDistrict(this.state.province);
            }else if(name == 'phase'){
                this.setState({
                    schoolActive: false
                })
                this.getSchool()
            }
        });
      };
    async getDistrict(province){
        let response = await fetch('/api/district/' + province);
        let result = await response.json();
        this.setState({
            districtArr: result
        });
    }
}
function matchStateToProps(state){
    return{
        province: state.province,
        user: state.auth,
        phases: state.phases
    }
}
export default withRouter(connect(matchStateToProps, actions)(withStyles(styles)(SocialProfileComponent)));