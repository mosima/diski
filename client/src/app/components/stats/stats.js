import React, { Component } from 'react';
import{Card, CardBody, CardTitle, CardFooter, CardText} from 'reactstrap';
import PropTypes from 'prop-types';

class StatsCard extends Component{
    render(){
        return(
            <div>
                <Card color={this.props.theme}>
                    <CardBody>
                        <CardTitle className={"display-4 float-left text-" + this.props.color}>{this.props.title}</CardTitle>
                        <CardText className={"float-right text-" + this.props.color}>
                        {(this.props.desc).toUpperCase()}<br />
                        <i className={"fa fa-3x fa-" + this.props.bgIcon}></i>
                        </CardText>
                    </CardBody>
                    {this.props.footer && 
                        <CardFooter>
                            <i className={"fa fa-" + this.props.icon}></i> {this.props.footerText}
                        </CardFooter>
                    }
                </Card>
            </div>
        )
    }
}

StatsCard.propTypes = {
    theme: PropTypes.oneOf(['danger', 'warning', 'info', 'primary', 'success']),
    color: PropTypes.oneOf(['danger', 'warning', 'info', 'primary', 'success', 'white']),
    title: PropTypes.string,
    icon: PropTypes.string,
    footer: PropTypes.bool,
    footerText: PropTypes.string,
    desc: PropTypes.string,
    bgIcon: PropTypes.string
}

export default StatsCard;