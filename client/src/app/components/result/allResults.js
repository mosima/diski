import React, { Component } from 'react';
import { Row, Col, Button, ButtonGroup } from 'reactstrap';
import {format} from 'date-fns';
import {Link} from 'react-router-dom';

class FixtureCard extends Component {

    render() {
        return (
            <Row style={{ marginBottom: 10 }}>
                <Col md={2} sm={12} xs={12} />
                <Col sm={8}>
                    <ButtonGroup
                    >
                        <Button style={{
                                    textAlign: "center",
                                    width: 300,
                                    borderBottomLeftRadius: 30
                                }} size="md" color="danger">
                            <span
                                className="lead"
                                
                            >
                                Cheche FC
                                </span>
                        </Button>
                        <Link to='/match'>
                        <Button size="md" color="warning">
                            <p style={{fontSize: 15}}>
                               1 - 1 <br/> click to view
                                </p>
                               
                        </Button>
                        </Link>
                        <Button style={{
                                    textAlign: "center",
                                    width: 300,
                                    borderBottomRightRadius: 30
                                }} size="md" color="danger">
                            <span
                                className="lead"
                                
                            >
                                 Itthynk FC
                                </span>
                        </Button>
                    </ButtonGroup>
                </Col>
            </Row>
        );
    }
}

export default FixtureCard;