import React, {Component} from 'react';

class ResultCard extends Component{
    render(){
        return (
            <section>
                <h3>This is {this.props.text} result</h3>
            </section>
        );
    }
}

export default ResultCard;