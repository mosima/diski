import React, {Component} from 'react';
import ResultRow from './resultRow';

class ResultContainer extends Component{
    render(){
        return (
            <ResultRow text={this.props.text} />
        );
    }
}

export default ResultContainer;