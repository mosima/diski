import React, { Component } from 'react';
import { Row, Col, Button, ButtonGroup, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { format } from 'date-fns';

class FixtureCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render() {
        return (

            <div>


                <Row style={{ marginBottom: 10 }}>
                    <Col md={2} sm={12} xs={12} />
                    <Col sm={8}>
                        <ButtonGroup
                        >
                            <Button style={{
                                textAlign: "center",
                                width: 300,
                                borderBottomLeftRadius: 30
                            }} size="md" color="danger">
                                <span
                                    className="lead"

                                >
                                    {this.props.fixture.cup_team_home.team.name}
                                </span>
                            </Button>
                            <Button size="md" color="warning">
                                1-0
                        </Button>
                            <Button style={{
                                textAlign: "center",
                                width: 300,
                                borderBottomRightRadius: 30
                            }} size="md" color="danger">
                                <span
                                    className="lead"
                                >
                                    {this.props.fixture.cup_team_away.team.name}
                                </span>
                            </Button>
                        </ButtonGroup>

                    </Col>
                </Row>
            </div>

        );
    }
}

export default FixtureCard;