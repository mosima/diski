import React, {Component} from 'react';
import ResultCard from './resultCard';

class ResultRow extends Component{
    render(){
        return (
            <ResultCard text={this.props.text} />
        );
    }
}

export default ResultRow;