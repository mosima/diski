import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, CardBody } from 'reactstrap';
import classnames from 'classnames';

export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}
            >
              LINEUP
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}
            >
              STATS
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>

          <TabPane tabId="1">
            <Row>
              <Col sm="12" md={{ size: 8, offset: 0 }}>
                <Card style={{ height: 570, width: 700 }} outline color="danger">
                  <CardBody>
                    <center><h5>STARTING 11</h5></center>
                    <center>
                      <CardText>
                        <h4>
                          <Row>
                            <Col xs="6" sm="4">Soweto FC</Col>
                            <Col xs="6" sm="4">-</Col>
                            <Col sm="4">Vilakazi FC</Col>
                          </Row>
                        </h4>
                      </CardText>
                      <h5>
                        <Row>
                          <Col xs="6" sm="4"><small>Dumu 1 <br />Tau 2<br />Tau 2<br />Tau 2<br />Tau 2<br />Tau 2
                                        <br />Tau 2<br />Tau 2<br />Tau 2<br />Tau 2<br />Tau 2</small></Col>
                          <Col xs="6" sm="4"></Col>
                          <Col sm="4"><small>Dumu 1 <br />Tau 2<br />Tau 2<br />Tau 2<br />Tau 2<br />Tau 2<br />Tau 2
                                        <br />Tau 2<br />Tau 2<br />Tau 2<br />Tau 2</small></Col>
                        </Row>
                      </h5>
                      <center><h5>SUBSTITUTES</h5></center>
                      <small>
                        <Row>
                          <Col xs="6" sm="4">Tau 2<br />Tau 2<br />Tau 2<br />Tau 2</Col>
                          <Col xs="6" sm="4"></Col>
                          <Col sm="4">Tau 2<br />Tau 2<br />Tau 2<br />Tau 2</Col>
                        </Row>
                      </small>
                      <center><h5>MANAGER</h5></center>
                      <small>
                        <Row>
                          <Col xs="6" sm="4">Shibambu</Col>
                          <Col xs="6" sm="4"></Col>
                          <Col sm="4">Maluleke</Col>
                        </Row>
                      </small>

                    </center>
                  </CardBody>
                  <center><small>GIYANI STADIUM</small></center>
                </Card>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col sm="12" md={{ size: 8, offset: 0 }}>
                <Card style={{ height: 150, width: 700 }} outline color="danger">
                  <CardBody outline>
                    <center>
                      <CardText>
                        <h4>
                          <Row>
                            <Col xs="6" sm="4">Soweto FC</Col>
                            <Col xs="6" sm="4">TEAM STATS</Col>
                            <Col sm="4">Vilakazi FC</Col>
                          </Row>
                        </h4>
                      </CardText>

                      <small>
                        <Row>
                          <Col xs="6" sm="4">3</Col>
                          <Col xs="6" sm="4">Yellow Cards</Col>
                          <Col sm="4">2</Col>
                        </Row>
                      </small>
                      <small>
                        <Row>
                          <Col xs="6" sm="4">3</Col>
                          <Col xs="6" sm="4">Red Cards</Col>
                          <Col sm="4">7</Col>
                        </Row>
                      </small>
                      <small>
                        <Row>
                          <Col xs="6" sm="4">6</Col>
                          <Col xs="6" sm="4">Offsides</Col>
                          <Col sm="4">2</Col>
                        </Row>
                      </small>
                      <small>
                        <Row>
                          <Col xs="6" sm="4">0</Col>
                          <Col xs="6" sm="4">Corners</Col>
                          <Col sm="4">2</Col>
                        </Row>
                      </small>
                    </center>
                  </CardBody>

                </Card>
              </Col>
            </Row>
          </TabPane>
        </TabContent>
      </div>
    );
  }
}