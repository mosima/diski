import React, { Component } from 'react';
import { Col, Card, CardBody, CardTitle, CardText, CardFooter, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

class TourCard extends Component {
    render() {
        return (
            <Col md={4} sm={12} xs={12} style={{marginBottom: 10}}>
                <Card>
                    <CardBody>
                        <CardTitle style={{fontSize: '4vh'}} className="display-4 text-danger">
                            {this.props.tour.name}
                      </CardTitle>
                        <CardText className="lead">
                            Format: {this.props.tour.type}
                      </CardText>
                    </CardBody>
                    <CardFooter>
                        <Link to={"/fixture/" + this.props.tour._id}>
                            <Button size="sm" color="danger">Result &amp; Fixtures</Button>{" "}
                        </Link>
                        <Link to={"/log/" + this.props.tour._id}>
                            <Button size="sm" color="danger">View Logs</Button>
                        </Link>
                    </CardFooter>
                </Card>
            </Col>
        )
    }
}

export default TourCard;