import React, {Component} from 'react';
import {Card, Button, Icon, Label, Image} from 'semantic-ui-react';
import format from 'date-fns/distance_in_words_to_now';



class PostCard extends Component{
    constructor(props){
        super(props);

        this.state ={
            expanded: false
        }
    }
    handleExpandClick = () => {
        this.setState({ expanded: !this.state.expanded });
      };
    render() {
    
        return (
          <div>
            <Card fluid>
            <Image src={this.props.post.media} />
              <Card.Content>
                <Image floated='left' size='mini' src={this.props.post.owner.picture} />
                <Card.Header>
                {this.props.post.owner.displayName}
                </Card.Header>
                <Card.Meta>
                {format(this.props.post.createdAt) + " ago"}
                </Card.Meta>
                <Card.Description>
                {this.props.post.postBody}
                </Card.Description>
              </Card.Content>
              <Card.Content extra>
              <Button as='div' labelPosition='right'>
      <Button color='red'>
        <Icon name='heart' />
        Like
      </Button>
      <Label as='a' basic color='red' pointing='left'>2,048</Label>
    </Button>
    <Button as='div' labelPosition='right'>
      <Button basic color='blue'>
        <Icon name='comments' />
        Comments
      </Button>
      <Label as='a' basic color='blue' pointing='left'>2,048</Label>
    </Button>
               
              </Card.Content>
            </Card>
            <br />
          </div>
        );
      }
}

export default PostCard;