import React, {Component} from 'react';
import {Container, Row} from 'reactstrap';
import {} from 'material-ui';
import FixtureRow from './fixtureRow';

class FixtureContainer extends Component{
    constructor(props){
        super(props);
        this.state = {
            fixtures: []
        }
    }
    render(){
        return(
            <Container style={{marginTop: 20}}>
                <Row>
                    <FixtureRow text={this.props.text} fixtures={this.state.fixtures} />
                </Row>
            </Container>
        )
    }
    async getFixtures(){
        let res = await fetch('/api/fixture/province/' + this.props.text);
        let fix = await res.json();
        
        this.setState({fixtures: fix});
        
    }
    componentDidMount(){
        this.getFixtures();
    }
}

export default FixtureContainer;