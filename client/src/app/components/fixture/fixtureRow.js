import React, {Component} from 'react';
import {} from 'material-ui';
import FixtureCard from './fixtureCard';

class FixtureRow extends Component{
    
    render(){
        //console.log("Row",this.props);
        return(
            <div>
                {this.props.fixtures.map((item, i)=>{
                   return(

                        <FixtureCard fixture={item} key={i} />
                        
                   )
                })}
            </div>
        )
    }
}

export default FixtureRow;