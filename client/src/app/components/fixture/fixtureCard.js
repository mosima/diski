import React, { Component } from 'react';
import { Row, Col, Button, ButtonGroup } from 'reactstrap';
import {format} from 'date-fns';

class FixtureCard extends Component {

    render() {
        return (
            <Row style={{ marginBottom: 10 }}>
                <Col md={2} sm={12} xs={12} />
                <Col sm={8}>
                    <ButtonGroup
                    >
                        <Button style={{
                                    textAlign: "center",
                                    width: 300,
                                    borderBottomLeftRadius: 30
                                }} size="md" color="danger">
                            <span
                                className="lead"
                                
                            >
                                {this.props.fixture.cup_team_home.team.name}
                                </span>
                        </Button>
                        <Button size="md" color="warning">
                            <p style={{fontSize: 11}}>
                                {format(this.props.fixture.match_date, "Do MMMM YYYY")}<br />
                                {format(this.props.fixture.match_date, "HH:MM")}
                                 </p>
                        </Button>
                        <Button style={{
                                    textAlign: "center",
                                    width: 300,
                                    borderBottomRightRadius: 30
                                }} size="md" color="danger">
                            <span
                                className="lead"
                                
                            >
                                 {this.props.fixture.cup_team_away.team.name}
                                </span>
                        </Button>
                    </ButtonGroup>
                </Col>
            </Row>
        );
    }
}

export default FixtureCard;