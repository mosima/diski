import React, { Component } from 'react';
import { 
    Button,
    Modal, ModalBody, ModalFooter, ModalHeader, 
    Card, CardHeader, CardBody, ListGroup, ListGroupItem,
    CardImg, CardTitle, CardText

 } from 'reactstrap';
import { format } from 'date-fns';
import {connect} from 'react-redux';
import * as actions from '../../../actions';

class PlayerRow extends Component {
    constructor(props){
        super(props);

        this.state = {
            info: false,
            edit: false,
            delete: false
        }
        this.toggleInfo = this.toggleInfo.bind(this);
        this.toggleDelete = this.toggleDelete.bind(this);
    }
    toggleInfo(){
        this.setState({info: !this.state.info});
    }
    toggleDelete(){
        this.setState({delete: !this.state.delete});
    }
    render() {
        return (
            <tr>
                <Modal isOpen={this.state.info} toggle={this.toggleInfo}>
                    <ModalHeader toggle={this.toggleInfo}></ModalHeader>
                    <ModalBody>
                        <Card>
                            <CardImg src={this.props.player.picture} style={{height: 150}}></CardImg>
                            <CardBody>
                                <ListGroup>
                                    <ListGroupItem>
                                        <CardTitle>Name</CardTitle>
                                        <CardText>{this.props.player.firstname + ' ' + this.props.player.lastname}</CardText>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <CardTitle>Date of Birth</CardTitle>
                                        <CardText>{format(this.props.player.dateOfBirth, "MMMM Do YYYY")}</CardText>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <CardTitle>City</CardTitle>
                                        <CardText>{this.props.player.city}</CardText>
                                    </ListGroupItem>
                                </ListGroup>
                            </CardBody>
                        </Card>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.delete} toggle={this.toggleDelete}>
                    <ModalHeader toggle={this.toggleDelete}>Delete Action Confirmation</ModalHeader>
                    <ModalBody>
                        <p className="lead">Are you sure you want to delete?</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button outline onClick={this.deletePlayer.bind(this)} color="success">Confirm</Button>{' '}
                        <Button outline color="danger" onClick={this.toggleDelete} >Cancel</Button>
                    </ModalFooter>
                </Modal>
                <td>{this.props.player.firstname}</td>
                <td>{this.props.player.lastname}</td>
                <td>{format(this.props.player.dateOfBirth, 'DD/MM/YYYY')}</td>
                <td>{this.props.player.status}</td>
                <td>
                    <Button size="sm" onClick={this.toggleInfo} outline color="danger"><i className="fa fa-info-circle"></i></Button>{' '}
                    <Button size="sm" outline color="danger"><i className="fa fa-edit"></i></Button>{' '}
                    <Button size="sm" onClick={this.toggleDelete} outline color="danger"><i className="fa fa-trash"></i></Button>{' '}
                </td>
            </tr>
        )
    }

    async deletePlayer(){
        try{
            let response = await fetch('/api/player/' + this.props.player._id, {
                method: "DELETE",
                credentials: "include"
            });
            let result = await response.json();
            this.props.get();
            this.props.fetchMyPlayer();
            this.setState({
                delete: false
            });
        }catch(err){
            console.error(err);
        }
    }
}

export default connect(null, actions)(PlayerRow);