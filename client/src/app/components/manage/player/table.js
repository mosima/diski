import React, { Component } from 'react';
import { Table } from 'reactstrap';
import PlayerRow from './row';

class PlayerTable extends Component {
    render() {
        return (
            <Table hover responsive size="sm">
                <thead>
                    <tr>
                        <td>First Name</td>
                        <td>Last Name</td>
                        <td>Date of Birth</td>
                        <td>Status</td>
                        <td></td>
                    </tr>
                </thead>
                {this.props.players && <tbody>
                    {
                        this.props.players.map((p, i)=>{
                            return (<PlayerRow player={p} key={i} get={this.props.getPlayer} />)
                        })
                    }
                </tbody>
                }
            </Table>
        )
    }
}

export default PlayerTable;