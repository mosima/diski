import React, {Component} from 'react';
import {Container, Row, Col, Card, CardHeader, CardTitle, CardBody, CardText} from 'reactstrap';
import Stats from '../stats/stats';
import {connect} from 'react-redux';

class Dashboard extends Component{
    render(){
        return(
            <div>
                <Container fluid>
                    <Row>
                        <Col md={4}>
                            <Stats 
                                title={this.props.teams.length} 
                                theme="success" 
                                desc="teams" 
                                color="white" 
                                bgIcon="users"
                                 />
                        </Col>
                        <Col md={4}>
                            <Stats 
                                title={this.props.coaches.length}
                                theme="warning" 
                                desc="coaches" 
                                color="white" 
                                bgIcon="user-circle"
                                 />
                        </Col>
                        <Col md={4}>
                            <Stats 
                                title={this.props.players.length}
                                theme="danger" 
                                desc="players" 
                                color="white" 
                                bgIcon="user"
                                 />
                        </Col>
                    </Row>
                    <hr />
                    <Row>
                        <Col md={6}>
                            <Card>
                                <CardHeader className="text-white lead" style={{backgroundColor: '#DC3545'}}>Results</CardHeader>
                                <CardBody></CardBody>
                            </Card>
                        </Col>
                        <Col md={6}>
                            <Card>
                                <CardHeader  className="text-white lead" style={{backgroundColor: '#DC3545'}}>Fixtures</CardHeader>
                                <CardBody></CardBody>
                            </Card>
                        </Col>
                    </Row>
                        <hr />
                    <Row>
                        <Col md={12}>
                            <Card>
                                <CardHeader  className="text-white lead" style={{backgroundColor: '#DC3545'}}>
                                    Team Stats
                                </CardHeader>
                                <CardBody></CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        coaches: state.my_coaches,
        teams: state.my_teams,
        players: state.my_players
    }
}

export default connect(mapStateToProps)(Dashboard);