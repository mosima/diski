import React, { Component } from 'react';
import {
    Container, Row, Col,
    Button, Modal, ModalBody, ModalFooter, ModalHeader,
    Form, FormGroup, Label, Input, FormText
} from 'reactstrap';
import Stats from '../stats/stats';
import TeamTable from './teampart/table';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class Team extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render() {
        let active = this.props.my_teams.filter(t => { return t.status === "active" });
        let inactive = this.props.my_teams.filter(t => { return t.status === "inactive" });
        return (
            <div>
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>Register New Team</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Label>Team Name</Label>
                                <Input onChange={(e) => { this.setState({ teamname: e.target.value }) }} />
                            </FormGroup>
                            <FormGroup>
                                <Label>Date</Label>
                                <Input type="date" onChange={(e) => { this.setState({ rDate: e.target.value }) }} />
                            </FormGroup>
                            <FormGroup>
                                <Label>Venue Address</Label>
                                <Input type="textarea" onChange={(e) => { this.setState({ venue: e.target.value }) }} />
                            </FormGroup>
                            <FormGroup>
                                <Label>Coach</Label>
                                <Input type="select" onChange={(e)=>{this.setState({coach: e.target.value})}}>
                                    <option>-- select coach --</option>
                                    {
                                        this.props.coaches.map((c, i)=>{
                                            return(
                                                <option value={c._id} key={i}>{c.firstname + ' ' + c.lastname}</option>
                                            )
                                        })
                                    }
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label>Logo</Label>
                                <Input type="file" onChange={(e) => { this.setState({ logo: e.target.files[0] }) }} />
                                <FormText>Click to upload team's logo or crest</FormText>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        {
                            this.state.teamname &&
                            <Button outline color="success" onClick={this.addTeam.bind(this)}>Submit</Button>}{' '}
                        <Button onClick={this.toggle} outline color="danger">Cancel</Button>
                    </ModalFooter>
                </Modal>
                <Container fluid>
                    <Row>
                        <Col md={4}>
                            <Stats
                                title={this.props.my_teams.length}
                                theme="success"
                                desc="teams"
                                color="white"
                                bgIcon="users"
                            />
                        </Col>
                        <Col md={4}>
                            <Stats
                                title={active.length}
                                theme="warning"
                                desc="active"
                                color="white"
                                bgIcon="users"
                            />
                        </Col>
                        <Col md={4}>
                            <Stats
                                title={inactive.length}
                                theme="danger"
                                desc="inactive"
                                color="white"
                                bgIcon="users"
                            />
                        </Col>
                    </Row>
                    <hr />
                    <Row>
                        <Button style={{ border: 'none' }} outline color="danger" onClick={this.toggle}><i className="fa fa-plus"></i></Button>
                        <TeamTable teams={this.props.my_teams} />
                    </Row>
                </Container>
            </div>
        )
    }

    async addTeam() {
        let formData = new FormData();
        formData.append("name", this.state.teamname);
        if (this.state.rDate) {
            formData.append("year", this.state.rDate);
        }
        if (this.state.venue) {
            formData.append("venue", this.state.venue);
        }
        if(this.state.coach){
            formData.append("coach", this.state.coach);
        }
        if (this.state.logo) {
            formData.append("logo", this.state.logo)
        } else {
            formData.append("logo", null);
        }

        try {
            let response = await fetch('/api/team', {
                method: "POST",
                credentials: "include",
                body: formData
            });
            let result = await response.json();
            this.props.fetchMyTeam();
            this.setState({
                modal: false
            });
        } catch (err) {
            console.error(err.message);
        }
    }
}

function mapStateToProps(state) {
    return {
        user: state.auth,
        my_teams: state.my_teams,
        coaches: state.my_coaches
    }
}

export default connect(mapStateToProps, actions)(Team);