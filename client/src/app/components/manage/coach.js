import React, { Component } from 'react';
import {
    Container, Row, Col, Button,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form, FormGroup, Input, FormText, Label
} from 'reactstrap';
import Stats from '../stats/stats';
import CoachTable from './coach/table';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class Coach extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }
    render() {
        return (
            <div>
                <Container fluid>
                    <Modal isOpen={this.state.modal} toggle={this.toggle}>
                        <ModalHeader toggle={this.toggle}>Register Coach</ModalHeader>
                        <ModalBody>
                            <Form>
                                <FormGroup>
                                    <Label>First Name</Label>
                                    <Input onChange={(e) => { this.setState({ fname: e.target.value }) }} />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Middle Name</Label>
                                    <Input onChange={(e) => { this.setState({ mname: e.target.value }) }} />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Last Name</Label>
                                    <Input onChange={(e) => { this.setState({ lname: e.target.value }) }} />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Date of Birth</Label>
                                    <Input type="date" onChange={(e) => { this.setState({ dob: e.target.value }) }} />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Bio</Label>
                                    <Input type="textarea" onChange={(e) => { this.setState({ bio: e.target.value }) }} />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Team</Label>
                                    <Input type="select" onChange={(e) => { this.setState({ team: e.target.value }) }}></Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label>Picture</Label>
                                    <Input type="file" onChange={(e) => { this.setState({ pic: e.target.files[0] }) }} />
                                    <FormText>Click to upload coach profile picture</FormText>
                                </FormGroup>
                            </Form>
                        </ModalBody>
                        <ModalFooter>
                            <Button onClick={this.addCoach.bind(this)} disabled={!(this.state.fname && this.state.lname)} outline color="success">Submit</Button>{' '}
                            <Button onClick={this.toggle} outline color="danger">Cancel</Button>
                        </ModalFooter>
                    </Modal>
                    <Row>
                        <Col md={4}>
                            <Stats
                                title={this.props.coaches.length}
                                theme="success"
                                desc="coaches"
                                color="white"
                                bgIcon="user-circle"
                            />
                        </Col>
                    </Row>
                    <hr />
                    <Row>
                        <Button style={{border: 'none'}} outline color="danger" onClick={this.toggle}><i className="fa fa-plus"></i></Button>
                        <CoachTable coaches={this.props.coaches} />
                    </Row>
                </Container>
            </div>
        )
    }
    async addCoach() {
        let formData = new FormData();
        formData.append("firstname", this.state.fname);
        formData.append("lastname", this.state.lname);
        if (this.state.mname) {
            formData.append("middlename", this.state.mname);
        }
        if (this.state.dob) {
            formData.append("dob", this.state.dob)
        }
        if (this.state.bio) {
            formData.append("bio", this.state.bio)
        }
        if (this.state.pic) {
            formData.append("picture", this.state.pic);
        }
        if (this.state.team) {
            formData.append("team", this.state.team);
        }
        try {
            let response = await fetch("/api/coach", {
                method: "POST",
                credentials: "include",
                body: formData
            });
            let result = await response.json();
            this.props.fetchMyCoach();
            this.setState({
                modal: false
            });
        } catch (err) {
            console.error(err);
        }

    }
}

function mapStateToProps(state) {
    return {
        user: state.auth,
        coaches: state.my_coaches
    }
}

export default connect(mapStateToProps, actions)(Coach);