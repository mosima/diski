import React, {Component} from 'react';
import {Container, Row, Col, Button} from 'reactstrap';

class Setting extends Component{
    render(){
        return (
            <div>
                <Container fluid>
                    <Row>
                        <Col>
                            <h4 className="display-4">Account</h4>
                            <p className="lead">
                                <Button>Delete Account</Button>
                            </p>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default Setting;