import React, {Component} from 'react';
import { Container, Row, Col, Card, CardHeader, CardBody, CardText, CardTitle } from 'reactstrap';

class Info extends Component{
    render(){
        return(
            <div>
                <Container fluid>
                    <Row>
                        <Col md={6}>
                            <Card>
                                <CardHeader>Announcement</CardHeader>
                                <CardBody></CardBody>
                            </Card>
                        </Col>
                        <Col md={6}>
                            <Card>
                                <CardHeader>Links</CardHeader>
                                <CardBody></CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default Info;