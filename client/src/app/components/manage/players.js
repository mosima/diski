import React, { Component } from 'react';
import {
    Container, Row, Col, Button,
    Modal, ModalBody, ModalFooter, ModalHeader,
    Form, FormGroup, Label, Input, FormText
} from 'reactstrap';
import { } from 'date-fns';
import StatsCard from '../stats/stats';
import PlayerTable from './player/table';
import * as actions from '../../actions';
import {connect} from 'react-redux';

class Player extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            players: []
        }
        this.toggle = this.toggle.bind(this);
        this.getPlayer = this.getPlayer.bind(this);
    }

    toggle() {
        this.setState({
            open: !this.state.open
        });
    }

    componentDidMount() {
        this.getPlayer();
    }

    render() {
        return (
            <div>
                <Container fluid>
                    <Modal isOpen={this.state.open} toggle={this.toggle}>
                        <ModalHeader toggle={this.toggle}>Player Information</ModalHeader>
                        <ModalBody>
                            <Form>
                                <FormGroup>
                                    <Label>First Name</Label>
                                    <Input onChange={(e) => { this.setState({ fname: e.target.value }) }} />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Middle Name</Label>
                                    <Input onChange={(e) => { this.setState({ mname: e.target.value }) }} />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Last Name</Label>
                                    <Input onChange={(e) => { this.setState({ lname: e.target.value }) }} />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Date of Birth</Label>
                                    <Input type="date" onChange={(e) => { this.setState({ dob: e.target.value }) }} />
                                </FormGroup>
                                <FormGroup>
                                    <Label>Picture</Label>
                                    <Input type="file" onChange={(e) => { this.setState({ pic: e.target.files[0] }) }} />
                                    <FormText>Click here to upload player's picture</FormText>
                                </FormGroup>
                            </Form>
                        </ModalBody>
                        <ModalFooter>
                            {
                                this.state.fname &&
                                this.state.lname &&
                                this.state.dob &&
                                <Button outline onClick={this.addPlayer.bind(this)} color="success">Submit</Button>}{' '}
                            <Button outline onClick={this.toggle} color="danger">Cancel</Button>
                        </ModalFooter>
                    </Modal>
                    <Row>
                        <Col md={3}>
                            <StatsCard
                                title={this.state.players.length}
                                theme="success"
                                desc="players"
                                color="white"
                                bgIcon="user"
                            />
                        </Col>
                    </Row>
                    <hr />
                    <Row>
                        <Col style={{marginBottom: 10}}>
                            <Button style={{border: 'none'}} outline onClick={()=>{this.props.history.goBack()}} color="danger"><i className="fa fa-chevron-circle-left"></i></Button>{' '}
                            <Button style={{border: 'none'}} outline onClick={this.toggle} color="danger"><i className="fa fa-plus"></i></Button>
                        </Col>
                        
                        <PlayerTable players={this.state.players} getPlayer={this.getPlayer} />
                    </Row>
                </Container>
            </div>
        )
    }

    async addPlayer() {
        let formData = new FormData();
        formData.append("firstname", this.state.fname);
        if (this.state.mname) {
            formData.append("middlename", this.state.mname);
        }
        formData.append("lastname", this.state.lname);
        formData.append("dateOfBirth", this.state.dob);
        if (this.state.pic) {
            formData.append("picture", this.state.pic);
        } else {
            formData.append("picture", null);
        }
        formData.append("teamId", this.props.match.params.teamId);
        try {
            let response = await fetch('/api/player/team', {
                method: "POST",
                credentials: "include",
                body: formData
            });
            let result = await response.json();
            this.getPlayer();
            this.props.fetchMyPlayer();
            this.setState({
                open: !this.state.open
            });
        } catch (err) {
            console.error(err.message);
        }

    }
    async getPlayer() {
        try {
            let response = await fetch('/api/player/team/' + this.props.match.params.teamId, {
                method: "GET",
                credentials: "include"
            });
            let result = await response.json();
            this.setState({
                players: result
            })
        } catch (err) {
            console.error(err.message);
        }
        
    }
}

export default connect(null, actions)(Player);