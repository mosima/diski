import React, { Component } from 'react';
import { Table } from 'reactstrap';
import CoachRow from './row';

class CoachTable extends Component {
    render() {
        return (
            <Table hover responsive size="sm">
                <thead>
                    <tr>
                        <td>First Name</td>
                        <td>Last Name</td>
                        <td>Team</td>
                        <td></td>
                    </tr>
                </thead>
                {this.props.coaches &&<tbody>
                    {
                        this.props.coaches.map((c, i)=>{
                            return <CoachRow key={i} coach={c} />
                        })
                    }
                </tbody>
                }
            </Table>
        )
    }
}

export default CoachTable;