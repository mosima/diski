import React, { Component } from 'react';
import { Button, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
import { format } from 'date-fns';
import { connect } from 'react-redux';
import * as actions from '../../../actions';

class CoachRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            view: false,
            edit: false,
            delete: false
        }
    }
    render() {
        return (
            <tr>
                <Modal isOpen={this.state.view} toggle={() => { this.setState({ view: !this.state.view }) }}></Modal>
                <Modal isOpen={this.state.edit} toggle={() => { this.setState({ edit: !this.state.edit }) }}></Modal>
                <Modal isOpen={this.state.delete} toggle={() => { this.setState({ delete: !this.state.delete }) }}>
                    <ModalHeader toggle={() => { this.setState({ delete: !this.state.delete }) }}>Delete Action Confirmation</ModalHeader>
                    <ModalBody>
                        <p className="lead">Are you sure you want to delete?</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.deleteCoach.bind(this)} outline color="success">Confirm</Button>{' '}
                        <Button outline color="danger" onClick={() => { this.setState({ delete: !this.state.delete }) }}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <td>{this.props.coach.firstname}</td>
                <td>{this.props.coach.lastname}</td>
                <td>{this.props.coach.team ? this.props.coach.team.name : ''}</td>
                <td>
                    <Button size="sm" outline color="danger"><i className="fa fa-info-circle"></i></Button>{' '}
                    <Button size="sm" outline color="danger"><i className="fa fa-edit"></i></Button>{' '}
                    <Button size="sm" onClick={() => { this.setState({ delete: !this.state.delete }) }} outline color="danger"><i className="fa fa-trash"></i></Button>{' '}
                </td>
            </tr>
        )
    }

    async deleteCoach() {
        try {
            let response = await fetch('/api/coach/' + this.props.coach._id, {
                method: "DELETE",
                credentials: "include"
            });
            let result = await response.json();
            this.props.fetchMyCoach();
            this.setState({
                delete: false
            });
        } catch (err) {
            console.error(err);
        }
    }
}


export default connect(null, actions)(CoachRow);