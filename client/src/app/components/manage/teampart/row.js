import React, { Component } from 'react';
import { Button, Modal, ModalBody, ModalFooter, Input, ModalHeader, Label } from 'reactstrap';
import { format } from 'date-fns';
import { withRouter, Link } from 'react-router-dom';
import * as actions from '../../../actions';
import { connect } from 'react-redux';

class TeamRow extends Component {
    constructor(props) {
        super(props);

        this.state = {
            infoModal: false,
            editModal: false,
            deleteModal: false
        }
    }
    render() {
        return (
            <tr>
                <Modal isOpen={this.state.infoModal} toggle={() => { this.setState({ infoModal: !this.state.infoModal }) }}>
                    <ModalHeader toggle={() => { this.setState({ infoModal: !this.state.infoModal }) }}>
                        Team Information
                    </ModalHeader>
                    <ModalBody>
                        <img src={this.props.team.team_logo ? this.props.team.team_logo : ""} className="img-thumbnail" />
                        <hr />
                        <Label>Name</Label><br />
                        <span className="lead">{this.props.team.name}</span>
                        <hr />
                        <Label>Year Registered</Label><br />
                        <span className="lead">{this.props.team.year ? format(this.props.team.year, "MMMM YYYY") : ''}</span>
                        <hr />
                        <Label>Venue</Label><br />
                        <span className="lead">{this.props.team.venue ? this.props.team.venue : ""}</span>
                        <hr />
                        <Label>Coach</Label><br />
                    </ModalBody>

                </Modal>
                <Modal isOpen={this.state.editModal} toggle={() => { this.setState({ editModal: !this.state.editModal }) }}>
                    <ModalHeader toggle={() => { this.setState({ editModal: !this.state.editModal }) }}>
                        Edit Team Information
                    </ModalHeader>
                    <ModalBody>
                        <img src={this.props.team.team_logo ? this.props.team.team_logo : ""} className="img-thumbnail" />
                        <hr />
                        <Label>Name</Label><br />
                        <Input defaultValue={this.props.team.name} onChange={(e) => { this.setState({ tname: e.target.value }) }} />
                        <hr />
                        <Label>Year Registered</Label><br />
                        <Input defaultValue={this.props.team.year ? format(this.props.team.year, "YYYY-MM-DD") : ''} placeholder="YYYY-MM-DD" onChange={(e) => { this.setState({ tyear: e.target.value }) }} />
                        <hr />
                        <Label>Venue</Label><br />
                        <Input defaultValue={this.props.team.venue ? this.props.team.venue : ""} onChange={(e) => { this.setState({ tvenue: e.target.value }) }} />
                        <hr />
                        <Label>Coach</Label><br />
                        <Input type="select" defaultValue={this.props.team.name} onChange={(e) => { this.setState({ tcoach: e.target.value }) }}>
                            {/* <option>{ this.props.team.coach.firstname + ' ' + this.props.team.coach.lastname}</option> */}
                        </Input>
                    </ModalBody>
                    <ModalFooter>
                        {(this.state.tname || this.state.tvenue || this.state.tyear) &&
                            <Button outline onClick={this.editTeam.bind(this)} color="success">Edit</Button>}{' '}
                        <Button outline color="danger" onClick={() => { this.setState({ editModal: !this.state.editModal }) }}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <Modal isOpen={this.state.deleteModal} toggle={() => { this.setState({ deleteModal: !this.state.deleteModal }) }}>
                    <ModalHeader toggle={() => { this.setState({ deleteModal: !this.state.deleteModal }) }}>Delete Action Confirmation</ModalHeader>
                    <ModalBody>
                        <p className="lead">Are you sure you want to delete?</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.deleteTeam.bind(this)} color="success" outline>Confirm</Button>{' '}
                        <Button color="danger" outline onClick={() => { this.setState({ deleteModal: !this.state.deleteModal }) }}>Close</Button>
                    </ModalFooter>
                </Modal>
                <td>{this.props.team.name}</td>
                <td>{this.props.team.coach ? this.props.team.coach.firstname + ' ' + this.props.team.coach.lastname : ''}</td>
                <td>{this.props.team.year ? format(this.props.team.year, "DD MMM YYYY") : ''}</td>
                <td>{this.props.team.status}</td>
                <td>
                    <Link to={"/manage/teams/" + this.props.team._id}><Button size="sm" outline color="danger"><i className="fa fa-users"></i> PLAYERS</Button></Link>{' '}
                    <Button size="sm" onClick={() => { this.setState({ infoModal: !this.state.infoModal }) }} outline color="danger"><i className="fa fa-info-circle"></i></Button>{' '}
                    <Button size="sm" onClick={() => { this.setState({ editModal: !this.state.editModal }) }} outline color="danger"><i className="fa fa-edit"></i></Button>{' '}
                    <Button size="sm" outline color="danger" onClick={() => { this.setState({ deleteModal: !this.state.deleteModal }) }}><i className="fa fa-trash"></i></Button>{' '}
                </td>
            </tr>
        )
    }

    async editTeam() {
        let obj = {
            name: this.state.tname,
            venue: this.state.tvenue,
            year: this.state.tyear
        }
        if (this.state.tcoach) {
            obj.coach = this.state.tcoach
        }
        try {
            let response = await fetch('/api/team/' + this.props.team._id, {
                method: "PUT",
                credentials: "include",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(obj)
            });
            let result = await response.json();
            this.props.fetchMyTeam();
            this.setState({
                editModal: !this.state.editModal
            });
        } catch (err) {
            console.error(err.message);
        }
    }

    async deleteTeam(){
        try{
            let response = await fetch('/api/team/' + this.props.team._id, {
                method: "DELETE",
                credentials: "include"
            });
            let result = await response.json();
            this.props.fetchMyTeam();
            this.setState({
                deleteModal: false
            })
        }catch(err){
            console.error(err);
        }
    }
}

export default withRouter(connect(null, actions)(TeamRow));