import React, { Component } from 'react';
import { Table } from 'reactstrap';
import TeamRow from './row';

class TeamTable extends Component {
    render() {
        //console.log("teams", this.props.teams)
        return (
            <Table hover responsive size="sm">
                <thead>
                    <tr>
                        <td>Team</td>
                        <td>Coach</td>
                        <td>Registered</td>
                        <td>Status</td>
                        <td></td>
                    </tr>
                </thead>
                {this.props.teams && <tbody>
                    {
                        this.props.teams.map((t, i) => {
                            return (<TeamRow team={t} key={i} />)
                        })
                    }
                </tbody>
                }
            </Table>
        )
    }
}

export default TeamTable;