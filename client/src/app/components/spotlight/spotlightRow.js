import React, {Component} from 'react';
import {Grid, Header, Image, Statistic, Icon, Card, Button} from 'semantic-ui-react';
import {format} from 'date-fns';
import {withRouter} from 'react-router-dom';

class SpotlightRowComponent extends Component{
    render(){
        
        return(
            <Grid.Row>
                <Grid.Column width={3}>
                    <Image src={this.props.item.media} />
                </Grid.Column>
                <Grid.Column width={10}>
                    <Header
                        as='h2'
                        icon='empty star'
                        content={this.props.item.title}
                    />
                    <Card>
                        <Card.Content description={this.props.item.description} />
                        <Card.Content extra>
                            <Button icon basic>
                                <Icon name="empty heart" />
                            </Button>
                            <Button icon basic>
                                <Icon name="comments outline" />
                            </Button>
                        </Card.Content>
                    </Card>
                </Grid.Column>
                <Grid.Column width={3}>
                <Statistic horizontal value={format(this.props.item.createdAt, 'Do MMM')} />
                </Grid.Column>
            </Grid.Row>
        )
    }
}

export default withRouter(SpotlightRowComponent);