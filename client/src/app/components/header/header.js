import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { withRouter } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Modal, ModalBody, ModalHeader, Button
} from 'reactstrap';
const logo = require("../../assets/img/logo.jpg");


class HeaderComponent extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.modalToggle = this.modalToggle.bind(this);
    this.state = {
      isOpen: false,
      modal: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  modalToggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
   // console.log(this.props.user)
    return (
      <div>
        <Modal isOpen={this.state.modal} toggle={this.modalToggle} className={this.props.className}>
          
          <ModalBody>
            <Button href="/auth/facebook/login" block color="info">Login with Facebook</Button>
            <Button href="/auth/google/login" block color="danger">Login with Google</Button>
          </ModalBody>
        </Modal>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/"><img src={logo} className="logo" /></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink to="/" className="nav-link">Home</NavLink>
              </NavItem>
              {/* <NavItem>
                <NavLink to="/news" className="nav-link">About</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="/news" className="nav-link">Project</NavLink>
              </NavItem> */}
              <NavItem>
                <NavLink to="/tournament" className="nav-link">Tournaments</NavLink>
              </NavItem>
              {/* <NavItem>
                <NavLink to="/news" className="nav-link">News</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="/news" className="nav-link">Spotlight</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="/news" className="nav-link">Showcase</NavLink>
              </NavItem> */}
              {this.props.user ?
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle nav caret>
                    {this.props.user.displayName}
                  </DropdownToggle>
                  <DropdownMenu right>
                    {/* <DropdownItem>
                      <NavLink className="nav-link" to="/profile">Profile</NavLink>
                    </DropdownItem>
                    <DropdownItem>
                      <NavLink className="nav-link" to="/social">Community</NavLink>
                    </DropdownItem> */}
                    <DropdownItem>
                      <NavLink className="nav-link" to="/manage">Manage Teams</NavLink>
                    </DropdownItem>
                    {this.props.user.isGlobalAdmin && <DropdownItem>
                      <NavLink className="nav-link" to="/global-admin">Global Setup</NavLink>
                    </DropdownItem>}
                    {this.props.user.isReferee && <DropdownItem>
                      <NavLink className="nav-link" to="/manage-match">Manage matches</NavLink>
                    </DropdownItem>}
                    <DropdownItem divider />
                    <DropdownItem>
                      <a className="nav-link" href="/auth/logout">Logout</a>
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
                :
                <NavItem>
                  <span onClick={this.modalToggle} className="nav-link">Login</span>
                </NavItem>
              }
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
  doLogout() {
    this.props.logout();
  }
  login() { }

  navigateRoute(routName) {
    this.props.history.push("/" + routName);
  }
}

function matchStateToProps(state) {
  return {
    user: state.auth
  };
}

export default withRouter(connect(matchStateToProps)(HeaderComponent));
