import React, {Component} from 'react';
import { withStyles } from 'material-ui/styles';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select';
import TextField from 'material-ui/TextField';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 250,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
});


class SchoolSearchComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            province: '',
            phase: '',
            district: '',
            districtArr: [],
            isActive: true
        }
    }
    

      handleChange = name => event => {
          
        this.setState({ [name]: event.target.value },()=>{
            if(name == 'province'){
                
                this.setState({
                    isActive: false
                })
            this.getProvinceDistrict(this.state.province);
            }
        });
        
      };
    

     async getProvinceDistrict(province){
          let res = await fetch('/api/district/' + province);
          let district = await res.json();
          this.setState({
              districtArr: district
          });
      }

      render() {
        const { classes } = this.props;
        return (
            <form className={classes.container} autoComplete="off">
                <FormControl className={classes.formControl}>
                <InputLabel htmlFor="province">Province</InputLabel>
                <Select
                    value={this.state.province}
                    onChange={this.handleChange('province')}
                    input={<Input id="province" />}
                >
                    <MenuItem value="">
                    <em>None</em>
                    </MenuItem>
                    {this.props.provinces.map(item=>{
                        return(
                            <MenuItem value={item.short_name}>{(item.name).toUpperCase()}</MenuItem>
                        )
                    })}
                </Select>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="phase">Phase</InputLabel>
                    <Select
                        value={this.state.phase}
                        onChange={this.handleChange('phase')}
                        input={<Input id="phase" />}
                    >
                        <MenuItem value="">
                        <em>None</em>
                        </MenuItem>
                        {this.props.phases.map(item=>{
                            return(
                                <MenuItem value={item.phase}>{(item.phase).toUpperCase()}</MenuItem>
                            )
                        })}
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="district">District</InputLabel>
                    <Select
                        value={this.state.district}
                        onChange={this.handleChange('district')}
                        input={<Input id="district" disabled={this.state.isActive} />}
                        disabled={this.state.isActive}
                    >
                        <MenuItem value="">
                        <em>None</em>
                        </MenuItem>
                        {this.state.districtArr.map(item=>{
                            return(
                                <MenuItem value={item.district}>{(item.district).toUpperCase()}</MenuItem>
                            )
                        })}
                    </Select>
                </FormControl>
                <FormControl>
                <TextField
                    id="search"
                    label="Search school name"
                    type="search"
                    className={classes.textField}
                    margin="normal"
                    disabled={this.state.isActive}
                    />
                </FormControl>
            </form>
        )
      }
}

export default withStyles(styles)(SchoolSearchComponent);