import React, {Component} from 'react';
import classNames from 'classnames';
import { withStyles } from 'material-ui/styles';
import keycode from 'keycode';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Checkbox from 'material-ui/Checkbox';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import FilterListIcon from 'material-ui-icons/FilterList';
import EnhancedTable from './EnhancedTable';

const styles = theme => ({
    root: {
      width: '100%',
      background: theme.palette.background.paper,
      position: 'relative',
      overflow: 'auto',
    },
    listSection: {
      background: 'inherit',
    },
  });


class SchoolListComponent extends Component{

    render(){
        return(
            <EnhancedTable schools={this.props.schools} />
        )
    }
}

export default withStyles(styles)(SchoolListComponent);