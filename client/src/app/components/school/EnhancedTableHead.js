import React, {Component} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import keycode from 'keycode';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Checkbox from 'material-ui/Checkbox';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import FilterListIcon from 'material-ui-icons/FilterList';

const styles = theme => ({
    root: {
      width: '100%',
      background: theme.palette.background.paper,
      position: 'relative',
      overflow: 'auto',
    },
    listSection: {
      background: 'inherit',
    },
  });


  const columnData = [
    { id: 'name', numeric: false, disablePadding: true, label: 'NAME' },
    { id: 'phase', numeric: false, disablePadding: false, label: 'PHASE' },
    { id: 'district', numeric: false, disablePadding: false, label: 'DISTRICT' },
    { id: 'province', numeric: false, disablePadding: false, label: 'PROVINCE' },
    { id: 'telephone', numeric: false, disablePadding: false, label: 'TELEPHONE' },
  ];
  
  

class EnhancedTableHead extends Component{
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
      };
    
    render(){
        const { onSelectAllClick, order, orderBy, numSelected, rowCount } = this.props;
        
        return(
            <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {columnData.map(column => {
            return (
              <TableCell
                key={column.id}
                numeric={column.numeric}
                padding={column.disablePadding ? 'none' : 'default'}
              >
                <Tooltip
                  title="Sort"
                  placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === column.id}
                    direction={order}
                    onClick={this.createSortHandler(column.id)}
                  >
                    {column.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            );
          }, this)}
        </TableRow>
      </TableHead>
    );

    }
}

export default withStyles(styles)(EnhancedTableHead);