import React, {Component} from 'react';
import {Container, Row, Col} from 'reactstrap';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import {connect} from 'react-redux';

const styles = theme => ({
    root: {
      backgroundColor: theme.palette.background.paper,
    },
  });

class TournamentBar extends Component{
    
    render(){
        //console.log('pro', this.props);
        const { classes, theme } = this.props;
        return (
                <div className={classes.root}>
                    <AppBar position="static" color="primary">
                    <Tabs
                        value={this.props.value}
                        onChange={this.props.handleChange}
                        indicatorColor="default"
                        textColor="default"
                        fullWidth
                    >
                    {
                        this.props.province.map((item, i)=>{
                            return(
                                <Tab label={item.name} key={i} />
                            )
                        })
                    }
                    </Tabs>
                    </AppBar>
            </div>
        )
    }
    handleChange = (event, value) => {
        this.setState({ value });
      };
    
      handleChangeIndex = index => {
        this.setState({ value: index });
      };
    
}
function matchStateToProps(state){
    return{
        province: state.province
    }
}
export default  connect(matchStateToProps)(withStyles(styles, { withTheme: true })(TournamentBar));