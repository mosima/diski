import React, { Component } from 'react';
import { 
    Container, Row, Col,
    Form, FormGroup, Input, Label,
    Button
 } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import logo from '../assets/img/logo.jpg';

class Fixture extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <div>
                <Container>
                    <Row>
                        <Col>
                            <center>
                                <img src={logo} className="img-fluid" />
                            </center>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3} sm={12}></Col>
                        <Col md={6}>
                            <hr />
                            <Button href="/auth/facebook/login" block color="info"><i className="fa fa-facebook"></i> Sign Up with Facebook</Button>
                            <Button href="/auth/google/login" block color="danger"><i className="fa fa-google"></i> Sign Up with Google</Button>
                            
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }

}
export default withRouter(Fixture);