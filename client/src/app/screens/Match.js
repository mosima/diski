import React from 'react';
import { Card, CardBody, Button, CardTitle, CardText, CardImg, Col, Row, Container } from 'reactstrap';
import ball from '../assets/img/ball.jpg';
import ResultTabs from '../components/result/allResultsTabs'

const Match = (props) => {
  return (
    <div className="container" >
      <Col sm="12" md={{ size: 8, offset: 2 }}>
        <Card style={{ height: 350, width: 700 }} outline color="danger">
          <CardBody>
            <CardTitle>
              <Row>
                <Col sm={{ size: 'auto', offset: 0 }}>
                  <small>Match Updates: 25/06  </small>
                </Col>
                <Col sm={{ size: 'auto', offset: 7 }}>
                  <small className="text-muted">Full time</small>
                </Col>
              </Row>

            </CardTitle>
            <CardText>
              <Row style={{ paddingLeft: 60, marginTop: 30 }}>
                <Col style={{ marginTop: 15 }} xs="6" sm="4"><h5>Soweto FC </h5></Col>
                <Col xs="6" sm="4"><h1> 3 - 2</h1></Col>
                <Col style={{ marginTop: 15 }} xs="6" sm="4"><h5>Vilakazi FC</h5></Col>
              </Row>
            </CardText>

            <Col md={{ size: 12, offset: 3 }}><small className="text-muted">Group Stage , Group A, Matchday | 1:3</small></Col>
            <hr style={{ color: "grey" }} className="my-2" />
            <Col md={{ size: 12, offset: 5 }}><CardImg bottom style={{ height: 25, width: 25 }} src={ball} alt="Card image cap" /></Col>
            <Row >
              <Col md={{ size: 4, offset: 0 }} xs="6" sm="4"><small>Thibane biyela 10'<br />Thibane biyela 10'<br />Godfrey Hlabolwa 35'</small> </Col>
              <Col md={{ size: 4, offset: 4 }} xs="6" sm="4"><small>Godfrey Hlabolwa 35'<br />Thibane biyela 10'</small></Col>
            </Row>
          </CardBody>

          {/* <CardImg bottom style={{ height: 500, width: 700 }} src="http://res.cloudinary.com/dlw1hdfx4/image/upload/v1474452412/big/3_b.jpg" alt="Card image cap" /> */}
        </Card>
        <ResultTabs/>
      </Col>
    </div>
  );
};
export default Match;