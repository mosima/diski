import React, { Component } from 'react';
import { Container, Col, Row, Table } from 'reactstrap';
import GroupRow from '../components/global/tour/group/GroupRow';
import { connect } from 'react-redux';
import * as actions from '../actions';


class Logs extends Component {

    state = {
        groups: []
    }

    componentDidMount() {
        this.props.fetchTour(this.props.match.params.tourId);
        this.getGroups();
    }
    render() {
        return (
            <Container>
                <h4 style={{ fontSize: '5vh' }} className="display-4">{this.props.tour ? this.props.tour.name : ''} Logs</h4>
                <hr />
                {
                    this.state.groups && <div>
                        {
                            this.state.groups.map((g, i) => {
                                return (
                                    <GroupRow group={g} key={i} />
                                )
                            })
                        }
                    </div>
                }

            </Container>
        )
    }

    async getGroups() {
        try {
            let response = await fetch('/api/group/' + this.props.match.params.tourId);
            let result = await response.json();
            this.setState({
                groups: result
            });
        } catch (error) {
            console.error(error);

        }
    }
}
function mapStateToProps(state) {
    return { tour: state.tour }
}

export default connect(mapStateToProps, actions)(Logs);