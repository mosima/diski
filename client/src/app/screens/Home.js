import React, { Component } from 'react';
import { connect } from 'react-redux';
import bg from '../assets/img/bg.jpg';
import {
    Container, Row, Col, Jumbotron, Button,
    Card, CardHeader, CardFooter, CardBody,
    CardTitle, CardText,
    Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }
    render() {
        return (
            <div>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Getting Started</ModalHeader>
                    <ModalBody>
                        <Button block color="danger" onClick={()=>{this.props.history.push('/register-user')}}>Create Account</Button>
                        <Button block color="warning" onClick={this.toggle}>Register School</Button>
                    </ModalBody>
                </Modal>
                <Jumbotron>
                    <Container style={{paddingTop: '10vh'}}>
                        <h1 className="text-white display-4 text-center">Welcome To Diski Nine 9</h1>
                        <p className="lead text-white text-center">The birthplace of football development in rural,
                                township and urban areas of South Africa.
                        </p>
                        <hr />
                        <center>
                            {!this.props.user &&
                            <Button onClick={this.toggle} color="danger" size="lg">Get Started with Diski Nine 9</Button>}
                        </center>
                    </Container>
                </Jumbotron>
                <Container>
                    <hr />
                    <Row>
                        <Col md={6}>
                            <Card color="danger">
                                <CardHeader className="text-white">Why football?</CardHeader>
                                <CardBody>
                                    <CardText className="lead text-white">
                                        The game of soccer develops leaders - past, present and future.
                                        It unites friends, families and communities.
                                        Soccer has the power to educate our people, to create jobs and empower
                                        small businesses. This is why we believe that we can use soccer as a tool
                                        to empower and educate young Africans.
                                    </CardText>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col md={6}>
                            <Card color="success">
                                <CardHeader className="text-white">We Support Local Coaches</CardHeader>
                                <CardBody>
                                    <CardText className="lead text-white">
                                        We provide financial management workshops to assist local amateur coaches run their
                                        teams as sustainable organisations. We also run coaching and leadership clinics conducted
                                        by professional coaches to ensure that amateur coaches gain the skills required to
                                        develop amateur soccer players.
                                    </CardText>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                    <hr />
                    <Row>
                        <Col md={6}>
                            <Card color="warning">
                                <CardHeader className="text-white">We Empower and Educate Young Talent</CardHeader>
                                <CardBody>
                                    <CardText className="lead text-white">
                                        We organise soccer tournaments for soccer clubs and schools.
                                        Our soccer tournaments incorporate educational workshops for our participants.
                                        Before our participants play soccer,
                                        they attend workshops focused on Entrepreneurship, Maths, English and Life-skills.
                                    </CardText>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col md={6}>
                            <Card color="primary">
                                <CardHeader className="text-white">We develop Community Leaders</CardHeader>
                                <CardBody>
                                    <CardText className="lead text-white">
                                        We are a volunteer based organisation. As a result, we work with a large number of volunteers
                                        who are either employed or unemployed. Through building a culture of volunteerism in South Africa,
                                        we equip our volunteers with leadership,
                                        project management and problem solving skills that are necessary to develop our communities.
                                    </CardText>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

function matchStateToProps(state) {
    return {
        fixtures: state.fixtures,
        news: state.news,
        user: state.auth
    }
}

export default connect(matchStateToProps)(Home);