import React, {Component} from 'react';
import {Container} from 'reactstrap';
import { withStyles } from 'material-ui/styles';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import {GoogleMap, Marker} from 'react-google-maps';

const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    height: 200,
  },
};

const AnyReactComponent = ({ text }) => <div>{text}</div>;

class SchoolDetail extends Component{
    constructor(props){
        super(props);
        this.state = {
            school: null
        }
    }
    render(){
        console.log(this.state);
        const { classes } = this.props;
        return (
            <Container>
                <div>
                    
                    
                    {(()=>{
                        if(this.state.school != null){
                            return(
                                <Card className={classes.card}>
                                
                            <CardContent>
                        <Typography type="headline" component="h2">
                            {this.state.school.Institution_Name}
                        </Typography>
                        <Typography component="p">
                        {this.state.school.StreetAddress}
                        </Typography>
                        </CardContent>
                        <CardActions>
                        <Button dense color="primary">
                        {this.state.school.Phase}
                        </Button>
                        <Button dense color="primary">
                        {this.state.school.EIDistrict}<br />
                        Province: {this.state.school.Province}
                        </Button>
                        </CardActions>
                    </Card>
                            )
                        }
                    })()}
                        
                    </div>
            </Container>
        );
    }
    async getSchool(){
        let response = await fetch('/api/school/' + this.props.match.params.id);
        let result = await response.json();
        this.setState({
            school: result
        });
    }
    componentDidMount(){
        this.getSchool();
    }
}

export default withStyles(styles)(SchoolDetail);