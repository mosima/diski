import React, {Component} from 'react';
import {Grid, Image, Icon, Header, Button, Card, Statistic} from 'semantic-ui-react';
import {format} from 'date-fns';
import {connect} from 'react-redux';
import {Container} from 'reactstrap';
import SpotlightRowComponent from '../components/spotlight/spotlightRow';

class Spotlight extends Component{
    render(){
        return (
            
            <div>
                <Container>
                <Grid celled>
                    {
                        
                    this.props.spotlights.map((item, i)=>{
                       return( <Grid.Row key={i}>
                        <Grid.Column width={3}>
                            <Image src={item.media} size="huge" />
                        </Grid.Column>
                        <Grid.Column width={10}>
                            <Header
                                color="red"
                                as='h2'
                                icon='empty star'
                                content={item.title}
                            />
                            <Card fluid>
                                <Card.Content description={item.description} />
                                <Card.Content extra>
                                    <Button icon basic>
                                        <Icon name="empty heart" />
                                    </Button>
                                    <Button icon basic>
                                        <Icon name="comments outline" />
                                    </Button>
                                </Card.Content>
                            </Card>
                        </Grid.Column>
                        <Grid.Column width={3}>
                        <Statistic horizontal value={format(item.createdAt, 'Do MMM')} />
                        </Grid.Column>
                    </Grid.Row>
                       )
                    })
                              
                        
                    }
                    
                </Grid>
                </Container>
            </div>
        )
    }
}

function matchStateToProps(state){
    return {
        spotlights: state.spotlights
    }
}

export default connect(matchStateToProps)(Spotlight);