import React, {Component} from 'react';
import {connect} from 'react-redux';
import SchoolListComponent from '../components/school/schoolList';
import SchoolSearchComponent from '../components/school/schoolSearch';
import {Container} from 'reactstrap';

class School extends Component{
    render(){
       
        return (
                <Container>
                    <div style={{marginTop: 20}}>
                    <SchoolSearchComponent provinces={this.props.province} phases={this.props.phases} />
                    <SchoolListComponent schools={this.props.schools} />
                    </div>
                </Container>
            
        );
    }
}

function matchStateToProps(state){
    return {
        schools: state.schools,
        province: state.province,
        phases: state.phases
    }
}
export default connect(matchStateToProps)(School);