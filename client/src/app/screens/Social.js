import React, {Component} from 'react';
import {Container, Row, Col, Form, FormGroup, Label, Input, FormText} from 'reactstrap'
import { withStyles } from 'material-ui/styles';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import {SvgIcon} from 'material-ui';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import ListSubheader from 'material-ui/List/ListSubheader';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Collapse from 'material-ui/transitions/Collapse';
import ListIcon from 'material-ui-icons/List';
import DraftsIcon from 'material-ui-icons/Drafts';
import Person from 'material-ui-icons/Person';
import Capture from 'material-ui-icons/Camera';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';
import StarBorder from 'material-ui-icons/StarBorder';
import HomeIcon from 'material-ui-icons/Home';
import PostContainer from '../components/post/postContainer';
import * as actions from '../actions';
import NewsSocialContainer from '../components/news/NewsSocialContainer';
import {Link, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import {Icon} from 'semantic-ui-react';
import SocialProfileComponent from '../components/profile/profile';
import SocialHomeComponent from '../components/profile/social';
import {Dimmer, Loader} from 'semantic-ui-react';
const styles = {
    card: {
      maxWidth: 345,
    },
    media: {
      height: 200,
    },
    input: {
        display: 'none',
      },
  };
  

class Social extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            open: false,
            loading: false
        }
     
    }

    handleClick = () => {
        this.setState({ open: !this.state.open });
      };
    render(){
        const { classes } = this.props;
        return (
            <section style={{paddingTop: 20}}>
                <Container>
                    <Row>
                        <Col md="3">
                        <Card className={classes.card}>
                            <Dimmer active={this.state.loading}>
                                <Loader>Uploading Image</Loader>
                            </Dimmer>
                            <CardMedia
                                className={classes.media}
                                image={this.props.user.picture}
                                title={this.props.user.displayName}
                            />
                            <CardContent>
                            <input onChange={(e)=>{this.changePicture.bind(this, e)()}} className={classes.input} type="file" id="file" />
                                    <label htmlFor="file">
                                        <Button component="span" className={classes.button}>
                                            <Capture /> Change Picture
                                        </Button>
                                </label>
                            </CardContent>
                            
                            <CardContent>
                            <List className={classes.root}>
                            <Link to="/social/"><ListItem button>
                                <ListItemIcon>
                                    <HomeIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="Home" />
                                </ListItem></Link>
                                <Link to="/social/profile"><ListItem button>
                                <ListItemIcon>
                                    <Person />
                                </ListItemIcon>
                                <ListItemText inset primary="Profile" />
                                </ListItem></Link>
                                
                            </List>
                            </CardContent>
                        </Card>
                        </Col>
                        <Col md="6">
                            <Route path="/social/" exact component={SocialHomeComponent} />
                            <Route path="/social/profile" component={SocialProfileComponent} />
                            
                        </Col>
                        <Col md="3">
                                <Label>Latest News Update</Label>
                                <section className="newsbar">
                                <NewsSocialContainer  news={this.props.news} />
                                </section>
                        </Col>
                    </Row>
                </Container>
            </section>
        )
    }
    async changePicture(e){
        console.log(e);
        this.setState({
            loading: true
        })
        let formData = new FormData();
        formData.append("image", e.target.files[0]);

        let response = await fetch('/user/change-picture', {
            method: "POST",
            credentials: "include",
            body: formData
        });
        let result = await response.json();
        if(result){
            this.props.fetchUser();
            this.setState({
                loading: false
            })
        }

    }
    
}
function matchStateToProps(state){
    return {
        user: state.auth,
        posts: state.posts,
        news: state.news
    }
}

export default connect(matchStateToProps, actions)(withStyles(styles)(Social));