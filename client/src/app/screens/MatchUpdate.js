import React, { Component } from "react";
import {
    Container,
    TabContent,
    TabPane,
    Row,
    Nav,
    NavItem,
    NavLink
} from "reactstrap";
import { connect } from 'react-redux';
import * as actions from '../actions';
import FixtureRow from '../components/manage-match/MatchCard';
import LineUp from './LineUp';
import classnames from "classnames";
import Statistics from "./Statistics";

class Fixture extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  
    render() {
        return (
            <Container>
                <h4 style={{ fontSize: '5vh' }} className="display-4">{this.props.tour ? this.props.tour.name : ''}</h4>
                <hr />
                <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === "1" })}
                onClick={() => {
                  this.toggle("1");
                }}
              >
                Match
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === "2" })}
                onClick={() => {
                  this.toggle("2");
                }}
              >
                Line up
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === "3" })}
                onClick={() => {
                  this.toggle("3");
                }}
              >
                Statistics
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={this.state.activeTab} style={{ paddingTop: 10 }}>
            <TabPane tabId="1">
              <Row>
              {
                    this.state.fixtures && <div>
                        {
                            this.state.fixtures.map((f, i) => {
                                return <FixtureRow key={i} fixture={f} />
                            })
                        }
                    </div>
                }
              </Row>
            </TabPane>
            <TabPane tabId="2">
              <Row>
              <LineUp/>
              </Row>
            </TabPane>
            <TabPane tabId="3">
              <Row>
              <Statistics/>
              </Row>
            </TabPane>
          </TabContent>
                

            </Container>
        );
    }
    async getTourFixtures() {
        try {
            let response = await fetch('/api/fixture/tournament/' + this.props.match.params.tourId);
            let result = await response.json();
            this.setState({
                fixtures: result
            })
        } catch (error) {
            console.error(error);

        }
    }
}

function mapStateToProps(state) {
    return { tour: state.tour }
}

export default connect(mapStateToProps, actions)(Fixture);
