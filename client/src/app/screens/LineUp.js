import React, { Component } from "react";
import {
    Container,
    TabContent,
    TabPane,
    Row,
    Nav,
    NavItem,
    NavLink,
    Card, CardBody, Button, CardTitle, CardText, CardImg, Col, CardSubtitle
    , Modal, ModalHeader, ModalBody, ModalFooter,
    Group, Label, Input, FormGroup
} from "reactstrap";
import ball from '../assets/img/ball.jpg';

class Fixture extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }
    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }


    render() {
        return (
            <Container>
                <Col sm="12" md={{ size: 8, offset: 2 }}>
                    <Card style={{ height: 570, width: 700 }} outline color="danger">
                        <CardBody>
                            <center><h5>STARTING 11</h5></center>
                            <center>
                                <CardText>
                                    <h4>
                                        <Row>
                                            <Col xs="6" sm="4">ITthynk FC</Col>
                                            <Col xs="6" sm="4">-</Col>
                                            <Col sm="4">Cheche FC</Col>
                                        </Row>
                                    </h4>
                                </CardText>
                                <h5>
                                    <Row>
                                        <Col xs="6" sm="4"><small>Dumu 1 <br/>Tau 2<br/>Tau 2<br/>Tau 2<br/>Tau 2<br/>Tau 2
                                        <br/>Tau 2<br/>Tau 2<br/>Tau 2<br/>Tau 2<br/>Tau 2</small></Col>
                                        <Col xs="6" sm="4"></Col>
                                        <Col sm="4"><small>Dumu 1 <br/>Tau 2<br/>Tau 2<br/>Tau 2<br/>Tau 2<br/>Tau 2<br/>Tau 2
                                        <br/>Tau 2<br/>Tau 2<br/>Tau 2<br/>Tau 2</small></Col>
                                    </Row>
                                </h5>
                                <center><h5>SUBSTITUTES</h5></center>
                                <small>
                                    <Row>
                                        <Col xs="6" sm="4">Tau 2<br/>Tau 2<br/>Tau 2<br/>Tau 2</Col>
                                        <Col xs="6" sm="4"></Col>
                                        <Col sm="4">Tau 2<br/>Tau 2<br/>Tau 2<br/>Tau 2</Col>
                                    </Row>
                                </small>
                                <center><h5>MANAGER</h5></center>
                                <small>
                                    <Row>
                                        <Col xs="6" sm="4">Shibambu</Col>
                                        <Col xs="6" sm="4"></Col>
                                        <Col sm="4">Maluleke</Col>
                                    </Row>
                                </small>
                               
                            </center>
                        </CardBody>
                        <center><small>GIYANI STADIUM</small></center>
                    </Card>
                </Col>
                <center>
                    <Button color="danger" onClick={this.toggle}>Update Line up{this.props.buttonLabel}</Button>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                        <ModalHeader toggle={this.toggle}>Add Line Up</ModalHeader>
                        <ModalBody>
                            <FormGroup>
                                <Label for="exampleSelect">Team Name</Label>
                                <Input type="select" name="select" id="exampleSelect">
                                    <option>--Select--</option>
                                    <option>ITthynk FC</option>
                                    <option>Checha Consulting FC</option>
                                    <option>FITS FC</option>
                                    <option>Dynamics FC</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Players</Label>
                                <Input type="select" name="select" id="exampleSelect">
                                    <option>--Select--</option>
                                    <option>ITthynk FC</option>
                                    <option>Checha Consulting FC</option>
                                    <option>FITS FC</option>
                                    <option>Dynamics FC</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Substitutes</Label>
                                <Input type="select" name="select" id="exampleSelect">
                                    <option>--Select--</option>
                                    <option>ITthynk FC</option>
                                    <option>Checha Consulting FC</option>
                                    <option>FITS FC</option>
                                    <option>Dynamics FC</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Coach</Label>
                                <Input type="select" name="select" id="exampleSelect">
                                    <option>--Select--</option>
                                    <option>ITthynk FC</option>
                                    <option>Checha Consulting FC</option>
                                    <option>FITS FC</option>
                                    <option>Dynamics FC</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Stadium</Label>
                                <Input type="select" name="select" id="exampleSelect">
                                    <option>--Select--</option>
                                    <option>ITthynk FC</option>
                                    <option>Checha Consulting FC</option>
                                    <option>FITS FC</option>
                                    <option>Dynamics FC</option>
                                </Input>
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={this.toggle}>Submit</Button>{' '}
                            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                        </ModalFooter>
                    </Modal>
                </center>
            </Container>
        );
    }

}


export default (Fixture);
