import React, { Component } from 'react';
import {
    Container, Row, Col,
    ListGroup, ListGroupItem, Collapse
} from 'reactstrap';
import { Route, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import Dashboard from '../components/manage/home';
import Team from '../components/manage/team';
import Coach from '../components/manage/coach';
import Info from '../components/manage/info';
import Setting from '../components/manage/setting';
import Player from '../components/manage/players';

class Manage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true
        }
    }
    render() {
        return (
            <div>
                <Container>
                    <hr />
                    <Collapse> isOpen={!this.state.open}
                    </Collapse>
                    <Row>

                        <Col md={3}>
                            <Collapse isOpen={this.state.open}>
                                <ListGroup>
                                    <ListGroupItem>
                                        <NavLink to="/manage/" className="nav-link text-secondary"><i className="fa fa-home"></i> Dashboard</NavLink>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <NavLink to="/manage/teams" className="nav-link text-secondary"><i className="fa fa-users"></i> Teams</NavLink>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <NavLink to="/manage/coaches" className="nav-link text-secondary"><i className="fa fa-user"></i> Coaches</NavLink>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <NavLink to="/manage/info" className="nav-link text-secondary"><i className="fa fa-info"></i> Information Center</NavLink>
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <NavLink to="/manage/settings" className="nav-link text-secondary"><i className="fa fa-cogs"></i> Settings</NavLink>
                                    </ListGroupItem>
                                </ListGroup>

                            </Collapse>
                        </Col>
                        <Col>
                            <Route path="/manage/" exact component={Dashboard} />
                            <Route path="/manage/teams" exact component={Team} />
                            <Route path="/manage/coaches" component={Coach} />
                            <Route path="/manage/info" component={Info} />
                            <Route path="/manage/settings" component={Setting} />
                            <Route path="/manage/teams/:teamId" component={Player} />
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

function matchStateToProps(state) {
    return {
        user: state.auth
    }
}

export default connect(matchStateToProps)(Manage);