import React, {Component} from 'react';
import {Container, Row, Col} from 'reactstrap';
import { withStyles } from 'material-ui/styles';
import SwipeableViews from 'react-swipeable-views';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import TournamentBar from '../components/appBar/appBar';
import LeagueTable from '../components/tourTable/leagueTable';
import {connect} from 'react-redux';

const styles = theme => ({
    root: {
      backgroundColor: theme.palette.background.paper,
    },
  });

class Table extends Component{
    constructor(props){
        super(props);
        this.state = {
            value: 0
        }
    }
    render(){
        
        const { classes, theme } = this.props;
        return (
                <div className={classes.root}>
                    <TournamentBar value={this.state.value} handleChange={this.handleChange} />
                    <SwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={this.state.value}
                    onChangeIndex={this.handleChangeIndex}
                    >
                    {
                        this.props.province.map((item, i)=>{
                            return(
                                <div dir={theme.direction}><LeagueTable fp={item.name} province={item.short_name} phases={this.props.phases} /></div>
                            )
                        })
                    }
                    </SwipeableViews>
            </div>
        )
    }
    handleChange = (event, value) => {
        this.setState({ value });
      };
    
      handleChangeIndex = index => {
        this.setState({ value: index });
      };
    
}
function matchStateToProps(state){
    return {
        tournaments: state.tournaments,
        phases: state.phases,
        province: state.province
    }
}

export default connect(matchStateToProps)(withStyles(styles, { withTheme: true })(Table));