import React, { Component } from "react";
import {
    Container,
    TabContent,
    TabPane,
    Row,
    Nav,
    NavItem,
    NavLink,
    Card, CardBody, Button, CardTitle, CardText, CardImg, Col, CardSubtitle, Modal, ModalHeader, ModalBody, ModalFooter,
    FormGroup, Label, Input
} from "reactstrap";


class Fixture extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeTab: "1"
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }


    render() {
        return (
            <Container>
                <Col sm="12" md={{ size: 8, offset: 2 }}>
                    <Card style={{ height: 150, width: 700 }} outline color="danger">
                        <CardBody outline>
                            <center>
                                <CardText>
                                    <h4>
                                        <Row>
                                            <Col xs="6" sm="4">ITthynk FC</Col>
                                            <Col xs="6" sm="4">TEAM STATS</Col>
                                            <Col sm="4">Cheche FC</Col>
                                        </Row>
                                    </h4>
                                </CardText>

                                <small>
                                    <Row>
                                        <Col xs="6" sm="4">3</Col>
                                        <Col xs="6" sm="4">Yellow Cards</Col>
                                        <Col sm="4">2</Col>
                                    </Row>
                                </small>
                                <small>
                                    <Row>
                                        <Col xs="6" sm="4">3</Col>
                                        <Col xs="6" sm="4">Red Cards</Col>
                                        <Col sm="4">7</Col>
                                    </Row>
                                </small>
                                <small>
                                    <Row>
                                        <Col xs="6" sm="4">6</Col>
                                        <Col xs="6" sm="4">Offsides</Col>
                                        <Col sm="4">2</Col>
                                    </Row>
                                </small>
                                <small>
                                    <Row>
                                        <Col xs="6" sm="4">0</Col>
                                        <Col xs="6" sm="4">Corners</Col>
                                        <Col sm="4">2</Col>
                                    </Row>
                                </small>
                            </center>
                        </CardBody>

                    </Card>
                </Col>
                <center>
                    <Button color="danger" onClick={this.toggle}>Update Line up{this.props.buttonLabel}</Button>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                        <ModalHeader toggle={this.toggle}>Add Line Up</ModalHeader>
                        <ModalBody>
                            <FormGroup>
                                <Label for="exampleSelect">Team Name</Label>
                                <Input type="select" name="select" id="exampleSelect">
                                    <option>--Select--</option>
                                    <option>ITthynk FC</option>
                                    <option>Checha Consulting FC</option>
                                    <option>FITS FC</option>
                                    <option>Dynamics FC</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Yellow Cards</Label>
                                <Input type="number" name="select" id="exampleSelect" min="0" max="11">
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Red Cards</Label>
                                <Input type="number" name="select" id="exampleSelect" min="0" max="5">
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Offsides</Label>
                                <Input type="number" name="select" id="exampleSelect" min="0" max="100">
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect">Corners</Label>
                                <Input type="number" name="select" id="exampleSelect" min="0" max="50">
                                </Input>
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="danger" onClick={this.toggle}>Submit</Button>{' '}
                            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                        </ModalFooter>
                    </Modal>
                </center>
            </Container>
        );
    }

}


export default (Fixture);
