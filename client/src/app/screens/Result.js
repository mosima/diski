import React, {Component} from 'react';
import {Container, Row, Col} from 'reactstrap';
import { withStyles } from 'material-ui/styles';
import SwipeableViews from 'react-swipeable-views';
import TournamentBar from '../components/appBar/appBar';
import ResultContainer from '../components/result/resultContainer';

const styles = theme => ({
    root: {
      backgroundColor: theme.palette.background.paper,
    },
  });

class Result extends Component{
    constructor(props){
        super(props);
        this.state = {
            value: 0
        }
    }
    render(){
        const { classes, theme } = this.props;
        return (
            <div className={classes.root}>
            <TournamentBar value={this.state.value} handleChange={this.handleChange} />
            <SwipeableViews
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={this.state.value}
            onChangeIndex={this.handleChangeIndex}
            >
                <div dir={theme.direction}><ResultContainer text='U-17' /></div>
                <div dir={theme.direction}><ResultContainer text='U-15' /></div>
                <div dir={theme.direction}><ResultContainer text='U-13' /></div>
                <div dir={theme.direction}><ResultContainer text='Group 1' /></div>
                <div dir={theme.direction}><ResultContainer text='Group 2' /></div>
            </SwipeableViews>
    </div>
        )
    }
    handleChange = (event, value) => {
        this.setState({ value });
      };
    
      handleChangeIndex = index => {
        this.setState({ value: index });
      };
}

export default withStyles(styles, { withTheme: true })(Result);