import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  Button,
  CardFooter,
  CardBody,
  CardTitle,
  CardText,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";
import { connect } from "react-redux";
import classnames from "classnames";
import { Link} from 'react-router-dom';
import TourCard from '../components/tounament/TourCard';

class Tournament extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: "1"
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    return (
      <div>
        <Container>
          <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === "1" })}
                onClick={() => {
                  this.toggle("1");
                }}
              >
                Active Tournaments
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === "2" })}
                onClick={() => {
                  this.toggle("2");
                }}
              >
                Upcoming Tournaments
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={this.state.activeTab} style={{ paddingTop: 10 }}>
            <TabPane tabId="1">
              <Row>
                {
                  this.props.tournaments.map((t, i)=>{
                    return <TourCard key={i} tour={t} />
                  })
                }
              </Row>
            </TabPane>
            <TabPane tabId="2">
              <Row>
                <Col md={4}>
                  <Card>
                    <CardBody>
                      <CardTitle className="display-4 text-danger">
                        Barclays Premier League
                      </CardTitle>
                      <CardText className="lead">
                        With supporting text below as a natural lead-in to
                        additional content.
                      </CardText>
                    </CardBody>
                    <CardFooter>
                      <Link to="/fixture">
                       <Button color="danger">Result &amp; Fixtures</Button>{" "}
                      </Link>
                      <Button color="danger">View Logs</Button>
                    </CardFooter>
                  </Card>
                </Col>
              </Row>
            </TabPane>
          </TabContent>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    tournaments: state.tournaments
  };
}


export default connect(mapStateToProps)(Tournament);
