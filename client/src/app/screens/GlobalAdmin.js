import React, { Component } from "react";
import { Container, Row, Col, ListGroup, ListGroupItem } from "reactstrap";
import { withRouter, NavLink, Route } from "react-router-dom";
import Tour from "../components/global/tour/Tour";
import TourSingle from "../components/global/tour/TourSingline";
import Users from '../components/global/users/Users';
import PerfectScrollbar from "react-perfect-scrollbar";
import TourFixture from "../components/global/tour/Fixture";

class GlobalAdmin extends Component {
    render() {
        return (
            <div>
                <Container>
                    <hr />
                    <Row>
                        <Col md={3}>
                            <PerfectScrollbar>
                                <ListGroup>
                                    <ListGroupItem>Home</ListGroupItem>
                                    <ListGroupItem>
                                        <NavLink to="/global-admin/tournament">Tournaments</NavLink>
                                    </ListGroupItem>
                                    <ListGroupItem>Fixtures</ListGroupItem>
                                    <ListGroupItem>Results</ListGroupItem>
                                    <ListGroupItem>Tournament Logs</ListGroupItem>
                                    <ListGroupItem>
                                    <NavLink to="/global-admin/users">Users</NavLink>
                                    </ListGroupItem>
                                </ListGroup>
                            </PerfectScrollbar>
                        </Col>
                        <Col md={9}>
                            <PerfectScrollbar>
                                <Route path="/global-admin/tournament" exact component={Tour} />
                                <Route
                                    path="/global-admin/tournament/:tourId"
                                    component={TourSingle}
                                />
                                <Route
                                    path="/global-admin/fixture/:tourId"
                                    component={TourFixture}
                                />
                                <Route
                                    path="/global-admin/users"
                                    component={Users}
                                />
                            </PerfectScrollbar>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default withRouter(GlobalAdmin);
