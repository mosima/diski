import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import HeaderComponent from './app/components/header/header';
import Home from './app/screens/Home';
import Tournament from './app/screens/Tournament';
import Manage from './app/screens/Manage';
import Register from './app/screens/Register';
import GlobalAdmin from './app/screens/GlobalAdmin';
import Fixture from './app/screens/Fixture';
import Log from './app/screens/Log';
import Match from './app/screens/Match';
import ManageMatch from './app/screens/ManageMatch';
import MatchUpdate from './app/screens/MatchUpdate';
// import Gallery from './app/screens/Gallery';
// import News from './app/screens/News';
// import Result from './app/screens/Result';
// import School from './app/screens/School';
// import Social from './app/screens/Social';
// import Table from './app/screens/Table';
// import Showcase from './app/screens/Showcase';
// import Spotlight from './app/screens/Spotlight';
// import SchoolDetail from './app/screens/SchoolDetail';
import { connect } from 'react-redux';
import * as actions from './app/actions';



class App extends Component {
  constructor() {
    super()

    this.userLogout = this.userLogout.bind(this);
  }
  render() {
    return (
      <BrowserRouter>
        <div>
          <HeaderComponent logout={this.userLogout} user={this.props.user} />
          <Route exact path="/" component={Home} />
          <Route path="/tournament" component={Tournament} />
          <Route path="/manage" component={Manage} />
          <Route path="/register-user" component={Register} />
          <Route path="/global-admin" component={GlobalAdmin} />
          <Route path='/fixture/:tourId' component ={Fixture}/>
          <Route path='/log/:tourId' component ={Log}/>
          <Route path='/match' component ={Match}/>
          <Route path='/manage-match' component ={ManageMatch}/>
          <Route path="/matchupdate/:tourId"  component={MatchUpdate} />
          <Route path="/Lineup" component={ManageMatch}/>
          {/* <Route exact path="/" component={Home} />
          <Route path='/fixture' component ={Fixture}/>
          <Route path="/result" component={Result} />
          <Route path="/news" component={News} />
          <Route path="/social" component={Social} />
          <Route path="/showcase" component={Showcase} />
          <Route path="/table" component={Table} />
          <Route path="/gallery" component={Gallery} />
          <Route exact path="/school" component={School} />
          <Route path="/school/:id" component={SchoolDetail} />
          <Route path="/spotlight" component={Spotlight} /> */}
          
          
        </div>
      </BrowserRouter>
    );
  }
  async userLogout() {
    fetch('/user/logout', { credentials: "include" })
      .then(res => { window.location.href = "/" });
  }

  componentDidMount() {
    this.props.fetchUser();
    this.props.fetchMyTeam();
    this.props.fetchMyCoach();
    this.props.fetchMyPlayer();
    this.props.fetchAllTeam();
    this.props.fetchUsers();
    // this.props.fetchPost();
    // this.props.fetchSchool();
    // this.props.fetchNews();
     this.props.fetchTournament();
    // this.props.fetchFixture();
    // this.props.fetchProvince();
    // this.props.fetchPhase();
    // this.props.fetchSpotlight();
    // this.props.fetchShowcase();
  }
}

function matchStateToProps(state) {
  return {
    user: state.auth
  }
}

export default connect(matchStateToProps, actions)(App);
