var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var teamSchema = new Schema({
    name: String,
    owner: { type: Schema.Types.ObjectId, ref: 'User' },
    year: {type: Date},
    team_logo: { type: String, default: '' },
    point: {type: Number, default: 0},
    total_games_played: {type: Number, default: 0},
    lifetime_total_games_played: {type: Number, default: 0},
    season_games_played: {type: Number, default: 0},
    games_won: {type: Number, default: 0},
    games_lost: {type: Number, default: 0},
    lifetime_total_yellow_card: {type: Number, default: 0},
    lifetime_total_red_card: {type: Number, default: 0},
    team_total_yellow_card: {type: Number, default: 0},
    team_total_red_card: {type: Number, default: 0},
    games_drawn: {type: Number, default: 0},
    season_goal_for: {type: Number, default: 0},
    season_goal_against: {type: Number, default: 0},
    total_team_goal: {type: Number, default: 0},
    type: { type: Schema.Types.ObjectId, ref: 'TeamType' },
    status: { type: String, default: 'active', enum: ['active', 'inactive'] },
    venue: { type: String, default: '' },
    location: [Number],
    coach: {type: Schema.Types.ObjectId, ref: 'Coach'},
    //edit
    news:[{type: Schema.Types.ObjectId, ref:'News'}],
    players: [{type: Schema.Types.ObjectId, ref: 'Player'}]
},{timestamps: {'createdAt': 'createdAt'}});

teamSchema.indexes({name: 'text'}, {location: '2dSphere'});

module.exports = mongoose.model('Team', teamSchema);

 