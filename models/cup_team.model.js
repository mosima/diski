const mongoose =  require('mongoose');
const {Schema} = mongoose;

const cupTeamSchema = new Schema({
    team: {type: Schema.Types.ObjectId, ref: 'Team'},
    tournament: {type: Schema.Types.ObjectId, ref: 'Tournament'},
    goal_scored: {type: Number, default: 0},
    goal_conceded: {type: Number, default: 0},
    match_played: {type: Number, default: 0},
    match_won: {type: Number, default: 0},
    match_lost: {type: Number, default: 0},
    match_drawn: {type: Number, default: 0},
    goalDifference: {type: Number, default: 0},
    point: {type: Number, default: 0},
    players: [{type: Schema.Types.ObjectId, ref: 'CupTeamPlayer'}]
});

module.exports = mongoose.model('CupTeam', cupTeamSchema);