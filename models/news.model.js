var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var newsSchema = new Schema({

	owner:{type: Schema.Types.ObjectId, ref:'User'},
	title: {type: String, default: ''},
	team: {type:Schema.Types.ObjectId , ref: 'School'},
	body:{type:String, default: ''},
	media: {type: String , default: ''}},
	{timestamps: {createdAt: 'createdAt', updatedAt: 'updatedAt'}}

);

module.exports = mongoose.model('news', newsSchema);
