var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tournamentTypeSchema = new Schema({
    type: {type: String, default: ''},
    tournaments: [{type: Schema.Types.ObjectId, ref:'Tournament'}]
});

module.exports = mongoose.model('TournamentType', tournamentTypeSchema);