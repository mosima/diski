var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var coachSchema = new Schema({
    firstname: { type: String, default: '' },
    middlename: { type: String, default: '' },
    lastname: { type: String, default: '' },
    date_of_birth: Date,
    status: { type: String, default: 'active', enum: ['active', 'inactive'] },
    profile: { type: String, default: '' },
    team: { type: Schema.Types.ObjectId, ref: 'Team' },
    teams: [{type: Schema.Types.ObjectId, ref: 'Team'}],
    created_by: {type: Schema.Types.ObjectId, ref: 'User'}
}, {timestamps: {createdAt: true}});

module.exports = mongoose.model('Coach', coachSchema);
