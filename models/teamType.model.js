var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var teamTypeSchema = new Schema({
    type: {type: String, default: ''},
    teams: [{type: Schema.Types.ObjectId, ref:'Team'}]

});

module.exports = mongoose.model('TeamType', teamTypeSchema);