var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var playerSchema = new Schema({
    firstname: { type: String, default: '' },
    middlename: { type: String, default: '' },
    lastname: { type: String, default: '' },
    address: { type: String, default: '' },
    suburb: { type: String, default: '' },
    city: { type: String, default: '' },
    province: { type: String, default: '' },
    postalCode: { type: String, default: '' },
    dateOfBirth: {type: Date},
    class: {type: String},
    cell: { type: String, default: '' },
    email: { type: String, default: '' },
    registeredBy: { type: Schema.Types.ObjectId, ref: 'User' },
    shirtNumber: { type: Number, default: 0 },
    school: { type: Schema.Types.ObjectId, ref: 'School' },
    team: {type: Schema.Types.ObjectId, ref: 'Team'},
    status: { type: String, default: 'active', enum: [ 'active', 'inactive' ] },
    picture: { type: String, default: 'http://images.clipartpanda.com/user-clipart-xigojzxKT.png' },
    season_goal_scored: {type: Number, default: 0},
    career_goal_scored: {type: Number, default: 0},
    season_yellow_card: {type: Number, default: 0},
    career_yellow_card: {type: Number, default: 0},
    season_red_card: {type: Number, default: 0},
    career_red_card: {type: Number, default: 0},
    season_games_played: {type: Number, default: 0},
    career_games_played: {type: Number, default: 0}
});

module.exports = mongoose.model('Player', playerSchema);