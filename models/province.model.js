'use strict';
const mongoose = require('mongoose');
const {Schema} = mongoose;

const provinceModel = new Schema({
    name: {type: String, required: true},
    short_name: {type: String, required: true}
});

module.exports = mongoose.model('Province', provinceModel);