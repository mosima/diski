var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var fixtureSchema = new Schema({
    home_team: {type: Schema.Types.ObjectId, ref: "Team"},
    away_team: {type: Schema.Types.ObjectId, ref: "Team"},
    played: {type: Boolean, default: false},
    tournament: {type: Schema.Types.ObjectId, ref: "Tournament"},
    match_date: {type: Date},
    group: {type: Schema.Types.ObjectId, ref: 'Group'},
    cup_team_home: {type: Schema.Types.ObjectId, ref: 'CupTeam'},
    cup_team_away: {type: Schema.Types.ObjectId, ref: 'CupTeam'},
    location: [Number],
    province: {type: String},
    createdBy: {type: Schema.Types.ObjectId, ref: "User"},
    modifiedBy: {type: Schema.Types.ObjectId, ref: "User"}
}, {timestamps: {"createdAt": "createdAt", "updatedAt": "updatedAt"}});

fixtureSchema.index({location: '2dsphere'});

module.exports = mongoose.model('Fixture', fixtureSchema);
