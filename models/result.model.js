var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var resultSchema = new Schema({
    fixture: {type: Schema.Types.ObjectId, ref: 'Fixture'},
    home_team: {type: Schema.Types.ObjectId, ref: 'CupTeam'},
    home_team_score: {type: Number, required: true, default: 0},
    away_team: {type: Schema.Types.ObjectId, ref: 'CupTeam'},
    away_team_score: {type: Number, required: true, default: 0},
    tournament: {type: Schema.Types.ObjectId, ref: 'Tournament'},
    goal: {type:  Schema.Types.ObjectId, ref: 'Goal'},
    bookedPlayer: {type: Schema.Types.ObjectId, ref:'Player'}
});

module.exports = mongoose.model('Result', resultSchema);
