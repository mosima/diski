const mongoose = require('mongoose');
const {Schema} = mongoose;

const schoolSchema = new Schema({
    Province: {type: String},
    Institution_Name: {type: String},
    Status: {type: String},
    Sector: {type: String},
    Type_DoE: {type: String},
    Phase: {type: String},
    Specialization: {type: String},
    OwnerLand: {type: String},
    OwnerBuild: {type: String},
    Ownership: {type: String},
    Location: [Number],
    Magisterial_District: {type: String},
    EIDistrict: {type: String},
    LMunName: {type: String},
    ExDept: {type: String},
    Ward_ID: {type: String},
    EIRegion: {type: String},
    EIDistrict: {type: String},
    Suburb: {type: String},
    Town_City: {type: String},
    StreetAddress: {type: String},
    PostalAddress: {type: String},
    Telephone: {type: String},
    Facsimile: {type: String},
    cellno: {type: String},
    E_Mail: {type: String},
    RegistrationDate: {type: Date},
    Urban_Rural: {type: String},
    point: {type: Number, default: 0},
    total_games_played: {type: Number, default: 0},
    lifetime_total_games_played: {type: Number, default: 0},
    season_games_played: {type: Number, default: 0},
    games_won: {type: Number, default: 0},
    games_lost: {type: Number, default: 0},
    lifetime_total_yellow_card: {type: Number, default: 0},
    lifetime_total_red_card: {type: Number, default: 0},
    team_total_yellow_card: {type: Number, default: 0},
    team_total_red_card: {type: Number, default: 0},
    games_drawn: {type: Number, default: 0},
    season_goal_for: {type: Number, default: 0},
    season_goal_against: {type: Number, default: 0},
    total_team_goal: {type: Number, default: 0}
});

schoolSchema.index({Location: '2dsphere'})

module.exports = mongoose.model('School', schoolSchema);
