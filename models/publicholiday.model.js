'use strict';

const mongoose = require('mongoose');
const {Schema} = mongoose;

const holidaySchema = new Schema({
    name: {type: String, required: true},
    date: {type: String, required: true},
    session: {type: Number}
}, {timestamps: {createdAt: 'createdAt'}});

module.exports = mongoose.model('Holiday', holidaySchema);