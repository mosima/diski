const mongoose = require('mongoose');
const {Schema} = mongoose;

const goalSchema = new Schema({
    fixture: {type: Schema.Types.ObjectId, ref: 'Fixture'},
    player: {type: Schema.Types.ObjectId, ref: 'Player'},
    team: {type: Schema.Types.ObjectId, ref: 'Team'}
}, {timestamps: {createdAt: true}});

module.exports = mongoose.model('Goal', goalSchema);