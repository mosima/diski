const mongoose = require('mongoose');
const {Schema} = mongoose;

const lineupSchema = new Schema({
    goalkeeper: {type: Schema.Types.ObjectId, ref: 'Player'},
    defenders: [{type: Schema.Types.ObjectId, ref: 'Player'}],
    midfielders: [{type: Schema.Types.ObjectId, ref: 'Player'}],
    attackers: [{type: Schema.Types.ObjectId, ref: 'Player'}],
    fixture: {type: Schema.Types.ObjectId, ref: 'Fixture'},
    team: {type: Schema.Types.ObjectId, ref: 'Team'}
})

module.exports = mongoose.model('Lineup', lineupSchema);