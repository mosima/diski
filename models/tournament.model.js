var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tournamentSchema = new Schema({
    name: String,
    type: { type: String, default: 'cup', enum: ['cup', 'league', 'exhibition']},
    numberOfTeams: { type: Number, default: 2 },
    teams: [{ type: Schema.Types.ObjectId, ref: 'Team' }],
    status: {type: String, default: 'active', enum:['active', 'ongoing', 'closed']},
    fixtures: [{type: Schema.Types.ObjectId, ref: 'Fixture'}],
    groups: [{type: Schema.Types.ObjectId, ref: 'Group'}],
    results: [{type: Schema.Types.ObjectId, ref: 'Result'}],
    cup_team: [{type: Schema.Types.ObjectId, ref: 'CupTeam'}]
});

module.exports = mongoose.model('Tournament', tournamentSchema);