'use strict';
const mongoose = require('mongoose');
const {Schema} = mongoose;

const districtSchema = new Schema({
  district: {type: String},
  province: {type: String},
  schools: [{type: Schema.Types.ObjectId, ref: 'District'}]
});

districtSchema.index({district: 'text'});

module.exports = mongoose.model('District', districtSchema);
