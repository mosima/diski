const mongoose = require('mongoose');
const {Schema} = mongoose;

const cupPlayer = new Schema({
    player: {type: Schema.Types.ObjectId, ref: 'Player'},
    team: {type: Schema.Types.ObjectId, ref: 'CupTeam'},
    goal_scored: {type: Number, default: 0},
    yellow_card: {type: Number, default: 0},
    red_card: {type: Number, default: 0},
    match_played: {type: Number, default: 0}
});

module.exports = mongoose.model('CupTeamPlayer', cupPlayer);