'use strict';
const mongoose = require('mongoose');
const {Schema} = mongoose;

const wardSchema = new Schema({
  ward: {type: String, required: true},
  schools: [{type: Schema.Types.ObjectId, ref: 'School'}],
  province: String
});

module.exports = mongoose.model('Ward', wardSchema);
