'use strict';
const mongoose = require('mongoose');
const {Schema} = mongoose;

const phaseSchema = new Schema({
  phase: {type: String},
  schools: [{type: Schema.Types.ObjectId, ref: 'Phase'}]
});

module.exports = mongoose.model('Phase', phaseSchema);
