var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var groupSchema = new Schema({
    tournament: {type: Schema.Types.ObjectId, ref: 'Tournament'},
    name: {type: String, default: ''},
    teams: [{type: Schema.Types.ObjectId, ref: 'CupTeam'}]
});

module.exports = mongoose.model('Group', groupSchema);