'use strict';

const mongoose = require('mongoose');
const {Schema} = mongoose;

const termSchema = new Schema({
    number: {type: Number, required: true},
    start_date: {type: Date, required: true},
    end_date: {type: Date, required: true},
    year: {type: Number, required: true}
}, {timestamps: {createdAt: 'createdAt'}});

module.exports = mongoose.model('Term', termSchema);