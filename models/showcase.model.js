'use strict';

const mongoose = require('mongoose');
const {Schema} = mongoose;

const showcaseSchema = new Schema({
  media: {type: String},
  title: {type: String},
  description: {type: String}
}, {timestamps: {createdAt: 'createdAt'}});

module.exports = mongoose.model('Showcase', showcaseSchema);
