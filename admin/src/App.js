import React, { Component } from 'react';
import './App.css';
import  LoginForm from './app/screens/Login';
import {BrowserRouter, Route, Link} from 'react-router-dom';
import {amber, red, indigo} from 'material-ui/colors';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import { Sidebar, Segment, Button, Menu, Image, Icon, Header } from 'semantic-ui-react';
import HeaderComponent from './app/components/header/headerComponent';
import Home from './app/screens/Home';
import School from './app/screens/School';
import Fixture from './app/screens/Fixture';
import Result from './app/screens/Result';
import Log from './app/screens/Log';
import Profile from './app/screens/Profile';
import EditProfile from './app/screens/EditProfile';
import Configuration from './app/screens/Configuration';
import {connect} from 'react-redux';
import * as actions from './app/actions';

const theme = createMuiTheme({
  palette:{
    primary: red
  }
});

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      visible: false
    }
  }
  toggleVisibility = () => this.setState({ visible: !this.state.visible });
  render() {
    return (
      <BrowserRouter>
      <MuiThemeProvider theme={theme}>
      <div style={{height: 800}}>
            {
              (()=>{
                if(this.props.user && this.props.user.isAdmin){
                  return(
                    <Sidebar.Pushable as={Segment}>
                    <Sidebar as={Menu} animation='push' width='thin' visible={this.state.visible} icon='labeled' vertical inverted>
                    <Menu.Item>
                      <Image src={this.props.user.picture} />
                    </Menu.Item>
                    <Link to="/"><Menu.Item name='home'>
                        <Icon name='home' />
                        Home
                      </Menu.Item></Link>
                      <Link to="/profile"><Menu.Item name='user'>
                        <Icon name='user' />
                        Profile
                      </Menu.Item></Link>
                      <Link to="/school"><Menu.Item name='university'>
                        <Icon name='university' />
                        School
                      </Menu.Item></Link>
                      <Link to="/fixture"><Menu.Item name='calendar'>
                        <Icon name='calendar' />
                        Fixtures
                      </Menu.Item></Link>
                      <Link to="/result"><Menu.Item name='browser'>
                        <Icon name='browser' />
                        Results
                      </Menu.Item></Link>
                      <Link to="/logs"><Menu.Item name='list ol'>
                        <Icon name='list ol' />
                        Logs
                      </Menu.Item></Link>
                      <Link to="/configuration"><Menu.Item name='gear'>
                        <Icon name='settings' />
                        Configuration
                      </Menu.Item></Link>
                    </Sidebar>
                    <Sidebar.Pusher>
                      <Segment basic>
                        <HeaderComponent toggleVisibility={this.toggleVisibility} />
                        <Route exact path="/" component={Home} />
                        <Route path='/fixture' component ={Fixture}/>
                        <Route path="/result" component={Result} />
                        <Route path="/school" component={School} />
                        <Route path="/logs" component={Log} />
                        <Route path="/profile" component={Profile} />
                        <Route path="/configuration" component={Configuration} />
                        <Route path="/edit-profile" component={EditProfile} />
                      </Segment>
                    </Sidebar.Pusher>
                  </Sidebar.Pushable>
                  )
                }else{
                  return(
                  <LoginForm />
                )
                }
              })()
            }
              
            </div>
      </MuiThemeProvider>
      </BrowserRouter>
    );
  }
  componentDidMount(){
    this.props.fetchUser();
     this.props.fetchPost();
     this.props.fetchSchool();
      this.props.fetchNews();
     this.props.fetchFixture();
     this.props.fetchProvince();
     this.props.fetchPhase();
  }
}
function matchStateToProp(state){
  return {
    user: state.auth
  }
}

export default connect(matchStateToProp, actions)(App);
