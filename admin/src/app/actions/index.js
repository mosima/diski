import { FETCH_USER, FETCH_POST, FETCH_SCHOOL, FETCH_PHASE, FETCH_DISTRICT, FETCH_FIXTURE, FETCH_NEWS, FETCH_TOURNAMENT, FETCH_PROVINCE } from './types';

export const fetchUser = () => async dispatch => {
    const res = await fetch('/user/get-current-user', {credentials: "include"});
    const data = await res.json();
    
    if(Object.keys(data).length === 0 && data.constructor === Object){
     dispatch({type: FETCH_USER, payload: false});
    }else{
     dispatch({type: FETCH_USER, payload: data});
    }
    
     
 };

    export const fetchPost = () => async dispatch =>{
        const res = await fetch('/api/post');
        const data = await res.json();

        dispatch({type: FETCH_POST, payload: data.data});
    }

    export const fetchSchool = () => async dispatch =>{
        const res = await fetch('/api/school');
        const data = await res.json();
        dispatch({type: FETCH_SCHOOL, payload: data});
    }


    export const fetchPhase = () => async dispatch =>{
        const res = await fetch('/api/phase');
        const data = await res.json();

        dispatch({type: FETCH_PHASE, payload: data});
    }

    export const fetchProvince = () => async dispatch =>{
        const res = await fetch('/api/province');
        const data = await res.json();
        dispatch({type: FETCH_PROVINCE, payload: data.province});
    }

    export const fetchNews = () => async dispatch =>{
        const res = await fetch('/api/news');
        const data = await res.json();

        dispatch({type: FETCH_NEWS, payload: data});
    }
    export const fetchFixture = () => async dispatch =>{
        const res = await fetch('/api/fixture');
        const data = await res.json();

        dispatch({type: FETCH_FIXTURE, payload: data});
    }
    export const fetchTournament = () => async dispatch =>{
        const res = await fetch('/api/tournament');
        const data = await res.json();

        dispatch({type: FETCH_TOURNAMENT, payload: data});
    }