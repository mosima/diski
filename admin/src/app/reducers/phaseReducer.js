import {FETCH_PHASE} from '../actions/types';

export default function(state = [], action){
    switch(action.type){
        case FETCH_PHASE:
        return action.payload || false;
         default:
        return state;
    }
}