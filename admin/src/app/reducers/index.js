import {combineReducers} from 'redux';
import authReducer from './authReducer';
import postReducer from './postReducer';
import fixtureReducer from './fixtureReducer';
import newsReducer from './newsReducer';
import tournamentReducer from './tournamentReducer';
import provinceReducer from './provinceReducer';
import phaseReducer from './phaseReducer';
import schoolReducer from './schoolReducer';

export default combineReducers({
    auth: authReducer,
    posts: postReducer,
    news: newsReducer,
    province: provinceReducer,
    fixtures: fixtureReducer,
    tournaments: tournamentReducer,
    phases: phaseReducer,
    schools: schoolReducer
});