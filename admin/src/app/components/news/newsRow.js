import React, {Component} from 'react';
import {Table, Header, Image, Icon, Button} from 'semantic-ui-react';
import {format} from 'date-fns';

class NewsRowComponent extends Component{
    render(){
        return(
            <Table.Row>
                <Table.Cell>
                <Header as='h4' image>
                    <Image src={this.props.item.media} rounded size='mini' />
                    <Header.Content>
                    {this.props.item.title}
                    </Header.Content>
                </Header>
                </Table.Cell>
                <Table.Cell>{format(this.props.item.createdAt, "Do MMMM YYYY")}</Table.Cell>
                <Table.Cell>
                <Button basic onClick={this.deleteItem.bind(this)} circular color="red" icon><Icon name="trash outline" color="red" /></Button>
                <Button basic circular icon><Icon name="edit" /></Button>
                    </Table.Cell>
            </Table.Row>
        )
    }
    deleteItem(){
        this.props.deleteItem(this.props.item._id);
    }
}

export default NewsRowComponent;