import React, {Component} from 'react';
import {Icon, Table, Button, Dimmer, Loader, Header, Modal, Image, Form} from 'semantic-ui-react';
import {format} from 'date-fns';
import {withRouter} from 'react-router-dom';
import NewsRowComponent from './newsRow';

class NewsPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            newsArr: [],
            modalOpen: false,
            loading: false
        }
        this.addNews = this.addNews.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
    }
    render(){
        return (
            <section>
                {(()=>{
                    if(this.state.newsArr.length < 0){
                        return(
                            <Dimmer active>
                                <Loader>Loading</Loader>
                            </Dimmer>
                        );
                    }else{
                        return(
                            <section>
                                <Dimmer active={this.state.loading}>
                                <Loader>Processing</Loader>
                                 </Dimmer>
                                <Modal dimmer={false} open={this.state.modalOpen} onClose={this.close} size="small">
                                    <Modal.Header>Add News</Modal.Header>
                                    <Modal.Content>
                                    <Form>
                                        <Form.Input
                                        required
                                            placeholder="Title"
                                            onChange={(e)=>{this.setState({title: e.target.value})}}
                                        />
                                        <Form.Input
                                        required
                                            placeholder="Body"
                                            onChange={(e)=>{this.setState({description: e.target.value})}}
                                        />
                                        <Form.Input
                                        required
                                        type="file"
                                        onChange={(e)=>{this.setState({media: e.target.files[0]})}}
                                        />
                                    </Form>
                                </Modal.Content>
                                <Modal.Actions>
                                <Button icon onClick={this.addNews} color='green' inverted>
                                    <Icon name='checkmark' /> Add
                                </Button>
                                <Button icon onClick={()=>{this.setState({modalOpen: false})}} color='red' inverted>
                                    <Icon name='remove' /> Close
                                </Button>
                                </Modal.Actions>
        </Modal>
                           
                        <Button color="black" onClick={()=>{this.setState({modalOpen: true})}} icon><Icon name="plus"  />New</Button>
                        <Table basic="very" celled>
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell>Title</Table.HeaderCell>
                            <Table.HeaderCell>Date Added</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                    
                        <Table.Body>
                            {this.state.newsArr.map((item, i)=>{
                            return(
                                
                                <NewsRowComponent key={i} item={item} deleteItem={this.deleteItem} />
                            
                              )
                            })}
                        </Table.Body>
                      </Table>
                              </section>
                      )
                    }
                })()}
            </section>
        );
    }
    async deleteItem(id){
        this.setState({
            loading: true
        })
       let response = await fetch('/api/news/' + id, {
           method: "DELETE"
       });
       let result = await response.json();
       if(result){
           this.setState({
               loading: false
           });
           this.props.history.push('/');
       }
       
    }
    async getNews(){
        let response = await fetch('/api/news');
        let result = await response.json();
        this.setState({
            newsArr: result
        });
    }
    async addNews(){
        this.setState({
            modalOpen: false,
            loading: true
        });
        let formData = new FormData();
        formData.append("title", this.state.title);
        formData.append("body", this.state.description);
        formData.append("media", this.state.media);
        let response = await fetch('/api/news/add-news', {
            method: "POST",
            body: formData
        });

        let result = response.json();
        
        this.setState({
           
            loading: false
        }, ()=>{
            this.props.history.replace('/');
        });
        
        
    }
    componentDidMount(){
        this.getNews();
    }
}

export default withRouter(NewsPage);