import React, {Component} from 'react';
import {Icon, Table, Button, Dimmer, Loader, Header, Modal, Image, Form} from 'semantic-ui-react';
import {format} from 'date-fns';
import {withRouter} from 'react-router-dom';

class SpotlightPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            spotlightArr: [],
            modalOpen: false
        }
        this.addSpotlight = this.addSpotlight.bind(this);
    }
    render(){
        return (
            <section>
                {(()=>{
                    if(this.state.spotlightArr.length < 0){
                        return(
                            <Dimmer active>
                                <Loader>Loading</Loader>
                            </Dimmer>
                        );
                    }else{
                        return(
                            <section>
                            <Modal dimmer={false} open={this.state.modalOpen} size='small'>
                                <Header icon='write' content='Add New Spotlight' />
                                <Modal.Content>
                                    <Form>
                                        <Form.Input
                                        required
                                            placeholder="Title"
                                            ref="title"
                                            onChange={(e)=>{this.setState({title: e.target.value})}}
                                        />
                                        <Form.Input
                                        required
                                            placeholder="Description"
                                            onChange={(e)=>{this.setState({description: e.target.value})}}
                                        />
                                        <Form.Input
                                        required
                                        type="file"
                                        onChange={(e)=>{this.setState({media: e.target.files[0]})}}
                                        />
                                    </Form>
                                </Modal.Content>
                                <Modal.Actions>
                                <Button icon onClick={this.addSpotlight} color='green' inverted>
                                    <Icon name='checkmark' /> Add
                                </Button>
                                <Button icon onClick={()=>{this.setState({modalOpen: false})}} color='red' inverted>
                                    <Icon name='remove' /> Close
                                </Button>
                                </Modal.Actions>
                            </Modal>
                        <Button color="black" onClick={()=>{this.setState({modalOpen: true})}} icon><Icon name="plus"  />New Spotlight</Button>
                        <Table basic="very" celled>
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell>Title</Table.HeaderCell>
                            <Table.HeaderCell>Date Added</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                    
                        <Table.Body>
                            {this.state.spotlightArr.map((item, i)=>{
                            return(
                                        
                            <Table.Row key={i}>
                                <Table.Cell>
                                <Header as='h4' image>
                                    <Image src={item.media} rounded size='mini' />
                                    <Header.Content>
                                    {item.title}
                                    </Header.Content>
                                </Header>
                                </Table.Cell>
                                <Table.Cell>{format(item.createdAt, "Do MMMM YYYY")}</Table.Cell>
                                <Table.Cell>
                                <Button basic circular color="red" icon><Icon name="trash outline" color="red" /></Button>
                                <Button basic circular icon><Icon name="edit" /></Button>
                                    </Table.Cell>
                              </Table.Row>
                              )
                            })}
                        </Table.Body>
                      </Table>
                              </section>
                      )
                    }
                })()}
            </section>
        );
    }
    async getSpotlight(){
        let response = await fetch('/api/spotlight');
        let result = await response.json();
        this.setState({
            spotlightArr: result
        });
    }
    async addSpotlight(){
        let formData = new FormData();
        formData.append("title", this.state.title);
        formData.append("description", this.state.description);
        formData.append("media", this.state.media);

        let response = await fetch('/api/spotlight', {
            method: "POST",
            body: formData,
            credentials: "include"
        });

        let result = response.json();
        this.setState({
            modalOpen: false
        }, ()=>{
            this.props.history.push('/configuration');
        });
        
        
    }
    componentDidMount(){
        this.getSpotlight();
    }
}

export default withRouter(SpotlightPage);