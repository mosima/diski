import React, {Component} from 'react';
import {Icon, Table, Button, Dimmer, Loader, Header, Modal, Form} from 'semantic-ui-react';
import {format} from 'date-fns';
import {withRouter} from 'react-router-dom';

class ShowcasePage extends Component{
    constructor(props){
        super(props);
        this.state = {
            showcaseArr: []
        }
    }
    render(){
        return (
            <section>
                {(()=>{
                    if(this.state.showcaseArr.length < 0){
                        return(
                            <Dimmer active>
                                <Loader>Loading</Loader>
                            </Dimmer>
                        );
                    }else{
                        return(
                            <section>
                            <Modal dimmer={false} open={this.state.modalOpen} size='small'>
                            <Header icon='write' content='Add New Showcase' />
                            <Modal.Content>
                                <Form>
                                    <Form.Input
                                        placeholder="Title"
                                        required
                                        onChange={(e)=>{this.setState({title: e.target.value})}}
                                    />
                                    <Form.Input
                                    required
                                        placeholder="Description"
                                        onChange={(e)=>{this.setState({description: e.target.value})}}
                                    />
                                    <Form.Input
                                    required
                                    type="file"
                                    onChange={(e)=>{this.setState({media: e.target.files[0]})}}
                                    />
                                </Form>
                            </Modal.Content>
                            <Modal.Actions>
                            <Button icon onClick={this.addSpotlight} color='green' inverted>
                                <Icon name='checkmark' /> Add
                            </Button>
                            <Button icon onClick={()=>{this.setState({modalOpen: false})}} color='red' inverted>
                                <Icon name='remove' /> Close
                            </Button>
                            </Modal.Actions>
                        </Modal>
                        <Button color="black" onClick={()=>{this.setState({modalOpen: true})}} icon><Icon name="plus"  />New Showcase</Button>
                            
                        <Table singleLine>
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell>Title</Table.HeaderCell>
                            <Table.HeaderCell>Date Added</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                    
                        <Table.Body>
                            {this.state.showcaseArr.map((item, i)=>{
                            return(
                                        
                            <Table.Row key={i}>
                                <Table.Cell>{item.title}</Table.Cell>
                                <Table.Cell>{format(item.createdAt, "Do MMMM YYYY")}</Table.Cell>
                                <Table.Cell>
                                    <Button icon><Icon name="trash outline" color="red" /></Button>
                                    <Button icon><Icon name="edit" color="violet" /></Button>
                                    </Table.Cell>
                              </Table.Row>
                              )
                            })}
                        </Table.Body>
                      </Table>
                              </section>
                      )
                    }
                })()}
            </section>
        );
    }
    async getShowcase(){
        let response = await fetch('/api/showcase');
        let result = await response.json();
        this.setState({
            showcaseArr: result
        });
    }

    async addShowcase(){
        let formData = new FormData();
        formData.append("title", this.state.title);
        formData.append("description", this.state.description);
        formData.append("media", this.state.media);

        let response = await fetch('/api/showcase', {
            method: "POST",
            body: formData,
            credentials: "include"
        });

        let result = response.json();
        this.setState({
            modalOpen: false
        }, ()=>{
            this.props.history.push('/configuration');
        });
        
        
    }

    componentDidMount(){
        this.getShowcase();
    }
}

export default withRouter(ShowcasePage);