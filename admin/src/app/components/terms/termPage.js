import React, {Component} from 'react';
import {Icon, Table, Button, Dimmer, Loader, Header, Modal, Form} from 'semantic-ui-react';
import {format} from 'date-fns';
import {withRouter} from 'react-router-dom';

const base = "http://localhost:88";

class TermPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            termArr: [],
            modalOpen: false
        }
    }
    render(){
        return (
            <section>
                {(()=>{
                    if(this.state.termArr.length <= 0){
                        return(
                            <Dimmer active>
                                <Loader>Loading</Loader>
                            </Dimmer>
                        );
                    }else{
                        return(
                            <section>
                            <Modal dimmer={false} open={this.state.modalOpen} size='small'>
                                <Header icon='write' content='Add New Term' />
                                <Modal.Content>
                                    <Form>
                                        <Form.Input
                                        placeholder="Term"
                                        type="number"
                                        min="1"
                                        max="4"
                                        label="Term"
                                        onChange={(e)=>{this.setState({term: e.target.value})}}
                                        />
                                        <Form.Input
                                        placeholder="Start Date"
                                        label="Start Date"
                                        type="date"
                                        onChange={(e)=>{this.setState({startDate: e.target.value})}}
                                        />
                                        <Form.Input
                                        placeholder="End Date"
                                        label="End Date"
                                        type="date"
                                        onChange={(e)=>{this.setState({endDate: e.target.value})}}
                                        />
                                        <Form.Input
                                        placeholder="Year"
                                        label="Year"
                                        type="date"
                                        onChange={(e)=>{this.setState({year: e.target.value})}}
                                        />
                                    </Form>
                                </Modal.Content>
                                <Modal.Actions>
                                <Button onClick={()=>{this.setState({modalOpen: false})}} icon basic color='red' inverted>
                                    <Icon name='remove' /> Clear
                                </Button>
                                <Button onClick={this.addTerm.bind(this)} icon color='green' inverted>
                                    <Icon name='checkmark' /> Add
                                </Button>
                                </Modal.Actions>
                            </Modal>
                            <Button onClick={()=>{this.setState({modalOpen: true})}} color="black" icon><Icon name="plus" /> Add New Term</Button>
                        <Table singleLine celled>
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell>Term</Table.HeaderCell>
                            <Table.HeaderCell>Start Date</Table.HeaderCell>
                            <Table.HeaderCell>End Date</Table.HeaderCell>
                            <Table.HeaderCell>Year</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                    
                        <Table.Body>
                            {this.state.termArr.map((item, i)=>{
                            return(
                                        
                            <Table.Row key={i}>
                                <Table.Cell>{"Term " + item.number}</Table.Cell>
                                <Table.Cell>{format(item.start_date, "Do MMMM YYYY")}</Table.Cell>
                                <Table.Cell>{format(item.end_date, "Do MMMM YYYY")}</Table.Cell>
                                <Table.Cell>{item.year}</Table.Cell>
                                <Table.Cell>
                                    <Button basic circular color="red" icon><Icon name="trash outline" color="red" /></Button>
                                    <Button basic circular icon><Icon name="edit" /></Button>
                                    </Table.Cell>
                              </Table.Row>
                              )
                            })}
                        </Table.Body>
                      </Table>
                              </section>
                      )
                    }
                })()}
            </section>
        );
    }
    async getTerms(){
        let response = await fetch('/api/term');
        let result = await response.json();
        this.setState({
            termArr: result
        });
    }
    async addTerm(){
        let obj = {
            number: this.state.term,
            start_date: this.state.startDate,
            end_date: this.state.endDate,
            year: this.state.year
        }

        let response = await fetch('/api/term', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(obj)
        });
        let result = await response.json();
        this.setState({
            modalOpen: false
        }, ()=>{this.props.history.push('/configuration')})
    }
    componentDidMount(){
        this.getTerms();
    }
}

export default withRouter(TermPage);