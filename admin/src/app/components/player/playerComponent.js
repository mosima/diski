import React,{Component} from 'react';
import {Icon, Table, Button, Dimmer, Loader, Header, Modal, Image, Form} from 'semantic-ui-react';
import {format} from 'date-fns';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

const base = 'http://localhost:88';

class PlayerComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            playerArr: [],
            modalOpen: false
        }
    }
    render(){
        return (
            <section>
            {(()=>{
                if(this.state.playerArr.length < 0){
                    return(
                        <Dimmer active>
                            <Loader>Loading</Loader>
                        </Dimmer>
                    );
                }else{
                    return(
                        <section>
                        <Modal open={this.state.modalOpen} basic size='small'>
                            <Header icon='write' content='Add New Player' />
                            <Modal.Content>
                                <Form>
                                    <Form.Input
                                    required
                                    placeholder="First Name"
                                    onChange={(e)=>{this.setState({firstname: e.target.value})}}
                                    />
                                    <Form.Input
                                    required
                                    placeholder="Middle Name"
                                    onChange={(e)=>{this.setState({middlename: e.target.value})}}
                                    />
                                    <Form.Input
                                    required
                                    placeholder="Last Name"
                                    onChange={(e)=>{this.setState({lastname: e.target.value})}}
                                    />
                                    <Form.Input
                                    required
                                    type="date"
                                    placeholder="Date of Birth"
                                    onChange={(e)=>{this.setState({dob: e.target.value})}}
                                    />
                                </Form>
                            </Modal.Content>
                            <Modal.Actions>
                            <Button basic icon onClick={()=>{this.setState({modalOpen: false})}} color='red' inverted>
                                <Icon name='remove' /> Cancel
                            </Button>
                            <Button onClick={this.addPlayer.bind(this)} icon color='green' inverted>
                                <Icon name='checkmark' /> Add
                            </Button>
                            </Modal.Actions>
                        </Modal>
                        <Button color="black" onClick={()=>{this.setState({modalOpen: true})}} icon><Icon name="add user" /></Button>
                    <Table basic='very' celled>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Player</Table.HeaderCell>
                        <Table.HeaderCell>Date of Birth</Table.HeaderCell>
                        <Table.HeaderCell>Goals</Table.HeaderCell>
                        <Table.HeaderCell>Games</Table.HeaderCell>
                        <Table.HeaderCell>Yellow</Table.HeaderCell>
                        <Table.HeaderCell>Red</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                
                    <Table.Body>
                        {this.state.playerArr.map((item, i)=>{
                        return(
                                    
                        <Table.Row key={i}>
                            <Table.Cell>
                            <Header as='h4' image>
                                <Image src={item.picture} rounded size='mini' />
                                <Header.Content>
                                {item.firstname}
                                <Header.Subheader>{item.lastname}</Header.Subheader>
                                </Header.Content>
                            </Header>
                            </Table.Cell>
                            <Table.Cell>{format(item.dateOfBirth, "Do MMMM YYYY")}</Table.Cell>
                            <Table.Cell>{item.career_goal_scored}</Table.Cell>
                            <Table.Cell>{item.career_games_played}</Table.Cell>
                            <Table.Cell>{item.career_yellow_card}</Table.Cell>
                            <Table.Cell>{item.career_red_card}</Table.Cell>
                            <Table.Cell>
                                <Button basic circular color="red" icon><Icon name="trash outline" color="red" /></Button>
                                <Button basic circular icon><Icon name="edit" color="violet" /></Button>
                                </Table.Cell>
                          </Table.Row>
                          )
                        })}
                    </Table.Body>
                  </Table>
                          </section>
                  )
                }
            })()}
        </section>
        )
    }
    async getPlayers(){
        let response = await fetch('/api/player/school/' + this.props.user.school._id );
        let result = await response.json();
        this.setState({
            playerArr: result
        });
    }
    async addPlayer(){
        let obj = {
            firstname: this.state.firstname,
            middlename: this.state.middlename,
            lastname: this.state.lastname,
            dateOfBirth: this.state.dob,
            teacher: this.props.user._id,
            school: this.props.user.school._id
        }
        let response = await fetch('/api/player', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(obj)
        });
        let result = await response.json();
        this.setState({
            modalOpen: false
        }, ()=>{this.props.history.push('/school')})
    }
    componentDidMount(){
        this.getPlayers();
    }
}

function matchStateToProps(state){
    return{
        user: state.auth
    }
}

export default withRouter(connect(matchStateToProps)(PlayerComponent));