import React, {Component} from 'react';
import {Table, Header, Icon, Button, Confirm} from 'semantic-ui-react';
import {format} from 'date-fns';

class HolidayRowComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            open: false
        }
        this.show = this.show.bind(this);
        this.handleCancel  = this.handleCancel.bind(this);
    }
    show(){
        this.setState({
            open: true
        })
    }
    render(){
        const {open} = this.state;
        return(
            <Table.Row>
                <Table.Cell>{this.props.item.name}</Table.Cell>
                <Table.Cell>{format(this.props.item.date, "Do MMMM YYYY")}</Table.Cell>
                <Table.Cell>
                <Button basic circular color="red" onClick={this.show} icon><Icon name="trash outline" color="red" /></Button>
                <Button basic circular icon><Icon name="edit" /></Button>
                    </Table.Cell>
                    <Confirm
                    content="Confirm delete?"
          open={open}
          header
          size="mini"
          onCancel={this.handleCancel}
          onConfirm={this.deleteHoliday.bind(this)}
        />
            </Table.Row>
        )
    }
    handleCancel(){
        this.setState({
            open: false
        })
    }
    deleteHoliday(){
        this.props.deleteHoliday(this.props.item._id);
        this.setState({
            open: false
        })
    }
}

export default HolidayRowComponent;