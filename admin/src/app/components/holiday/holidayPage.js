import React, {Component} from 'react';
import {Icon, Table, Button, Dimmer, Loader, Header, Modal, Form} from 'semantic-ui-react';
import {format} from 'date-fns';
import {withRouter} from 'react-router-dom';
import HolidayRowComponent from './holidayRow';
import {ToastContainer, toast} from 'react-toastify';

const base = "http://localhost:88";

class HolidayPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            holidayArr: [],
            modalOpen: false,
            loading: false
        }
        this.deleteHoliday = this.deleteHoliday.bind(this);
        this.successNotify = this.successNotify.bind(this);
        this.failureNotify = this.failureNotify.bind(this);
    }
    successNotify(msg){
        toast.success(msg);
    }
    failureNotify(msg){
        toast.success(msg);
    }
    render(){
        return (
            <section>
                <ToastContainer />
                {(()=>{
                    if(this.state.holidayArr.length <= 0){
                        return(
                            <Dimmer active>
                                <Loader>Loading</Loader>
                            </Dimmer>
                        );
                    }else{
                        return(
                            <section>
                                 <Dimmer active={this.state.loading}>
                                <Loader>Loading</Loader>
                            </Dimmer>
                            <Modal dimmer={false} open={this.state.modalOpen} size='small'>
                                <Header icon='write' content='Add New Holiday' />
                                <Modal.Content>
                                    <Form>
                                        <Form.Field>
                                        <label>Name</label>
                                        <input placeholder='Name' onChange={(e)=>{this.setState({name: e.target.value})}} />
                                        </Form.Field>
                                        <Form.Field>
                                        <label>Date</label>
                                        <input placeholder='Date' onChange={(e)=>{this.setState({date: e.target.value})}} type="date" />
                                        </Form.Field>
                                    </Form>
                                </Modal.Content>
                                <Modal.Actions>
                                <Button onClick={()=>{this.setState({modalOpen: false})}} icon basic color='red' inverted>
                                    <Icon name='remove' /> Clear
                                </Button>
                                <Button onClick={this.addHoliday.bind(this)} icon color='green' inverted>
                                    <Icon name='checkmark' /> Add
                                </Button>
                                </Modal.Actions>
                            </Modal>
                        <Button onClick={()=>{this.setState({modalOpen: true})}} color="black" icon><Icon name="plus" /> Add New Holiday</Button>
                        <Table celled singleLine>
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell>Name</Table.HeaderCell>
                            <Table.HeaderCell>Date</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                    
                        <Table.Body>
                            {this.state.holidayArr.map((item, i)=>{
                            return(
                                        
                            <HolidayRowComponent deleteHoliday={this.deleteHoliday} item={item} key={i} />
                              )
                            })}
                        </Table.Body>
                      </Table>
                              </section>
                      )
                    }
                })()}
            </section>
        );
    }
    async deleteHoliday(id){
        this.setState({
            loading: true
        })
        let response = await fetch('/api/holiday/' + id, {
            method: "DELETE"
        });
        let result = await response.json();
        if(result){
            this.setState({
                loading: false
            });
            this.successNotify(result.response);
            this.props.history.push('/')       ;
        }
    }
    async getHoliday(){
        let response = await fetch('/api/holiday');
        let result = await response.json();
        this.setState({
            holidayArr: result
        });
    }

    async addHoliday(){
        this.setState({
            modalOpen: false,
            loading: true
        });
        let obj = {
            name: this.state.name,
            date: this.state.date
        }
        let response = await fetch('/api/holiday', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(obj)
        });
        let result = await response.json();
        if(result){
            this.setState({
                loading: false
            });
            this.props.history.push('/');
        }
        
    }
    componentDidMount(){
        this.getHoliday();
    }
}

export default withRouter(HolidayPage);