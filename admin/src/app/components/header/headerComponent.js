import React, {Component} from 'react';
import { Input, Menu, Button, Icon } from 'semantic-ui-react'
const logo = require('../../assets/img/logo.jpg');
class HeaderComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            activeItem: 'home'
        }
        this.toggleVisibility = this.toggleVisibility.bind(this);
        this.doLogout = this.doLogout.bind(this);
    }
    handleItemClick = (e, { name }) => this.setState({ activeItem: name });
    render(){
        return (
            <Menu secondary>
            <Menu.Item><Button basic icon secondary size="large" onClick={this.toggleVisibility}><Icon size="large" name='sidebar' /></Button></Menu.Item>
            <Menu.Item><img src={logo} style={{height: 80, width: 'auto'}} /></Menu.Item>
            <Menu.Menu position='right'>
              <Menu.Item>
                <Button as="a" href="/user/logout" basic icon secondary size="large"><Icon size="large" name='log out' /></Button>
              </Menu.Item>
            </Menu.Menu>
          </Menu>
        )
    }
    async doLogout(){
       let res = await fetch('/user/logout');
       window.location.href = "/";
    }
    toggleVisibility(){
        this.props.toggleVisibility();
    }
}

export default HeaderComponent;