import React, {Component} from 'react';
import {Table, Button, Icon} from 'semantic-ui-react';
import {format} from 'date-fns';

class FixtureRowComponent extends Component{
    render(){
        return(
            <Table.Row>
                <Table.Cell>{this.props.item.home_team.Institution_Name}</Table.Cell>
                <Table.Cell>{this.props.item.away_team.Institution_Name}</Table.Cell>
                <Table.Cell>{format(this.props.item.match_date, "Do MMMM YYYY")}</Table.Cell>
                <Table.Cell>{this.props.item.home_team.StreetAddress}</Table.Cell>
                <Table.Cell></Table.Cell>
                <Table.Cell><Button icon color="red" basic><Icon name="cancel" /> RESCHEDULE</Button></Table.Cell>
            </Table.Row>
        )
    }
}

export default FixtureRowComponent;