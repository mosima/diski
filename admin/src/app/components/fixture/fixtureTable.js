import React, {Component} from 'react';
import {Icon, Table, Button, Dimmer, Loader, Header, Modal, Form} from 'semantic-ui-react';
import {connect} from 'react-redux';
import FixtureRowComponent from './fixtureRow';

class FixtureTableComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            fixtureArr: []
        }
    }
    render(){
            return (
                <section>
                {(()=>{
                    if(this.state.fixtureArr.length <= 0){
                        return(
                            <Dimmer active>
                                <Loader>Loading</Loader>
                            </Dimmer>
                        );
                    }else{
                        return(
                            <section>
                        
                        <Table singleLine>
                        <Table.Header>
                          <Table.Row>
                            <Table.HeaderCell>Home Team</Table.HeaderCell>
                            <Table.HeaderCell>Away Team</Table.HeaderCell>
                            <Table.HeaderCell>Match Date</Table.HeaderCell>
                            <Table.HeaderCell>Address</Table.HeaderCell>
                            <Table.HeaderCell>Official</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                    
                        <Table.Body>
                            {this.state.fixtureArr.map((item, i)=>{
                            return(
                                <FixtureRowComponent item={item} key={i} />
                              )
                            })}
                        </Table.Body>
                      </Table>
                              </section>
                      )
                    }
                })()}
            </section>
            )
    }
    async getFixtures(){
        let res = await fetch('/api/fixture/school/' + this.props.user.school._id);
        let ris = await res.json();
        this.setState({
            fixtureArr: ris
        });
    }
    componentDidMount(){
        this.getFixtures();
    }
}

function matchStateToProps(state){
    return {
        user: state.auth
    }
}

export default connect(matchStateToProps)(FixtureTableComponent);