import React,{Component} from 'react';
import {Card, Icon, Dimmer, Loader} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {format} from 'date-fns';

class FixtureCardComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            fixtureArr: []
        }
    }
    render(){
        return(
            <section>
                {(()=>{
                    if(this.state.fixtureArr.length < 0){
                        return(
                            <Dimmer active>
                                <Loader>Loading</Loader>
                            </Dimmer>
                        );
                    }else{
                        return(
                        this.state.fixtureArr.map(item=>{
                            return(
                                <Card color="grey" fluid key={item._id}>
                                <Card.Content textAlign="center" header={item.home_team.Institution_Name} />
                                <Card.Content textAlign="center">
                                    <Icon name="soccer" size="large" />
                                </Card.Content>
                                <Card.Content textAlign="center" header={item.away_team.Institution_Name} />
                                <Card.Content textAlign="center" extra>
                                  <Icon name='calendar' />
                                  {format(item.match_date, "Do MMMM YYYY")}
                                </Card.Content>
                              </Card>
                            )
                        })
                    )
                        }
                })()}
            </section>
            
            
        )
    }

    async getFixtures(){
        let res = await fetch('/api/fixture/school/three/' + this.props.user.school._id);
        let ris = await res.json();
        this.setState({
            fixtureArr: ris
        });
    }

    componentDidMount(){
        this.getFixtures();
    }
}

function matchStateToProps(state){
    return {
        user: state.auth
    }
}

export default connect(matchStateToProps)(FixtureCardComponent);