import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Icon, Dimmer, Loader} from 'semantic-ui-react';
import { withStyles } from 'material-ui/styles';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select';
import TextField from 'material-ui/TextField';
import {withRouter} from 'react-router-dom';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 250,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
});

class FixturePage extends Component{
    constructor(props){
        super(props);
        this.state = {
            province: '',
            phase: '',
            term: '',
            termArr: [],
            district: '',
            districtArr: [],
            isActive: true,
            dim: false
        }
        this.generateFixtures = this.generateFixtures.bind(this);
    }

    handleChange = name => event => {
        
      this.setState({ [name]: event.target.value },()=>{
          if(name == 'province'){
              
              this.setState({
                  isActive: false
              })
          this.getProvinceDistrict(this.state.province);
          }
      });
      
    };

    async getProvinceDistrict(province){
        let res = await fetch('/api/district/' + province);
        let district = await res.json();
        this.setState({
            districtArr: district
        });
    }

    render(){
        const { classes } = this.props;
        return(
            <section>
                <Dimmer active={this.state.dim}>
                    <Loader>Processing</Loader>
                </Dimmer>
                <h3>Generate Fixtures:</h3>
                <form className={classes.container} autoComplete="off">
                <FormControl className={classes.formControl}>
                <InputLabel htmlFor="province">Province</InputLabel>
                <Select
                    value={this.state.province}
                    onChange={this.handleChange('province')}
                    input={<Input id="province" />}
                >
                    <MenuItem value="">
                    <em>None</em>
                    </MenuItem>
                    {this.props.province.map((item, i)=>{
                        return(
                            <MenuItem key={i} value={item.short_name}>{(item.name).toUpperCase()}</MenuItem>
                        )
                    })}
                </Select>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="phase">Phase</InputLabel>
                    <Select
                        value={this.state.phase}
                        onChange={this.handleChange('phase')}
                        input={<Input id="phase" />}
                    >
                        <MenuItem value="">
                        <em>None</em>
                        </MenuItem>
                        {this.props.phases.map((item, i)=>{
                            return(
                                <MenuItem key={i} value={item.phase}>{(item.phase).toUpperCase()}</MenuItem>
                            )
                        })}
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="term">School Term</InputLabel>
                    <Select
                        value={this.state.term}
                        onChange={this.handleChange('term')}
                        input={<Input id="term" />}
                    >
                        <MenuItem value="">
                        <em>None</em>
                        </MenuItem>
                        {this.state.termArr.map((item, i)=>{
                            return(
                                <MenuItem key={i} value={item._id}>{"Term " + item.number + " " + item.year}</MenuItem>
                            )
                        })}
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="district">District</InputLabel>
                    <Select
                        value={this.state.district}
                        onChange={this.handleChange('district')}
                        input={<Input id="district" disabled={this.state.isActive} />}
                        disabled={this.state.isActive}
                    >
                        <MenuItem value="">
                        <em>None</em>
                        </MenuItem>
                        {this.state.districtArr.map((item, i)=>{
                            return(
                                <MenuItem key={i} value={item.district}>{(item.district).toUpperCase()}</MenuItem>
                            )
                        })}
                    </Select>
                </FormControl>
                <FormControl>
                <Button basic icon onClick={this.generateFixtures} className="btn-generate" size="medium"><Icon name='settings' /> Generate Fixtures</Button>
                </FormControl>
            </form>
            </section>
        );
    }
    async generateFixtures(e){
        e.preventDefault();
        this.setState({
            dim: true
        })
        let response = await fetch('/api/fixture/generate-fixture/' + this.state.province + '/' + this.state.district + '/' + this.state.phase + '/' + this.state.term,{
            method: "GET",
            credentials: "include"
        });
        let result = await response.json();
        if(result){
            this.setState({
                dim: false
            });
            this.props.history.push('/');
        }
    }
    async getTerms(){
        let response = await fetch('/api/term');
        let result = await response.json();
        this.setState({
            termArr: result
        });
    }

    componentDidMount(){
        this.getTerms();
    }
}
function matchStateToProps(state){
    return {
        province: state.province,
        phases: state.phases
    }
}

export default withRouter(connect(matchStateToProps)(withStyles(styles)(FixturePage)));