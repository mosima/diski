import React, {Component} from 'react';
import { Grid, Segment, Header, Icon, List, Image, Button } from 'semantic-ui-react';
import {connect} from 'react-redux';
import {format} from 'date-fns';
import {Link} from 'react-router-dom';

class Profile extends Component{
    render(){
        const items = {

        }
        return (
            <section>
            <div style={{padding: 10}}>
               <Link to="/edit-profile"><Button fluid basic icon color="red"><Icon name="edit" /> Edit Profile</Button></Link>
            </div>
            <Grid columns='equal'>
            
            <Grid.Column>
              <Segment>
                <Header as='h2'>
                    <Icon name='user' />
                    <Header.Content>
                    User Information
                    <Header.Subheader>
                        View &amp; Your Personal Information 
                    </Header.Subheader>
                    </Header.Content>
                </Header>
                <Image src={this.props.user.picture} style={{height: 250, width: 'auto'}} />
                <List>
                    <List.Item>
                    <List.Header>Name</List.Header>
                    {this.props.user.displayName}
                    </List.Item>
                    <List.Item>
                    <List.Header>Email</List.Header>
                    {this.props.user.email}
                    </List.Item>
                    <List.Item>
                    <List.Header>Gender</List.Header>
                    {this.props.user.gender}
                    </List.Item>
                    <List.Item>
                    <List.Header>Role</List.Header>
                        {(()=>{
                            if(this.props.user.isAdmin){
                                return "Teacher"
                            }else{
                                return "Referee"
                            }
                        })()}
                    </List.Item>
                </List>
              </Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>
              <Header as='h2'>
                    <Icon name='map outline' />
                    <Header.Content>
                        Location
                    <Header.Subheader>
                        Update Your Location
                    </Header.Subheader>
                    </Header.Content>
                </Header>
                <List>
                    <List.Item>
                    <List.Header>Address</List.Header>
                    {this.props.user.address}
                    </List.Item>
                    <List.Item>
                    <List.Header>Suburb</List.Header>
                    {this.props.user.suburb}
                    </List.Item>
                    <List.Item>
                    <List.Header>City</List.Header>
                    {this.props.user.city}
                    </List.Item>
                    <List.Item>
                    <List.Header>Province</List.Header>
                        {this.props.user.province}
                    </List.Item>
                </List>
              </Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>
                  
                <Header as='h2'>
                    <Icon name='fork' />
                    <Header.Content>
                    Diski99
                    <Header.Subheader>
                        Your Related Records on Diski99
                    </Header.Subheader>
                    </Header.Content>
                </Header>
                <List>
                    <List.Item>
                    <List.Header>School</List.Header>
                    {this.props.user.school.Institution_Name}
                    </List.Item>
                    <List.Item>
                    <List.Header>Phase</List.Header>
                    {this.props.user.school.Phase}
                    </List.Item>
                    <List.Item>
                    <List.Header>Email</List.Header>
                    {this.props.user.school.Email}
                    </List.Item>
                    <List.Item>
                    <List.Header>Date Established</List.Header>
                        {format(this.props.user.school.RegistrationDate, "Do MMMM YYYY")}
                    </List.Item>
                </List>
              </Segment>
            </Grid.Column>
          </Grid>
          </section>
        )
    }
}

function matchStateToProp(state){
    return {
        user: state.auth
    }
}

export default connect(matchStateToProp)(Profile);