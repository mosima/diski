import React, {Component} from 'react';
import { Button, Form, Grid, Header, Image, Message, Segment, Icon } from 'semantic-ui-react';
const logo = require('../assets/img/logo.jpg');

class LoginForm extends Component{
    constructor(props){
        super(props);

        this.doLogin = this.doLogin.bind(this);
    }
    render(){
        return(
            <div className='login-form'>
            {/*
              Heads up! The styles below are necessary for the correct render of this example.
              You can do same with CSS, the main idea is that all the elements up to the `Grid`
              below must have a height of 100%.
            */}
            <style>{`
              body > div,
              body > div > div,
              body > div > div > div.login-form {
                height: 100%;
              }
            `}</style>
            <Grid
              textAlign='center'
              style={{ height: '100%' }}
              verticalAlign='middle'
            >
              <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='black' textAlign='center'>
                  <Image src={logo} style={{height: 80, width: 'auto'}} /><br />
                  Log-in to your account
                </Header>
                <Form size='large'>
                  <Segment stacked>
                    <Form.Input
                      fluid
                      icon='user'
                      iconPosition='left'
                      placeholder='E-mail address'
                    />
                    <Form.Input
                      fluid
                      icon='lock'
                      iconPosition='left'
                      placeholder='Password'
                      type='password'
                    />
                    
        
                    <Button color='black' fluid size='large'>Login</Button>
                    <br />
                    <Button color="google plus" onClick={this.doLogin} fluid size='large' icon><Icon name="google" />oogle</Button>
                    <br />
                    <Button color="facebook" fluid size='large' icon><Icon name="facebook" />acebook</Button>
                  </Segment>
                </Form>
              </Grid.Column>
            </Grid>
          </div>
        )
    }
    doLogin(){
        window.location.href = "/user/google/login";
    }
}
export default LoginForm
