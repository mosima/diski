import React, {Component} from 'react';
import {Header, Icon, Segment} from 'semantic-ui-react';
import {connect} from 'react-redux';

class Result extends Component{
    render(){
        return (
            <section>
                <Segment>
                <Header as='h2'>
                    <Icon name='university' />
                    <Header.Content>
                    {this.props.user.school.Institution_Name} RESULTS
                    <Header.Subheader>
                        RECENT RESULTS
                    </Header.Subheader>
                    </Header.Content>
                </Header>
                Nothing to display
            </Segment>
            </section>
        )
    }
}

function matchStateToProp(state){
    return {
        user: state.auth
    }
}
export default connect(matchStateToProp)(Result);