import React, {Component} from 'react';
import { Grid, Segment, Header, List, Statistic, Icon, Item, Button, Feed, Message } from 'semantic-ui-react';
import FixtureCardComponent from '../components/fixture/fixtureCard';
import {connect} from 'react-redux';
import format from 'date-fns/distance_in_words_to_now';
import {Link} from 'react-router-dom';

class Home extends Component{
    render(){
        const evt = this.props.news.map((item, i)=>{
            return {
                date: format(item.createdAt) + " ago",
                image: item.media,
                summary: item.title,
                extraText: (item.body).substring(0, 60)
            }
        });
        return (
            <section>
            <Grid columns='equal'>
            <Grid.Column>
              <Segment>
                <Header as='h2'>
                    <Icon color="red" name='rss' />
                    <Header.Content>
                    News Headlines
                    <Header.Subheader>
                        Diski99 News Feeds
                    </Header.Subheader>
                    </Header.Content>
                </Header>
                <Message>
                    <Message.List>
                        {this.props.news.map((item, i)=>{
                        return(
                            <Message.Item className="mews" key={i}><Link to="">{item.title}</Link></Message.Item>
                            )
                        })}
                    </Message.List>
                </Message>
              </Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>
              <Header as='h2'>
                    <Icon color="red" name='signal' />
                    <Header.Content>
                        Statistics
                    <Header.Subheader>
                        Your Statistics
                    </Header.Subheader>
                    </Header.Content>
                </Header>
                    <Statistic.Group>
                        <Statistic color="red">
                            <Statistic.Label>Games Played</Statistic.Label>
                            <Statistic.Value>{this.props.user.school.total_games_played}</Statistic.Value>
                        </Statistic>
                        <Statistic color="red">
                            <Statistic.Label>Games Won</Statistic.Label>
                            <Statistic.Value>{this.props.user.school.games_won}</Statistic.Value>
                        </Statistic>
                        </Statistic.Group>
                        <Statistic.Group>
                        <Statistic color="red">
                            <Statistic.Label>Games Drawn</Statistic.Label>
                            <Statistic.Value>{this.props.user.school.games_drawn}</Statistic.Value>
                        </Statistic>
                        <Statistic color="red">
                            <Statistic.Label>Games Lost</Statistic.Label>
                            <Statistic.Value>{this.props.user.school.games_lost}</Statistic.Value>
                        </Statistic>
                        </Statistic.Group>
                        <Statistic.Group>
                        <Statistic color="red">
                            <Statistic.Label>Goals Scored</Statistic.Label>
                            <Statistic.Value>{this.props.user.school.total_team_goal}</Statistic.Value>
                        </Statistic>
                        <Statistic text>
                            <Statistic.Label>Highest Goal Scorer</Statistic.Label>
                            <Statistic.Value></Statistic.Value>
                        </Statistic>
                    </Statistic.Group>
                    
              </Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>
                <Header as='h2'>
                    <Icon color="red" name='calendar' />
                    <Header.Content>
                    Upcoming Fixtures
                    <Header.Subheader>
                        Your Upcoming 2 Fixtures
                    </Header.Subheader>
                    </Header.Content>
                </Header>
                <FixtureCardComponent />
              </Segment>
            </Grid.Column>
          </Grid>
          </section>
        )
    }
}
function matchStateToProps(state){
    return{
        news: state.news,
        user: state.auth
    }
}

export default connect(matchStateToProps)(Home);