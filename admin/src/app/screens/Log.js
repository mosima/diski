import React, {Component} from 'react';
import {Header, Icon, Segment, Table, Button, Dimmer, Loader, Modal, Form} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {} from 'semantic-ui-react';

class Log extends Component{
    constructor(props){
        super(props);
        this.state = {
            schoolArr: [],
            districtArr: [],
            suburbArr: []
        }
    }
    render(){
        console.log(this.state)
        return (
            <section>
            <Segment>
            <Header as='h2'>
                <Icon name='university' />
                <Header.Content>
                WARD {this.props.user.school.Ward_ID} STANDING
                <Header.Subheader>
                    WARD LOG
                </Header.Subheader>
                </Header.Content>
            </Header>
            {(()=>{
                    if(this.state.schoolArr.length <= 0){
                        return(
                            <Dimmer active>
                                <Loader>Loading</Loader>
                            </Dimmer>
                        );
                    }else{
                        return(
                        <Table singleLine>
                        <Table.Header>
                          <Table.Row>
                          <Table.HeaderCell>Pos</Table.HeaderCell>
                            <Table.HeaderCell>School</Table.HeaderCell>
                            <Table.HeaderCell>P</Table.HeaderCell>
                            <Table.HeaderCell>W</Table.HeaderCell>
                            <Table.HeaderCell>L</Table.HeaderCell>
                            <Table.HeaderCell>D</Table.HeaderCell>
                            <Table.HeaderCell>GF</Table.HeaderCell>
                            <Table.HeaderCell>GA</Table.HeaderCell>
                            <Table.HeaderCell>GD</Table.HeaderCell>
                            <Table.HeaderCell>Pts</Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                    
                        <Table.Body>
                            {this.state.schoolArr.map((item, i)=>{
                            return(
                                        
                            <Table.Row key={i} className={(item.Institution_Name === this.props.user.school.Institution_Name) ? 'self': ''}>
                                <Table.Cell>{i + 1}</Table.Cell>
                                <Table.Cell>{item.Institution_Name}</Table.Cell>
                                <Table.Cell>{item.season_games_played}</Table.Cell>
                                <Table.Cell>{item.games_won}</Table.Cell>
                                <Table.Cell>{item.games_lost}</Table.Cell>
                                <Table.Cell>{item.games_drawn}</Table.Cell>
                                <Table.Cell>{item.season_goal_for}</Table.Cell>
                                <Table.Cell>{item.season_goal_against}</Table.Cell>
                                <Table.Cell>{item.season_goal_for - item.season_goal_against}</Table.Cell>
                                <Table.Cell>{item.point}</Table.Cell>
                              </Table.Row>
                              )
                            })
                            }
                            </Table.Body>
                            </Table>
                            )
                        }
                    })()}
        </Segment>
        <Segment>
            <Header as='h2'>
                <Icon name='university' />
                <Header.Content>
                SUBURB {this.props.user.school.Suburb} STANDING
                <Header.Subheader>
                    SUBURB LOG
                </Header.Subheader>
                </Header.Content>
            </Header>
            {(()=>{
                    if(this.state.suburbArr.length <= 0){
                        return(
                            <Dimmer active>
                                <Loader>Loading</Loader>
                            </Dimmer>
                        );
                    }else{
                        return(
                        <Table singleLine>
                        <Table.Header>
                          <Table.Row>
                          <Table.HeaderCell>Pos</Table.HeaderCell>
                            <Table.HeaderCell>School</Table.HeaderCell>
                            <Table.HeaderCell>P</Table.HeaderCell>
                            <Table.HeaderCell>W</Table.HeaderCell>
                            <Table.HeaderCell>L</Table.HeaderCell>
                            <Table.HeaderCell>D</Table.HeaderCell>
                            <Table.HeaderCell>GF</Table.HeaderCell>
                            <Table.HeaderCell>GA</Table.HeaderCell>
                            <Table.HeaderCell>GD</Table.HeaderCell>
                            <Table.HeaderCell>Pts</Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                    
                        <Table.Body>
                            {this.state.suburbArr.map((item, i)=>{
                            return(
                                        
                            <Table.Row key={i} className={(item.Institution_Name === this.props.user.school.Institution_Name) ? 'self': ''}>
                                <Table.Cell>{i + 1}</Table.Cell>
                                <Table.Cell>{item.Institution_Name}</Table.Cell>
                                <Table.Cell>{item.season_games_played}</Table.Cell>
                                <Table.Cell>{item.games_won}</Table.Cell>
                                <Table.Cell>{item.games_lost}</Table.Cell>
                                <Table.Cell>{item.games_drawn}</Table.Cell>
                                <Table.Cell>{item.season_goal_for}</Table.Cell>
                                <Table.Cell>{item.season_goal_against}</Table.Cell>
                                <Table.Cell>{item.season_goal_for - item.season_goal_against}</Table.Cell>
                                <Table.Cell>{item.point}</Table.Cell>
                              </Table.Row>
                              )
                            })
                            }
                            </Table.Body>
                            </Table>
                            )
                        }
                    })()}
        </Segment>
        <Segment>
            <Header as='h2'>
                <Icon name='university' />
                <Header.Content>
                DISTRICT {this.props.user.school.EIDistrict} STANDING
                <Header.Subheader>
                    DISTRICT LOG
                </Header.Subheader>
                </Header.Content>
            </Header>
            {(()=>{
                    if(this.state.districtArr.length <= 0){
                        return(
                            <Dimmer active>
                                <Loader>Loading</Loader>
                            </Dimmer>
                        );
                    }else{
                        return(
                        <Table singleLine>
                        <Table.Header>
                          <Table.Row>
                          <Table.HeaderCell>Pos</Table.HeaderCell>
                            <Table.HeaderCell>School</Table.HeaderCell>
                            <Table.HeaderCell>P</Table.HeaderCell>
                            <Table.HeaderCell>W</Table.HeaderCell>
                            <Table.HeaderCell>L</Table.HeaderCell>
                            <Table.HeaderCell>D</Table.HeaderCell>
                            <Table.HeaderCell>GF</Table.HeaderCell>
                            <Table.HeaderCell>GA</Table.HeaderCell>
                            <Table.HeaderCell>GD</Table.HeaderCell>
                            <Table.HeaderCell>Pts</Table.HeaderCell>
                          </Table.Row>
                        </Table.Header>
                    
                        <Table.Body>
                            {this.state.districtArr.map((item, i)=>{
                            return(
                                        
                            <Table.Row key={i} className={(item.Institution_Name === this.props.user.school.Institution_Name) ? 'self': ''}>
                                <Table.Cell>{i + 1}</Table.Cell>
                                <Table.Cell>{item.Institution_Name}</Table.Cell>
                                <Table.Cell>{item.season_games_played}</Table.Cell>
                                <Table.Cell>{item.games_won}</Table.Cell>
                                <Table.Cell>{item.games_lost}</Table.Cell>
                                <Table.Cell>{item.games_drawn}</Table.Cell>
                                <Table.Cell>{item.season_goal_for}</Table.Cell>
                                <Table.Cell>{item.season_goal_against}</Table.Cell>
                                <Table.Cell>{item.season_goal_for - item.season_goal_against}</Table.Cell>
                                <Table.Cell>{item.point}</Table.Cell>
                              </Table.Row>
                              )
                            })
                            }
                            </Table.Body>
                            </Table>
                            )
                        }
                    })()}
        </Segment>
        </section>
        )
    }

    async getLogs(){
        let res = await fetch('/api/school/ward/' + this.props.user.school.Ward_ID + '/' + this.props.user.school.Phase);
        let ris = await res.json();
        this.setState({
            schoolArr: ris
        });
    }

    async getDistrictLogs(){
        let res = await fetch('/api/school/district/' + this.props.user.school.EIDistrict + '/' + this.props.user.school.Phase);
        let ris = await res.json();
        this.setState({
            districtArr: ris
        });
    }

    async getSuburbLogs(){
        let res = await fetch('/api/school/suburb/' + this.props.user.school.Suburb + '/' + this.props.user.school.Phase);
        let ris = await res.json();
        this.setState({
            suburbArr: ris
        });
    }

    componentDidMount(){
        this.getLogs();
        this.getDistrictLogs();
        this.getSuburbLogs();
    }
}

function matchStateToProp(state){
    return {
        user: state.auth
    }
}

export default connect(matchStateToProp)(Log);