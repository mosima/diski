import React, {Component} from 'react';
import FixtureTableComponent from '../components/fixture/fixtureTable';
import {Header, Icon, Segment} from 'semantic-ui-react';
import {connect} from 'react-redux';

class Fixture extends Component{
    render(){
        return (
            <section>
                <Segment>
                <Header as='h2'>
                    <Icon name='university' />
                    <Header.Content>
                    {this.props.user.school.Institution_Name} FIXTURES
                    <Header.Subheader>
                        All Fixtures
                    </Header.Subheader>
                    </Header.Content>
                </Header>
            <FixtureTableComponent />
            </Segment>
            </section>
        )
    }
}

function matchStateToProp(state){
    return {
        user: state.auth
    }
}

export default connect(matchStateToProp)(Fixture);