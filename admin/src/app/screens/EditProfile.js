import React, {Component} from 'react';
import {Grid, Header, Image, Icon, Segment} from 'semantic-ui-react';
import {TextField, MenuItem, Button} from 'material-ui';
import { withStyles } from 'material-ui/styles';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
const styles = theme => ({
    container: {
      
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
    },
    menu: {
      width: 300,
    },
    button: {
        margin: theme.spacing.unit,
      },
      input: {
        display: 'none',
      },
  });

class EditProfile extends Component{
    constructor(props){
        super(props);
        this.state = {
            district: null,
            districtArr: [],
            isActive: true,
            school: null,
            isSchoolActive: true,
            schoolArr: []
        }

    }
    handleChange = name => event => {
        
        this.setState({
          [name]: event.target.value,
        }, ()=>{
            if(name == 'province'){
                
                this.setState({
                    isActive: false
                })
            this.getProvinceDistrict(this.state.province);
            }
            if(name == 'district'){
                
                this.setState({
                    isSchoolActive: false
                })
            this.getSchools(this.state.province, this.state.district);
            }
        });
      };

      async getSchools(province, district){
          let res = await fetch('/api/school/district/profile/' + province + '/' + district);
          let result = await res.json();
          console.log("result", result);
          this.setState({
              schoolArr: result
          });
      }

      async getProvinceDistrict(province){
        let res = await fetch('/api/district/' + province);
        let district = await res.json();
        this.setState({
            districtArr: district
        });
    }
    render(){
        const { classes } = this.props;
        return (
            <section>
                <Grid columns='equal'>
            
            <Grid.Column computer={9}>
              <Segment>
                <Header as='h2'>
                    <Icon name='user' />
                    <Header.Content>
                    User Information
                    <Header.Subheader>
                        View &amp; Your Personal Information 
                    </Header.Subheader>
                    </Header.Content>
                </Header>
                <Image src={this.props.user.picture} style={{height: 250, width: 'auto'}} />
                            <input
                    accept="image/*"
                    className={classes.input}
                    id="raised-button-file"
                    multiple
                    type="file"
                />
                <label htmlFor="raised-button-file">
                    <Button component="span" className={classes.button}>
                    <Icon name="camera" size="large" /> change photo
                    </Button>
                </label>
                <form className={classes.container} noValidate autoComplete="off">
                    <TextField
                    id="name"
                    label="Name"
                    fullWidth={true}
                    className={classes.textField}
                    defaultValue={this.props.user.displayName}
                    onChange={this.handleChange('name')}
                    margin="normal"
                    />
                    <TextField
                    id="email"
                    label="Email"
                    fullWidth={true}
                    defaultValue={this.props.user.email}
                    className={classes.textField}
                    onChange={this.handleChange('email')}
                    margin="normal"
                    disabled
                    />
                    <TextField
                    required
                    id="address"
                    fullWidth={true}
                    label="Address"
                    defaultValue={this.props.user.address}
                    className={classes.textField}
                    onChange={this.handleChange('address')}
                    margin="normal"
                    />
                    <TextField
                    id="image"
                    label="Picture"
                    fullWidth={true}
                    className={classes.textField}
                    type="text"
                    defaultValue={this.props.user.picture}
                    margin="normal"
                    onChange={this.handleChange('picture')}
                    />
                    <TextField
                        id="province"
                        select
                        label="Province"
                        fullWidth={true}
                        className={classes.textField}
                        value={this.state.province || this.props.user.province}
                        onChange={this.handleChange('province')}
                        SelectProps={{
                            MenuProps: {
                            className: classes.menu,
                            },
                        }}
                        helperText="Please select your province"
                        margin="normal"
                        >
                        {this.props.province.map(option => (
                            <MenuItem key={option._id} value={option.short_name}>
                            {option.name}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        id="district"
                        select
                        label="District"
                        fullWidth={true}
                        disabled={this.state.isActive}
                        className={classes.textField}
                        value={this.state.district || this.props.user.district}
                        onChange={this.handleChange('district')}
                        SelectProps={{
                            MenuProps: {
                            className: classes.menu,
                            },
                        }}
                        helperText="Please select your district"
                        margin="normal"
                        >
                        {this.state.districtArr.map(option => (
                            <MenuItem key={option._id} value={option.district}>
                            {option.district}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        id="school"
                        select
                        label="School"
                        fullWidth={true}
                        disabled={this.state.isSchoolActive}
                        className={classes.textField}
                        value={this.state.school || this.props.user.school.Institution_Name}
                        onChange={this.handleChange('school')}
                        SelectProps={{
                            MenuProps: {
                            className: classes.menu,
                            },
                        }}
                        helperText="Please select your school"
                        margin="normal"
                        >
                        {this.state.schoolArr.map(option => (
                            <MenuItem key={option._id} value={option._id}>
                            {option.Institution_Name}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                    id="city"
                    label="City"
                    fullWidth={true}
                    className={classes.textField}
                    defaultValue={this.props.user.city}
                    onChange={this.handleChange('city')}
                    margin="normal"
                    />
                    <TextField
                    id="suburb"
                    label="Suburb"
                    fullWidth={true}
                    className={classes.textField}
                    defaultValue={this.props.user.suburb}
                    onChange={this.handleChange('suburb')}
                    margin="normal"
                    />
                    <Link to="/profile"><Button>
                        <Icon name="angle left" size="large" /> Back To Profile
                    </Button></Link>
                    <Button>
                        <Icon name="save" size="large" /> Save Profile
                    </Button>
                </form>
              </Segment>
            </Grid.Column>
            </Grid>
            </section>
        )
    }
}

function matchStateToProps(state){
    return{
        user: state.auth,
        province: state.province
    }
}

export default connect(matchStateToProps)(withStyles(styles)(EditProfile));