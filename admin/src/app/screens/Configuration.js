import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Divider, Tab } from 'semantic-ui-react';
import FixturePage from '../components/fixture/fixturePage';
import ResultPage from '../components/result/resultPage';
import TermPage from '../components/terms/termPage';
import HolidayPage from '../components/holiday/holidayPage';
import SpotlightPage from '../components/spotlight/spotlightPage';
import ShowcasePage from '../components/showcase/showcasePage';
import NewsPage from '../components/news/newsPage';

const colors = [
    'red', 'orange', 'yellow', 'olive', 'green', 'teal',
    'blue', 'violet', 'purple', 'pink', 'brown', 'grey',
  ]
  
  const panes = [
    { menuItem: 'FIXTURES', render: () => <Tab.Pane attached={true}><FixturePage /></Tab.Pane> },
    { menuItem: 'RESULTS', render: () => <Tab.Pane attached={true}><ResultPage /></Tab.Pane> },
    { menuItem: 'SCHOOL TERMS', render: () => <Tab.Pane attached={true}><TermPage /></Tab.Pane> },
    { menuItem: 'SCHOOL HOLIDAY', render: () => <Tab.Pane attached={true}><HolidayPage /></Tab.Pane> },
    { menuItem: 'SPOTLIGHT', render: () => <Tab.Pane attached={true}><SpotlightPage /></Tab.Pane> },
    { menuItem: 'SHOWCASE', render: () => <Tab.Pane attached={true}><ShowcasePage /></Tab.Pane> },
    { menuItem: 'NEWS', render: () => <Tab.Pane attached={true}><NewsPage /></Tab.Pane> },
  ];

class Configuration extends Component{
    constructor(props){
        super(props);
        this.state = {
            color: colors[0]
        }
    }

    handleColorChange = e => this.setState({ color: e.target.value });
    render(){
        const { color } = this.state
        return (
            <div>

        <Divider hidden />

        <Tab
          menu={{inverted: true, attached: true, tabular: false }}
          panes={panes}
        />
      </div>
        )
    }

    
}

function matchStateToProps(state){
    return {
        province: state.province,
        phases: state.phases
    }
}

export default connect(matchStateToProps)(Configuration);