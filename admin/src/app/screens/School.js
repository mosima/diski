import React, {Component} from 'react';
import {connect} from 'react-redux';
import MapComponent from '../components/map/mapComponent';
import { Grid, Segment, Header, Icon, List } from 'semantic-ui-react';
import PlayerComponent from '../components/player/playerComponent';

class School extends Component{
   
    render(){
        return (
            <Grid columns='equal'>
            <Grid.Column>
              <Segment>
                <Header as='h2'>
                    <Icon name='university' />
                    <Header.Content>
                    {this.props.user.school.Institution_Name}
                    <Header.Subheader>
                        School Detail
                    </Header.Subheader>
                    </Header.Content>
                </Header>
                <MapComponent isMarkerShown latitude={this.props.user.school.Location[0]} longitude={this.props.user.school.Location[1]} />
                <List>
                    <List.Item>
                    <List.Header>Name</List.Header>
                    {this.props.user.school.Institution_Name}
                    </List.Item>
                    <List.Item>
                    <List.Header>Phase</List.Header>
                    {this.props.user.school.Phase}
                    </List.Item>
                    <List.Item>
                    <List.Header>Address</List.Header>
                    {this.props.user.school.StreetAddress}
                    </List.Item>
                    <List.Item>
                    <List.Header>Ward</List.Header>
                    {this.props.user.school.Ward_ID}
                    </List.Item>
                    <List.Item>
                    <List.Header>Suburb</List.Header>
                    {this.props.user.school.Suburb}
                    </List.Item>
                    <List.Item>
                    <List.Header>Municaplity</List.Header>
                    {this.props.user.school.LMunName}
                    </List.Item>
                    <List.Item>
                    <List.Header>City</List.Header>
                    {this.props.user.school.Town_City}
                    </List.Item>
                    <List.Item>
                    <List.Header>District</List.Header>
                    {this.props.user.school.EIDistrict}
                    </List.Item>
                    <List.Item>
                    <List.Header>Province</List.Header>
                    {this.props.user.school.Province}
                    </List.Item>
                </List>
              </Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>
                  
                <Header as='h2'>
                    <Icon name='users' />
                    <Header.Content>
                    Players
                    <Header.Subheader>
                        Your Player List
                    </Header.Subheader>
                    </Header.Content>
                    
                </Header>
                <PlayerComponent />
              </Segment>
            </Grid.Column>
          </Grid>
        )
    }
}

function matchStateToProps(state){
    return{
        user: state.auth
    }
}
export default connect(matchStateToProps)(School);