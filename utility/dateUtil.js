const date_fns = require('date-fns');
const Term = require('../models/school_term.model');
const Holiday = require('../models/publicholiday.model');
const School = require('../models/school.model');
const Province = require('../models/province.model');
const Fixture = require('../models/fixture.model');
const Ward = require('../models/ward.model');

Date.prototype.addDays = function(days) {
  var dat = new Date(this.valueOf())
  dat.setDate(dat.getDate() + days);
  return dat;
}
function getDates (startDate, stopDate){
  var dateArray = new Array();
  var currentDate = startDate;
  while (currentDate <= stopDate) {
    dateArray.push(currentDate)
    currentDate = currentDate.addDays(1);
  }
  return dateArray;
}

module.exports ={
 




 generateMatchDates: (id, arr, cb)=>{
    var availDates = [];
    var matchDate = [];
    Term.findById(id, (err, term)=>{
        if(err){return next(err.message)}
        availDates =  getDates(new Date(term.start_date), new Date(term.end_date));
        availDates.forEach(date=>{
            if(date_fns.isWednesday(new Date(date))){
                if(date_fns.isAfter(new Date(date), new Date())){

                matchDate.push(new Date(date).toDateString());
                
            }
            }
            
        });        
        cb(null, arr, matchDate);
});
}
}
/**
 * 
 * }
 * 
 */