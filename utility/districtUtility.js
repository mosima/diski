'use strict';
const School = require('../models/school.model');
const District = require('../models/district.model');

function createDistrict(){
  var distinctDistrict = [];
  School
  .find()
  .exec((err, schs)=>{
    if(err){console.log(err.message);}

    var districts_province = schs.map(item=>{
      return {
        district: item.EIDistrict,
        province: item.Province,
        schools:[]
      }
    });

    var districts = schs.map(item=>{
      return item.EIDistrict;
    });
    districts.forEach(item=>{
        if(distinctDistrict.indexOf(item) < 0){
            distinctDistrict.push(item);
        }
    });
    distinctDistrict.forEach(item=>{
        var province = {};
        for (var i = 0; i < districts_province.length; i++) {
              if(districts_province[i].district === item){
                province = districts_province[i];
                break;
              }
        }
        let new_district = new District(province);
        new_district.save(err=>{
          if(err){console.log(err.message);}
          console.log(new_district);
        });
    });

  });
}

createDistrict();
